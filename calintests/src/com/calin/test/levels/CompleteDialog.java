package com.calin.test.levels;

import android.util.Log;

import com.calin.tests.Badge;
import com.calin.tests.objects.MenuButton;
import com.calin.tests.objects.NumbersDisplay;

public class CompleteDialog extends MenuButton{

	public NumbersDisplay currentScoreDisplay; 
	public NumbersDisplay earnedXpDisplay;
	public NumbersDisplay earnedCoinsDisplay;
	
	public Badge newHighScoreBadge;
	public Badge levelUpBadge;
	public Badge unlockedNewLevelsBadge;
	
	boolean isNewHeighScore;
	boolean isLevelUp;
	boolean previouslyPassed;
	boolean passed=false;
	boolean hasUnlockedNewLevels;
	
	public CompleteDialog(float x, float y, float width, float height) {
		super(x, y, width, true);
		currentScoreDisplay=new NumbersDisplay(x, y, 1, 0, 10);
		currentScoreDisplay.show();
		earnedXpDisplay = new NumbersDisplay(x, y-1.5f, 1, 0, 40);
		earnedCoinsDisplay = new NumbersDisplay(x, y-3, 1, 0, 40);
		
		newHighScoreBadge=new Badge(x-width/3, y+height/3, 4, 4);
		levelUpBadge = new Badge(x+width/3, y+height/3, 4, 4);
		unlockedNewLevelsBadge = new Badge(x-width/3, y-height/3, 4, 4);
	}
	
	@Override
	public boolean update(float deltaTime)
	{
		super.update(deltaTime);
		switch (super.state) {
		case NORMAL:
			currentScoreDisplay.update(deltaTime);
			earnedXpDisplay.update(deltaTime);
			earnedCoinsDisplay.update(deltaTime);
			
			newHighScoreBadge.update(deltaTime);
			levelUpBadge.update(deltaTime);
			unlockedNewLevelsBadge.update(deltaTime);
			
			boolean showNewHighScoreBadge = isNewHeighScore && currentScoreDisplay.isNormalState();
			boolean showLevelUpBadge = isLevelUp && earnedXpDisplay.isNormalState();
			boolean showUnlockedNewLevelsBadge = hasUnlockedNewLevels && earnedCoinsDisplay.isNormalState();
			boolean showEarnedXpDisplay = (!isNewHeighScore && currentScoreDisplay.isNormalState()) || (isNewHeighScore && currentScoreDisplay.isNormalState());
			boolean showEarnedCoinsDisplay = (!isLevelUp && earnedXpDisplay.isNormalState()) || (isLevelUp && levelUpBadge.isNormalState());
			
//			newHighScoreBadge.tryShow(showNewHighScoreBadge);
//			earnedXpDisplay.tryShow(showEarnedXpDisplay);
//			levelUpBadge.tryShow(showLevelUpBadge);
//			earnedCoinsDisplay.tryShow(showEarnedCoinsDisplay);
//			unlockedNewLevelsBadge.tryShow(showUnlockedNewLevelsBadge);
			
			if (isNewHeighScore && currentScoreDisplay.isNormalState()){
				newHighScoreBadge.show();
			}
			else if (!isNewHeighScore && currentScoreDisplay.isNormalState()){
				earnedXpDisplay.show();
			}
			if (isNewHeighScore && newHighScoreBadge.isNormalState()){
				earnedXpDisplay.show();
			}
			
			if (isLevelUp && earnedXpDisplay.isNormalState()){
				levelUpBadge.show();
			}
			else if (!isLevelUp && earnedXpDisplay.isNormalState()){
				earnedCoinsDisplay.show();
			}
			
			if (isLevelUp && levelUpBadge.isNormalState()){
				earnedCoinsDisplay.show();
			}
			
			if (hasUnlockedNewLevels && earnedCoinsDisplay.isNormalState()){
				unlockedNewLevelsBadge.show();
			}
			break;
		}
		return false;
	}

	public void setFinalData(boolean passed, boolean isNewHeighScore, boolean previouslyPassed, boolean levelUp,  int reachedScore, int earnedXp, int earnedCoins) {
		this.isNewHeighScore = isNewHeighScore && passed;
		this.previouslyPassed=previouslyPassed;//TODO remove hard coded val
		this.passed=passed;
		this.isLevelUp = levelUp;
		this.hasUnlockedNewLevels = passed && !this.previouslyPassed;
		
		currentScoreDisplay.setTargetedNumber(reachedScore);
		earnedXpDisplay.setTargetedNumber(earnedXp);
		earnedCoinsDisplay.setTargetedNumber(earnedCoins);
	}
}
