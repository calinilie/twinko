package com.calin.test.levels;

import java.util.ArrayList;

import com.calin.tests.IDragonHitListener;
import com.calin.tests.TargetController;
import com.calin.tests.food.*;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.Hint;

public class Level03 extends Level{
	
	public Mushroom mushroom;
	public Orange orange;
	

	public Level03(Dragon dragon, int currentHighScore,
			boolean previouslyPassed) {
		super(dragon, currentHighScore, previouslyPassed, 30, 20, 3, LevelIconsDisplayModel.MEDOW_LEVELS);
		state=TUTORIAL_PAUSED;
		hint=new Hint(9, 4.5f, 7, 9);
		
		mushroom=new Mushroom(1, 1, 2, 2, 25, 8f, 5, Stake.BOTTOM_RIGHT, 10);
		orange=new Orange(0, 0, 1.6f, 1.6f, 10, 4, 4, Stake.BOTTOM_MIDDLE, 10);
		
		
		targetController=new TargetController(0, 2);
		targetController.food=new ArrayList<Food>();
		targetController.food.add(mushroom);
		targetController.food.add(orange);
		
		targets.add(mushroom);
		targets.add(orange);
		
		mushroom.subscribeDeadTargetListeners(this, targetController);
		orange.subscribeDeadTargetListeners(this, targetController);
		
		super.setCurrentTarget(mushroom);
	}
	
	@Override
	protected void updateRunning(float deltaTime, Vector2 touchPos, boolean isTouched){
		mushroom.update(deltaTime, isTouched);
		orange.update(deltaTime, isTouched);
		this.revealObjects(59.3f);
		super.updateRunning(deltaTime, touchPos, isTouched);
	}

	@Override
	void initDragon(Dragon dragon) {
		dragon.setBoundsSizeOrientationAndPosition(3, 2.5f, 4, 4, Dragon.EAST);
	}

	@Override
	boolean checkPassed() {
		return score>=50;
	}

	@Override
	void registerEnemies() {
		//DO NOTHING - no enemies in this lvl
		
	}

	@Override
	public void registerDrgaonHitListeners(IDragonHitListener... listeners) {
		//DO NOTHING - no enemies in this lvl
	}

}
