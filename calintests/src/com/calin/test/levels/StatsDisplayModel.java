package com.calin.test.levels;

import android.provider.ContactsContract.CommonDataKinds.Event;
import android.util.Log;

import com.calin.tests.GameObject;
import com.calin.tests.MainScreen;
import com.calin.tests.framework.Input.TouchEvent;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.OverlapTester;
import com.calin.tests.objects.DragonStats;
import com.calin.tests.objects.MenuButton;
import com.calin.tests.objects.NumbersDisplay;

public class StatsDisplayModel {
	
	public GameObject staminaText;
	public NumbersDisplay staminaPoints;
	public MenuButton incStamina;
	
	public GameObject hpText;
	public NumbersDisplay hpPoints;
	public MenuButton incHp;
	
	public GameObject damageText;
	public NumbersDisplay damagePoints;
	public MenuButton incDamage;
	
	public GameObject skillPointsText;
	public NumbersDisplay skillPoints;
	public MenuButton moreSkillPoints;
	
	public MenuButton back;
	
	private DragonStats stats;

	public StatsDisplayModel(DragonStats stats){
		this.stats = stats;
		
		staminaText =  new GameObject(9, 7.875f, 5, 5/4f);
		staminaPoints = new NumbersDisplay(12, 7.875f, 1);
		incStamina = new MenuButton(13, 7.875f, 1);
		
		hpText = new GameObject(9, 5.625f, 5, 5/4f);
		hpPoints = new NumbersDisplay(12, 5.625f, 1);
		incHp = new MenuButton(13, 5.625f, 1);
		
		damageText = new GameObject(9, 3.375f, 5, 5/4f);
		damagePoints = new NumbersDisplay(12, 3.375f, 1);
		incDamage= new MenuButton(13, 3.375f, 1);
		
		skillPointsText = new GameObject(9, 1.125f, 5, 5/4);
		skillPoints = new NumbersDisplay(12, 1.125f, 1);
		moreSkillPoints = new MenuButton(13, 1.125f, 1);
		
		back = new MenuButton(15, 1.125f, 2);
	}
	
	public void update(float deltaTime){
		staminaPoints.update(deltaTime, stats.maxStamina);
		incStamina.update(deltaTime);
		
		hpPoints.update(deltaTime, stats.maxHP);
		incHp.update(deltaTime);
		
		damagePoints.update(deltaTime, stats.damage*1000);
		incDamage.update(deltaTime);
		
		skillPoints.update(deltaTime, stats.unusedSkillPoints);
		moreSkillPoints.update(deltaTime);
		
		back.update(deltaTime);
	}
	
	/**
	 * 
	 * @param event
	 * @param fingerTouch
	 * @return TRUE when back is pressed
	 */
	public boolean onTouchEvent(TouchEvent event, Circle fingerTouch){
		if (event.type == TouchEvent.TOUCH_DOWN) onTouchDown(fingerTouch);
		if (event.type == TouchEvent.TOUCH_UP) return onTouchUp(fingerTouch);
		return false;
	}

	private boolean onTouchUp(Circle fingerTouch) {
		if (OverlapTester.overlapCircleRectangle(fingerTouch, incStamina.bounds)){
			Log.d("stats", "increase stamina");
			stats.increaseMaxStamina();
		}
		
		if (OverlapTester.overlapCircleRectangle(fingerTouch, incHp.bounds)){
			Log.d("stats", "increase HP");
			stats.increaseMaxHp();
		}
		
		if (OverlapTester.overlapCircleRectangle(fingerTouch, incDamage.bounds)){
			Log.d("stats", "increase dmg");
			stats.increaseDmg();
		}
		
		if (OverlapTester.overlapCircleRectangle(fingerTouch, moreSkillPoints.bounds)){
			//TODO google play UI, or store....
		}
		
		if (OverlapTester.overlapCircleRectangle(fingerTouch, back.bounds)){
			return true;
		}
		return false;
		
	}

	private void onTouchDown(Circle fingerTouch) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
