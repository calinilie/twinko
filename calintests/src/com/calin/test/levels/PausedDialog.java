package com.calin.test.levels;

import java.util.List;

import com.calin.tests.IPauseListener;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.objects.MenuButton;

public class PausedDialog extends MenuButton{

	public MenuButton continueButton;
	public MenuButton replayButton;
	public MenuButton shopButton;
	
	private IPauseListener pausedListener;
	
	private int levelId;
	
	public PausedDialog(float x, float y, float width, float height, int levelId){
		super(x, y, width, false);
		
		this.levelId = levelId;
		
		continueButton = new MenuButton(x-2.5f, y, 2, true);
		continueButton.setIncRate(1);
		
		replayButton = new MenuButton(x, y, 2, true);
		replayButton.setIncRate(1);
		
		shopButton = new MenuButton(x+2.5f, y, 2, true);
		shopButton.setIncRate(1);
	}
	
	public boolean update(float deltaTime){
		super.update(deltaTime);
		switch(state){
		case MenuButton.NORMAL:
			if (continueButton.update(deltaTime)){
				onContinueTouchDownComplete();
			}
			if (replayButton.update(deltaTime)){
				onReplayTouchDownComplete();
			}
//			replayButton.update(deltaTime);
			shopButton.update(deltaTime);
			break;
		}
		return false;
	}
	
	public void show(){
		state = MenuButton.NORMAL;
		continueButton.pop();
		replayButton.pop();
		shopButton.pop();
	}
	
	public boolean onTouchDown(Circle finger){
		if (state!=INVISIBLE){
			continueButton.onTouchDown(finger);
			replayButton.onTouchDown(finger);
		}
		return false;
	}
	
	public void setPausedListener(IPauseListener listener){
		this.pausedListener = listener;
	}
	
	private void onContinueTouchDownComplete(){
		state = INVISIBLE;
		continueButton.collapse();
		replayButton.collapse();
		shopButton.collapse();
		pausedListener.onContinuePressed();
	}
	
	private void onReplayTouchDownComplete(){
		pausedListener.onReplayPressed(this.levelId);
//		throw new RuntimeException("not implemented yet!");
	}
	
	private void onShopTouchDownComplete(){
		//TODO
		throw new RuntimeException("not implemented yet!");
	}
	
}
