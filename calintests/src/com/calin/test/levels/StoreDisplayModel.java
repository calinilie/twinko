package com.calin.test.levels;

import java.util.ArrayList;
import android.util.Log;
import com.calin.tests.Drag;
import com.calin.tests.MainScreen;
import com.calin.tests.framework.Input.TouchEvent;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.objects.DamagePotion;
import com.calin.tests.objects.DragonBag;
import com.calin.tests.objects.HealingPotion;
import com.calin.tests.objects.Item;
import com.calin.tests.objects.ItemDescriptionArea;
import com.calin.tests.objects.ItemPlayStore;
import com.calin.tests.objects.MenuButton;
import com.calin.tests.objects.NumbersDisplay;
import com.calin.tests.objects.Potion;
import com.calin.tests.objects.StaminaPotion;
import com.calin.tests.objects.Store;

public class StoreDisplayModel {
	
	public ArrayList<MenuButton> objects;
	public final static float objectSize=2.5f;
	public final static float margin=0.25f;
	
	public HealingPotion healingPotion;
	public StaminaPotion staminaPotion;
	public DamagePotion damagePotion;
	public ItemPlayStore smallGold;
	public ItemPlayStore mediumGold;
	public ItemPlayStore largeGold;
	
	private final float topPosition=9-(objectSize/2+margin/2);
	public static final float unit=objectSize+margin*2;
	
	public MenuButton potionsCategoryButton;
	public MenuButton goldCategoryButton;
	public MenuButton backButton;
	public MenuButton buyButton;
	
	public ItemDescriptionArea itemDescriptionArea;
	
	public MenuButton selected;
	private DragonBag bag;
	
	public NumbersDisplay goldCoins;

	public StoreDisplayModel(DragonBag bag){
		this.bag = bag;
		
		selected=new Item(-10);
		objects=new ArrayList<MenuButton>();
		
		healingPotion = bag.getHealingPotion();
		staminaPotion = bag.getStaminaPotion();
		damagePotion = bag.getDamagePotion();
		smallGold = Store.getInstance().smallGold;
		mediumGold = Store.getInstance().mediumGold;
		largeGold = Store.getInstance().largeGold;
		
		addObejct(healingPotion);
		addObejct(staminaPotion);
		addObejct(damagePotion);
		addObejct(smallGold);
		addObejct(mediumGold);
		addObejct(largeGold);
		
		potionsCategoryButton=new MenuButton(2, topPosition, 2, 2);
		goldCategoryButton=new MenuButton(4.5f, topPosition, 2, 2);
		backButton=new MenuButton(1.2f, 1.2f, 2, true);
		buyButton=new MenuButton(11.4f, 1.2f, 2, true);
		goldCoins = new NumbersDisplay(5f, topPosition-0.5f, 0.7f);		
		
		itemDescriptionArea=new ItemDescriptionArea(6.5f, 3.5f, 11, 5.5f);
	}
	
	public void update(float deltaTime){
		for (MenuButton i : objects){
			i.update(deltaTime);
		}
		goldCoins.update(deltaTime, bag.getGoldCoins());
		
		goldCategoryButton.update(deltaTime);
		potionsCategoryButton.update(deltaTime);
		
		buyButton.update(deltaTime);
		
		//back pressed and animation over
		if (backButton.update(deltaTime)) MainScreen.setState(MainScreen.NORMAL);
		if (itemDescriptionArea.update(deltaTime)) {
			backButton.pop();
			buyButton.pop();
		}
	}
	
	/**
	 * 
	 * @param event
	 * @param fingerTouch
	 * @return TRUE if buyButton is pressed
	 */
	public boolean onTouchEvent(TouchEvent event, Circle fingerTouch){
		int dist=0;
		if ((dist=Drag.listenFling(event, fingerTouch.center))!=0) {
			if (!onEdge(dist)){
				selected.move(dist);
				for (MenuButton i : objects) {
					i.move(dist);
				}
				return false;
			}
			else{
				selected.tryMove(dist);
				for (MenuButton i : objects) {
					i.tryMove(dist);
				}
			}
			return false;
		}
		if (event.type==TouchEvent.TOUCH_DOWN) return onTouchDown(fingerTouch);
		if (event.type==TouchEvent.TOUCH_UP) onTouchUp(fingerTouch);
		return false;
	}
	
	public void addObejct(MenuButton e){
		int size=objects.size();
		float posY = (size==0) ? topPosition : topPosition-unit*size;
		
		if (e instanceof Potion)
			((Potion) e).setPositionSizeAndQuantityInit(14.5f, posY, objectSize, objectSize);
		else 
			e.setPositionAndBounds(14.5f, posY, objectSize, objectSize);
		
		objects.add(e);
	}
	
	private void onTouchUp(Circle finger){
		for (MenuButton i : objects){
			if (i.onTouchDown(finger)){
				selected = i;
				backButton.collapse();
				buyButton.collapse();
				itemDescriptionArea.grow();
				return;
			}
		}
	}
	
	/**
	 * 
	 * @param finger
	 * @return TRUE if buy button is pressed
	 */
	private boolean onTouchDown(Circle finger){
		if (potionsCategoryButton.onTouchDown(finger)){
			float dist=topPosition-objects.get(0).position.y;
			if (dist!=0){
				selected.moveDistance(dist);
				for (MenuButton i : objects) {
					i.moveDistance(dist);
				}
			}
			return false;
		}
		if (goldCategoryButton.onTouchDown(finger)){
			float dist=topPosition-objects.get(3).position.y;
			if (dist!=0){
				selected.moveDistance(dist);
				for (MenuButton i : objects) {
					i.moveDistance(dist);
				}
			}
			return false;
		}
		backButton.onTouchDown(finger);
		if (buyButton.onTouchDown(finger)){
			if (selected instanceof Item){
				int price = ((Item) selected).price;
				if (bag.tryBuy(price)){
					if (selected instanceof HealingPotion)
						bag.incHealingPotionQ();
					if (selected instanceof DamagePotion)
						bag.incDamagePotionQ();
					if (selected instanceof StaminaPotion)
						bag.incStaminaPotionQ();
				}
			}
			if (selected instanceof ItemPlayStore){
				//TODO in app billing initiate
			}
			return true;
		}
		return false;
	}
	
	private boolean onEdge(int dist){
		if (dist==-1){
			if (objects.get(0).position.y==topPosition) return true;
		}
		if (dist==1){
			if (objects.get(objects.size()-1).position.y==topPosition-unit*2) return true;
		}
		return false;
	}
}
