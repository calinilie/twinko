package com.calin.test.levels;

import java.util.ArrayList;

import android.util.Log;

import com.calin.tests.IDragonHitListener;
import com.calin.tests.IDragonRendererListener;
import com.calin.tests.TargetController;
import com.calin.tests.TimeStamp;
import com.calin.tests.food.Cherry;
import com.calin.tests.food.Mushroom;
import com.calin.tests.food.Plum;
import com.calin.tests.food.Stake;
import com.calin.tests.food.Food;
import com.calin.tests.food.Strawberry;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.Bee;
import com.calin.tests.objects.Enemy;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.Target;

public class Level08 extends Level{
	
	public Bee bee;
	public Plum plum;
	public Mushroom mushroomFast, mushroomSlow, mushroomSlowLow, mushroomFastLow;
	float elapsedTime=0;
	
	int mushrooms;
	int plums;
	int requieredMushrooms=15;
	int requieredPlums=5;
	
	ArrayList<TimeStamp> stamps;

	public Level08(Dragon dragon, int currentHighScore,
			boolean previouslyPassed) {
		super(dragon, currentHighScore, previouslyPassed, 50, 90, 8, LevelIconsDisplayModel.MEDOW_LEVELS);
		state=TUTORIAL_PAUSED;

		mushroomSlowLow=	new Mushroom(	0,	0, 	2.0f, 		2.0f, 		40, 	8.0f, 	5, 	Stake.BOTTOM_LEFT_MIDDLE, 	10);
		mushroomFastLow=	new Mushroom(	0,	0, 	1.5f, 		1.5f, 		15, 	8.0f, 	5, 	Stake.BOTTOM_RIGHT, 		5);
		mushroomFast   =	new Mushroom(	0,	0, 	1.5f, 		1.5f, 		15, 	2.0f, 	5, 	Stake.TOP_MIDDLE, 			5);
		mushroomSlow   =	new Mushroom(	0,	0, 	2.0f, 		2.0f, 		40, 	0.5f, 	5, 	Stake.TOP_RIGHT,		 	10);
		plum		   =	new Plum	(	0,  0, 	1.6f, 		1.6f, 		10, 	1.3f, 	5, 	Stake.TOP_FAR_RIGHT, 		10);
		bee=new Bee(-0.5f, 4.6f, 0.75f, 0.75f, 10, 4, 15, 2,  10, super.dragon.position, super.dragon.bounds, new Vector2(15, 3.5f));
		
		targetController=new TargetController(1, 2);
		targetController.food=new ArrayList<Food>();
		targetController.food.add(mushroomSlowLow);
		targetController.food.add(mushroomFastLow);
		targetController.enemies=new ArrayList<Enemy>();
		targetController.enemies.add(bee);
		
		targets.add(bee);
		targets.add(mushroomFastLow);
		targets.add(mushroomSlowLow);
		
		bee.subscribeDragonHitListeners(this);
		
		bee.subscribeDeadTargetListeners(this, targetController);
		plum.subscribeDeadTargetListeners(this, targetController);
		mushroomSlow.subscribeDeadTargetListeners(this, targetController);
		mushroomFast.subscribeDeadTargetListeners(this, targetController);
		mushroomFastLow.subscribeDeadTargetListeners(this, targetController);
		mushroomSlowLow.subscribeDeadTargetListeners(this, targetController);
		
		initTimeStamps();
		super.setCurrentTarget(mushroomSlowLow);
	}
	
	protected void updateRunning(float deltaTime, Vector2 touchPos, boolean isTouched){
		for (Target t:targets){
			t.update(deltaTime, isTouched);
		}
		timeStampListener(deltaTime);
		super.revealObjects(89.3f);
		super.updateRunning(deltaTime, touchPos, isTouched);
	}

	@Override
	boolean checkPassed() {
		Log.d("game", "mushrooms "+mushrooms+ " plums "+plums);
		if (mushrooms>=requieredMushrooms && plums>=requieredPlums) return true;
		return false;
	}

	@Override
	void initDragon(Dragon dragon) {
		dragon.setBoundsSizeOrientationAndPosition(3, 2.5f, 4, 4, Dragon.EAST);
	}
	
	public void onDeadTargetEvent(Target source, int score) {
		super.onDeadTargetEvent(source, score);
		if (source instanceof Mushroom && score>0) mushrooms++;
		if (source instanceof Plum && score>0) plums++;
//		Log.d("game", "mushrooms "+mushrooms+ " plums "+plums);
	}
	
	
	void initTimeStamps(){
		stamps=new ArrayList<TimeStamp>();
		//MUSHROOMS MUSHROOMS MUSHROOMS MUSHROOMS
		stamps.add(new TimeStamp(20) {
			
			@Override
			public void execute() {
				addExtraMushrooms();
				this.executed=true;
			}
		});
		stamps.add(new TimeStamp(5) {
			
			@Override
			public void execute() {
				this.executed=removeExtramushrooms();
			}
		});
		//PLUM PLUM PLUM PLUM PLUM
		stamps.add(new TimeStamp(5) {
			
			@Override
			public void execute() {
				addPlum();
				this.executed=true;
			}
		});
		
		stamps.add(new TimeStamp(5) {
			
			@Override
			public void execute() {
				this.executed=removePlum();
			}
		});
		//PLUM PLUM PLUM PLUM
		stamps.add(new TimeStamp(5) {
			
			@Override
			public void execute() {
				addPlum();
				this.executed=true;
			}
		});
		stamps.add(new TimeStamp(5) {
			
			@Override
			public void execute() {
				this.executed=removePlum();
			}
		});
		//MUSHROOMS MUSHROOMS MUSHROOMS MUSHROOMS
		stamps.add(new TimeStamp(20) {
			
			@Override
			public void execute() {
				addExtraMushrooms();
				this.executed=true;
			}
		});
		stamps.add(new TimeStamp(5) {
			
			@Override
			public void execute() {
				this.executed=removeExtramushrooms();
			}
		});
		
		//PLUM PLUM PLUM PLUM
		stamps.add(new TimeStamp(5) {
			
			@Override
			public void execute() {
				addPlum();
				this.executed=true;
			}
		});
		stamps.add(new TimeStamp(10) {
			
			@Override
			public void execute() {
				this.executed=removePlum();
			}
		});
	}
	
	void timeStampListener(float deltaTime){
		elapsedTime+=deltaTime;
		if (stamps.size()>0){
			TimeStamp stamp=stamps.get(0);
			if (!stamp.executed){
				if (elapsedTime>=stamp.after){
					stamp.execute();
				}
			}
			else{
				stamps.remove(0);
				elapsedTime=0;
			}
		}
	}
	
	private void addPlum(){
		targetController.setMaxFoodNo(3);
		targetController.food.add(plum);
		targets.add(plum);
	}
	
	private boolean removePlum(){
		targetController.setMaxFoodNo(2);
		if (removeTarget(plum)) return true;
		return false;
	}
	
	private void addExtraMushrooms(){
		targetController.setMaxFoodNo(4);
		targetController.food.add(mushroomFast);
		targetController.food.add(mushroomSlow);
		targets.add(mushroomFast);
		targets.add(mushroomSlow);
	}
	
	private boolean removeExtramushrooms(){
		targetController.setMaxFoodNo(2);
		if (removeTarget(mushroomFast) && removeTarget(mushroomSlow)){
			return true;
		}
		return false;
	}

	@Override
	void registerEnemies() {
		enemies.add(bee);
		
	}
	
	public void registerDrgaonHitListeners(IDragonHitListener...listeners){
		for (Enemy e:enemies){
			e.subscribeDragonHitListeners(listeners);
		}
	}

}
