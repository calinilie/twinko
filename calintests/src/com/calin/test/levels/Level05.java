package com.calin.test.levels;

import java.util.ArrayList;

import com.calin.tests.IDragonHitListener;
import com.calin.tests.IDragonRendererListener;
import com.calin.tests.TargetController;
import com.calin.tests.food.Cherry;
import com.calin.tests.food.Food;
import com.calin.tests.food.Stake;
import com.calin.tests.food.Strawberry;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.Bee;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.FailNotifier;
import com.calin.tests.objects.Hint;
import com.calin.tests.objects.Enemy;
import com.calin.tests.objects.Target;

public class Level05  extends Level{
	
	public Strawberry strawberry;
	public Cherry cherry;
	
	public Bee bee;
	
	int tries=0;

	public Level05(Dragon dragon, int currentHighScore,
			boolean previouslyPassed) {
		super(dragon, currentHighScore, previouslyPassed, 40, 90, 5, LevelIconsDisplayModel.MEDOW_LEVELS);
		state=TUTORIAL_PAUSED;
		hint=new Hint(10, 4.5f, 7, 12);
		failNotifier=new FailNotifier(1, 8);
		
		cherry=		new Cherry		(0, 	0, 	1.5f, 1.5f, 10, 0.7f, 	3, 	Stake.TOP_FAR_RIGHT, 	5);
		strawberry=	new Strawberry	(0, 	0, 	1.5f, 1.5f, 20, 1.5f,	1, 	Stake.TOP_RIGHT_MIDDLE,	10);
		bee=new Bee(3.2f, 2.6f, 0.75f, 0.75f, 10, 4, 15, 5, 10, super.dragon.position, super.dragon.bounds, new Vector2(14, 1.5f));
		bee.subscribeDragonHitListeners(this);
		
		targetController=new TargetController(1, 1);
		
		targetController.food=new ArrayList<Food>();
		targetController.food.add(strawberry);
		targetController.food.add(cherry);
		
		targetController.enemies=new ArrayList<Enemy>();
		targetController.enemies.add(bee);
		registerEnemies();
		
		targets.add(bee);
		targets.add(cherry);
		targets.add(strawberry);
		
		for (Target t: targets){
			t.subscribeDeadTargetListeners(this, targetController);
		}
		
		super.setCurrentTarget(strawberry);
	}
	
	protected void updateRunning(float deltaTime, Vector2 touchPos, boolean isTouched){
		for (Target t : targets){
			t.update(deltaTime, isTouched);
			if (t.position.y<=0 && t.state==Target.ALIVE){
				t.kill(0);
				tries++;
				switch (tries) {
				case 1:
					failNotifier.pulseFirst();
					break;
				case 2:
					failNotifier.pulseSecond();
					break;
				case 3:
					failNotifier.pulseLast();
					state=COMPLETED;
					break;
				}
			}
		}
		this.revealObjects(89.3f);
		super.updateRunning(deltaTime, touchPos, isTouched);
	}

	@Override
	void initDragon(Dragon dragon) {
		dragon.setBoundsSizeOrientationAndPosition(4, 2.5f, 4, 4, Dragon.EAST);
	}

	@Override
	boolean checkPassed() {
		return true;
	}

	@Override
	void registerEnemies() {
		enemies.add(bee);
		
	}
	
	public void registerDrgaonHitListeners(IDragonHitListener...listeners){
		for (Enemy e:enemies){
			e.subscribeDragonHitListeners(listeners);
		}
	}
	
	

}
