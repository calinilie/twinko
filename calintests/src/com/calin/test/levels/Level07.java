package com.calin.test.levels;

import java.util.ArrayList;

import com.calin.tests.IDragonHitListener;
import com.calin.tests.IDragonRendererListener;
import com.calin.tests.TargetController;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.Bee;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.Enemy;
import com.calin.tests.objects.Target;

public class Level07 extends Level{

	public Bee bee;
	public Bee mosquito;
	public Bee bat;
	
	public Level07(Dragon dragon, int currentHighScore,
			boolean previouslyPassed) {
		super(dragon, currentHighScore, previouslyPassed, 10, 90, 7, LevelIconsDisplayModel.MEDOW_LEVELS);
		state=TUTORIAL_PAUSED;

		mosquito=	new Bee( 3.2f, 2.6f, 0.50f, 0.50f,  5, 6,  3, 1,  3, super.dragon.position, super.dragon.bounds, new Vector2(14.5f, 1.5f));
		bee=		new Bee(-0.5f, 4.6f, 0.75f, 0.75f, 10, 4, 10, 2,  6, super.dragon.position, super.dragon.bounds, new Vector2(15, 3.5f));
		bat=		new Bee(17.0f, 4.0f, 1.50f, 1.50f, 20, 3, 13, 3, 12, super.dragon.position, super.dragon.bounds, new Vector2(14, 1.5f));
		
		targetController=new TargetController(3, 0);
		targetController.enemies=new ArrayList<Enemy>();
		targetController.enemies.add(mosquito);
		targetController.enemies.add(bee);
		targetController.enemies.add(bat);
		registerEnemies();
		
		targets.add(mosquito);
		targets.add(bee);
		targets.add(bat);
		
		for (Target t:targets){
			t.subscribeDeadTargetListeners(this, targetController);
			((Enemy)t).subscribeDragonHitListeners(this);
		}
		
		super.setCurrentTarget(mosquito);
	}
	
	protected void updateRunning(float deltaTime, Vector2 touchPos, boolean isTouched){
		for(Target t:targets){
			t.update(deltaTime, isTouched);
		}
//		timeStampListener(deltaTime);
		this.revealObjects(89.3f);
		super.updateRunning(deltaTime, touchPos, isTouched);
	}

	@Override
	boolean checkPassed() {
		return true;
	}

	@Override
	void initDragon(Dragon dragon) {
		dragon.setBoundsSizeOrientationAndPosition(3, 2.5f, 4, 4, Dragon.EAST);		
	}

	@Override
	void registerEnemies() {
		enemies.add(bee);
		enemies.add(mosquito);
		enemies.add(bat);
	}
	
	public void registerDrgaonHitListeners(IDragonHitListener...listeners){
		for (Enemy e:enemies){
			e.subscribeDragonHitListeners(listeners);
		}
	}
}
