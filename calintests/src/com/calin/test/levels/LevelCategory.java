package com.calin.test.levels;

import com.calin.tests.objects.GameMenuItem;

public class LevelCategory extends GameMenuItem{

	public boolean isUnlocked;
	
	public LevelCategory(float destX, float destY, float x, float y, float width,
			float height) {
		super(destX, destY, x, y, width, height, 15);
	}
}
