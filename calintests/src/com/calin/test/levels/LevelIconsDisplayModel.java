package com.calin.test.levels;

import android.util.Log;

import com.calin.tests.datasource.Facade;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.OverlapTester;

public class LevelIconsDisplayModel {
	public final static byte MEDOW_LEVELS=1;
	public final static byte JUNGLE_LEVELS=2;
	public final static byte CAVE_LEVELS=3;
	public final static byte CATEGORY4=4;
	public byte state=MEDOW_LEVELS;
	
	public static LevelIcon[] medowLevels;
	public static LevelIcon[] jungleLevels;
	public static LevelIcon[] caveLevels;
	public static LevelIcon[][] categories;
	
	static {
		medowLevels=new LevelIcon[9];
		jungleLevels=new LevelIcon[9];
		caveLevels=new LevelIcon[9];
		categories = new LevelIcon[][]{medowLevels, jungleLevels, caveLevels};
	}
	
	private static boolean firstTimeConstructed = true;
	
	private Facade facade;
	
	public LevelIconsDisplayModel(Facade facade){
		this.facade = facade;
		if (firstTimeConstructed){
			initLevelIcons(medowLevels, MEDOW_LEVELS);
			initLevelIcons(jungleLevels, JUNGLE_LEVELS);
			initLevelIcons(caveLevels, CAVE_LEVELS);
			firstTimeConstructed = false;
		}
	}
	
	public int onTouchUp(Circle touch){
		for (int i=0; i<medowLevels.length; i++)
			if (OverlapTester.overlapCircleRectangle(touch,
					medowLevels[i].bounds)
					&& medowLevels[i].isUnlocked
					&& state == MEDOW_LEVELS) {
				return i+1;
			}
		return 0;
	}
	
	public void update(float deltaTime){
		switch (state) {
		case MEDOW_LEVELS:
			updateLevelIcons(deltaTime, medowLevels);
			break;
		case JUNGLE_LEVELS:
			updateLevelIcons(deltaTime, jungleLevels);
			break;
		case CAVE_LEVELS:
			updateLevelIcons(deltaTime, caveLevels);
			break;
		}
	}
	
	public void setState(byte state){
		this.state = state;
	}
	
	private void initLevelIcons(LevelIcon[] icons, int categoryId){
		float x=3;
		float y=7;
		float size = 2;
		facade.open();
		for (int i=0; i<icons.length; i++){
			if (i % 3 == 0 && i!=0) {
				y-=2.5f;
				x=3;
			}
			icons[i] = new LevelIcon(x, y, size, size);
			int levelId = getLevelIdFromCategoryAndIndex(i, categoryId);
 			icons[i].isUnlocked = this.isLevelUnlocked(levelId);
 			Log.d("levels", String.format("level %d is %b", levelId, icons[i].isUnlocked));
 			x+=4.5f;
		}
		facade.close();
		icons[0].state=LevelIcon.grow;
	}
	
	public static void unlockNextLevels(int currentLevelId, int categoryId){
		int index = getIndexFromCategoryAndLevelId(currentLevelId, categoryId);
		int categoryindex = categoryId-1;
		if (index<categories[categoryindex].length-3){//once a level is passed next 2 levels are unlocked
			categories[categoryindex][index+1].isUnlocked = true;
			categories[categoryindex][index+2].isUnlocked = true;
		}
	}
	
	private void updateLevelIcons(float deltaTime, LevelIcon[] icons){
		for (int i=0; i<icons.length; i++){
			LevelIcon icon=icons[i];
			if (icon.state!=LevelIcon.normal){
				LevelIcon next=null;
				if (i!=icons.length-1) next=icons[i+1];
				icon.update(deltaTime, next);
			}
		}
	}
	
	private static int getLevelIdFromCategoryAndIndex(int index, int categoryId){
		switch(categoryId){
		case (MEDOW_LEVELS):
			return index+1;
		case (JUNGLE_LEVELS):
			return index+11;
		case (CAVE_LEVELS):
			return index+21;
		default:
			return index+1;
		}
	}
	
	private static int getIndexFromCategoryAndLevelId(int levelId, int categoryId){
		switch(categoryId){
		case (MEDOW_LEVELS):
			return levelId-1;
		case (JUNGLE_LEVELS):
			return levelId-11;
		case (CAVE_LEVELS):
			return levelId-21;
		default:
			return levelId-1;
		}
	}
	
	/**
	 * if one level is passed, next 2 levels should be unlocked.
	 * @param levelId
	 * @return
	 */
	private boolean isLevelUnlocked(int levelId){
		if (levelId==1 || levelId==11 || levelId==21) return true;
		else{
			if (levelId==2) return facade.getLevelPreviouslyPassed(1);
			else{
				return facade.getLevelPreviouslyPassed(levelId-2) || facade.getLevelPreviouslyPassed(levelId-1);
			}
		}
	}
	
//	public void backPressed(){
//		if (state!=sets) state=sets;
//	}
}
