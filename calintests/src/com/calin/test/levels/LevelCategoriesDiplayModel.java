package com.calin.test.levels;

import android.util.Log;

import com.calin.tests.datasource.Facade;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.OverlapTester;
import com.calin.tests.objects.GameMenuItem;

public class LevelCategoriesDiplayModel {

	public static final byte CATEGORY_MEDOW = LevelIconsDisplayModel.MEDOW_LEVELS;
	public static final byte CATEGORY_JUNGLE = LevelIconsDisplayModel.JUNGLE_LEVELS;
	public static final byte CATEGORY_CAVE = LevelIconsDisplayModel.CAVE_LEVELS;
	
	public static LevelCategory medowCategory;
	public static LevelCategory jungleCategory;
	public static LevelCategory caveCategory;
	
	public static final float width = 6.5f;
	public static final float height = 3;
	
	private static boolean firstTimeConstructed = true;
	
	static{
		medowCategory=new LevelCategory(1+width, 9-1-height/2f, -7, 9-1-height/2f, width, height);
		jungleCategory=new LevelCategory(16-1-width, 9-1-height/2f, 23, 9-1-height/2f, width, height);
		caveCategory=new LevelCategory(8, 1+height, 8, -4, width, height);
		medowCategory.movingState=GameMenuItem.MOVE;
		jungleCategory.movingState=GameMenuItem.MOVE;
		caveCategory.movingState=GameMenuItem.MOVE;
	}
	
	public LevelCategoriesDiplayModel(Facade facade){
		if (firstTimeConstructed){
			initLevelCategories(facade);
			firstTimeConstructed = false;
		}
	}
	
	private void initLevelCategories(Facade facade) {
		facade.open();
		medowCategory.isUnlocked = facade.isCategoryUnlocked(CATEGORY_MEDOW);
		jungleCategory.isUnlocked = facade.isCategoryUnlocked(CATEGORY_JUNGLE);
		caveCategory.isUnlocked = facade.isCategoryUnlocked(CATEGORY_CAVE);
//		Log.d("levels", String.format("medow categ is %b", medowCategory.isUnlocked));
//		Log.d("levels", String.format("jungle categ is %b", jungleCategory.isUnlocked));
//		Log.d("levels", String.format("cave categ is %b", caveCategory.isUnlocked));
		facade.close();
	}

	/**
	 * CATEGORY_MEDOW = 1
	 * CATEGORY_JUNGLE = 2
	 * CATEGORY_CAVE = 3
	 * @param touch
	 * @return id of chosen category
	 */
	public byte onTouchUp(Circle touch){
		if (OverlapTester.overlapCircleRectangle(touch, medowCategory.bounds)
				&&
				medowCategory.isUnlocked){
			return CATEGORY_MEDOW;
		}
		if (OverlapTester.overlapCircleRectangle(touch, jungleCategory.bounds)
				&&
				jungleCategory.isUnlocked){
			return CATEGORY_JUNGLE;
		}
		if (OverlapTester.overlapCircleRectangle(touch, caveCategory.bounds)
				&&
				caveCategory.isUnlocked){
			return CATEGORY_CAVE;
		}
		return 0;
	}
	
	public void update(float deltaTime){
		medowCategory.update(deltaTime);
		jungleCategory.update(deltaTime);
		caveCategory.update(deltaTime);
	}
	
	public static void unlockMedowCategory(){
		medowCategory.isUnlocked=true;
	}
	
	public static void unlockCaveCategory(){
		caveCategory.isUnlocked=true;
	}
	
	public static void unlockJungleCategory(){
		jungleCategory.isUnlocked=true;
	}
}
