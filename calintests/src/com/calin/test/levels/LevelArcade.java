package com.calin.test.levels;

import java.util.ArrayList;

import android.util.Log;

import com.calin.tests.IDragonHitListener;
import com.calin.tests.TargetController;
import com.calin.tests.TimeStamp;
import com.calin.tests.food.*;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.*;

public class LevelArcade extends Level{
	
	public Bee beeEasy;
	public Peach peachEasy, peachHard, peachNormal;
	public Orange orangeEasy, orangeNormal, orangeHard;
	public Plum plumEasy, plumNormal, plumHard;
	public Strawberry strawberryEasy, strawberryNormal, strawberryHard;
	public Cherry cherryEasy, cherryNormal, cherryHard;
	public Mushroom mushroom;
	
	public ArrayList<TimeStamp> stamps;
	float elapsedTime=0;
	
	public LevelArcade(Dragon dragon, int currentHighScore,
			boolean previouslyPassed) {
		super(dragon, currentHighScore, previouslyPassed, 10, 100000, 100, LevelIconsDisplayModel.MEDOW_LEVELS);
		targetController=new TargetController(0, 1);
		
		initTimeStamp();
		initEasyObjects();
		
		targets.add(mushroom);
		
		
		targetController.food=new ArrayList<Food>();
		targetController.food.add(mushroom);
		targetController.enemies=new ArrayList<Enemy>();
		
		super.setCurrentTarget(mushroom);
	}
	
	protected void updateRunning(float deltaTime, Vector2 touchPos, boolean isTouched){
		timeStampListener();
		elapsedTime+=deltaTime;
		for (Target t : targets) t.update(deltaTime, isTouched);
		super.updateRunning(deltaTime, touchPos, isTouched);
		this.revealObjects(99999.3f);
	}
	@Override
	void updateTutorial(float deltaTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	boolean onTouchUpTutorial(Circle touchPos) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	void initDragon(Dragon dragon) {
		dragon.setBoundsSizeOrientationAndPosition(4, 2, 7, 7, Dragon.EAST);
	}
	
	void initEasyObjects(){
		mushroom=new Mushroom(1, 1, 2, 2, 25, 8f, 6, Stake.BOTTOM_RIGHT, 10);
		mushroom.subscribeDeadTargetListeners(targetController, this);
		
		peachEasy=new Peach(0, 0, 1.4f, 1.4f, 10, 1.6f, 5, Stake.TOP_FAR_LEFT, 10);
		peachEasy.subscribeDeadTargetListeners(targetController, this);
		
		plumEasy=new Plum(0, 0, 1.6f, 1.6f, 10, 1.3f, 5, Stake.TOP_MIDDLE, 10);
		plumEasy.subscribeDeadTargetListeners(targetController, this);
		
		strawberryEasy=new Strawberry(0, 0, 1.5f, 1.5f, 10, 1, 5, Stake.TOP_FAR_RIGHT, 10);
		strawberryEasy.subscribeDeadTargetListeners(targetController, this);
		
		orangeEasy=new Orange(0, 0, 1.6f, 1.6f, 20, 1, 4, Stake.TOP_MIDDLE, 10);
		orangeEasy.subscribeDeadTargetListeners(targetController, this);
		
		cherryEasy=new Cherry(0, 0, 1.5f, 1.5f, 10, 8, 3, Stake.BOTTOM_MIDDLE, 10);
		cherryEasy.subscribeDeadTargetListeners(targetController, this);
		
		beeEasy=new Bee(3.2f, 2.6f, 0.75f, 0.75f, 10, 5, 10, 5, 10,super.dragon.position, super.dragon.bounds, new Vector2(14, 1.5f));
		beeEasy.subscribeDeadTargetListeners(this, targetController);
	}
	
	void timeStampListener(){
		if (stamps.size()>0){
			TimeStamp stamp=stamps.get(0);
			if (!stamp.executed){
				if (elapsedTime>=stamp.after){
					stamp.execute();
				}
			}
			else{
				stamps.remove(0);
				elapsedTime=0;
			}
		}
	}
	
	void initTimeStamp(){
		stamps=new ArrayList<TimeStamp>();
		stamps.add(new TimeStamp(1) {
			//1 Te Te
			@Override
			public void execute() {
				Log.d("arcade", "1 Te Te");
				targets.add(orangeEasy);
				targetController.food.add(orangeEasy);
				elapsedTime=0;
				this.executed=true;
			}
		});
		
		stamps.add(new TimeStamp(20) {
			//2 Te Te
			@Override
			public void execute() {
				Log.d("arcade", "2 Te Te");
				targetController.setMaxFoodNo(2);
				elapsedTime=0;
				this.executed=true;
			}
		});
		
		stamps.add(new TimeStamp(25) {
			// 1 Te Te 1 Ee
			@Override
			public void execute() {
				Log.d("arcade", "1 Te Te 1 Ee");
				targets.add(beeEasy);
				targetController.enemies.add(beeEasy);
				targetController.setMaxEnemiesNo(1);
				targetController.setMaxFoodNo(1);
				elapsedTime=0;
				this.executed=true;
			}
		});
		
		stamps.add(new TimeStamp(25) {
			// 1 Te Te Te 1 Ee 
			@Override
			public void execute() {
				Log.d("arcade", "1 Te Te Te 1 Ee");
				targets.add(cherryEasy);
				targetController.food.add(cherryEasy);
				elapsedTime=0;
				this.executed=true;
			}
		});
		
		stamps.add(new TimeStamp(30) {
			//2 Te Te 1Ee
			@Override
			public void execute() {
				Log.d("arcade", "2 Te Te 1Ee");
				targetController.setMaxFoodNo(2);
				if (removeTarget(mushroom)){
					this.executed=true;
					elapsedTime=0;
				}
				
			}
		});
		
		
		
		
		
	}

	@Override
	boolean checkPassed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	void registerEnemies() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerDrgaonHitListeners(IDragonHitListener... listeners) {
		// TODO Auto-generated method stub
		
	}

}
