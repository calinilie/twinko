package com.calin.test.levels;

import java.util.ArrayList;

import android.util.Log;

import com.calin.tests.IDragonHitListener;
import com.calin.tests.IDragonRendererListener;
import com.calin.tests.TargetController;
import com.calin.tests.TimeStamp;
import com.calin.tests.food.*;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.Bee;
import com.calin.tests.objects.Enemy;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.Target;

public class Level06 extends Level{
	
	public Strawberry stFast, stFastLow, stSlowLow;
	public Cherry cherry;
	public Bee bee;
	
	ArrayList<TimeStamp> stamps;
	float elapsedTime=0;
	
	int requieredStrawberries=8;
	int gatheredStr=0;
	int requieredCherries=5;
	int gatheredCherries=0;

	public Level06(Dragon dragon, int currentHighScore,
			boolean previouslyPassed) {
		//9 - playtime TODO test temp
		super(dragon, currentHighScore, previouslyPassed, 50, 9, 6, LevelIconsDisplayModel.MEDOW_LEVELS);
		state=TUTORIAL_PAUSED;

		cherry=		new Cherry		(0, 0, 	1.5f, 1.5f, 10,   8f, 	1, 	Stake.BOTTOM_LEFT,	 		 5);
		stFast=		new Strawberry	(0, 0, 	1.2f, 1.2f, 15,   2f,	4, 	Stake.TOP_LEFT_MIDDLE,			 5);
		stFastLow=  new Strawberry	(0, 0, 	1.2f, 1.2f, 15,	  8f,	4, 	Stake.BOTTOM_LEFT,		 5);
		stSlowLow=new Strawberry	(0, 0, 	2.0f, 2.0f, 40,   8f,	4, 	Stake.BOTTOM_RIGHT_MIDDLE,	10);
		bee=new Bee(14, 1.5f, 0.75f, 0.75f, 10, 5, 15, 5, 10, super.dragon.position, super.dragon.bounds, new Vector2(3.2f, 2.6f));
//		bee.subscribeDragonHitListeners(this);
		
		targetController=new TargetController(1, 2);
		targetController.food=new ArrayList<Food>();
		targetController.food.add(cherry);
		targetController.food.add(stSlowLow);
		
		targetController.enemies=new ArrayList<Enemy>();
		targetController.enemies.add(bee);
		registerEnemies();
		
		targets.add(bee);
		targets.add(cherry);
		targets.add(stSlowLow);
		
		this.initTimeSTamps();
		
		for (Target t: targets){
			t.subscribeDeadTargetListeners(this, targetController);
		}
		
		stFast.subscribeDeadTargetListeners(this, targetController);
		stFastLow.subscribeDeadTargetListeners(this, targetController);
		super.setCurrentTarget(cherry);
	}
	
	protected void updateRunning(float deltaTime, Vector2 touchPos, boolean isTouched){
		for(Target t:targets){
			t.update(deltaTime, isTouched);
		}
		timeStampListener(deltaTime);
		this.revealObjects(89.3f);
		super.updateRunning(deltaTime, touchPos, isTouched);
	}
	
	void timeStampListener(float deltaTime){
		elapsedTime+=deltaTime;
		if (stamps.size()>0){
			TimeStamp stamp=stamps.get(0);
			if (!stamp.executed){
				if (elapsedTime>=stamp.after){
					stamp.execute();
				}
			}
			else{
				stamps.remove(0);
				elapsedTime=0;
			}
		}
	}
	
	public void onDeadTargetEvent(Target source, int score) {
		super.onDeadTargetEvent(source, score);
		if (source instanceof Cherry && score>0) gatheredCherries++;
		if (source instanceof Strawberry && score>0) gatheredStr++;
	}
	
	void initTimeSTamps(){
		stamps=new ArrayList<TimeStamp>();
		stamps.add(new TimeStamp(15) {
			
			@Override
			public void execute() {
				addStrawberries();
				targetController.setMaxFoodNo(4);
				this.executed=true;
				Log.d("ts", "first Executed!!!!!");
			}
		});
		
		stamps.add(new TimeStamp(20) {
			@Override
			public void execute() {
				targetController.setMaxFoodNo(2);
				 if (removeTarget(stFast) && removeTarget(stFastLow)){
					 this.executed=true;
					 Log.d("ts", "SECOND Executed!!!!!");
				 }
			}
		});
		
		stamps.add(new TimeStamp(15) {
			
			@Override
			public void execute() {
				addStrawberries();
				targetController.setMaxFoodNo(4);
				this.executed=true;
				Log.d("ts", "first Executed!!!!!");
			}
		});
		
		stamps.add(new TimeStamp(20) {
			@Override
			public void execute() {
				targetController.setMaxFoodNo(2);
				 if (removeTarget(stFast) && removeTarget(stFastLow)){
					 this.executed=true;
					 Log.d("ts", "SECOND Executed!!!!!");
				 }
			}
		});
	}

	@Override
	void initDragon(Dragon dragon) {
		dragon.setBoundsSizeOrientationAndPosition(13, 2.5f, 4, 4, Dragon.WEST);		
	}
	
	private void addStrawberries(){
		targets.add(stFast);
		targets.add(stFastLow);
		
		targetController.food.add(stFast);
		targetController.food.add(stFastLow);
	}

	@Override
	boolean checkPassed() {
		Log.d("lvl over","str: "+gatheredStr);
		Log.d("lvl over", "cherry: "+gatheredCherries);
		//TODO TEST TEMP
//		if (gatheredStr>=requieredStrawberries && gatheredCherries>=requieredCherries) 
			return true;
//		return false;
	}

	@Override
	void registerEnemies() {
		enemies.add(bee);
	}
	
	public void registerDrgaonHitListeners(IDragonHitListener...listeners){
		for (Enemy e:enemies){
			e.subscribeDragonHitListeners(listeners);
		}
	}

}
