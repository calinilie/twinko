package com.calin.test.levels;

import java.util.ArrayList;

import android.util.Log;

import com.calin.tests.IDragonHitListener;
import com.calin.tests.IDragonRendererListener;
import com.calin.tests.TargetController;
import com.calin.tests.TimeStamp;
import com.calin.tests.food.*;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.Bee;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.FailNotifier;
import com.calin.tests.objects.Hint;
import com.calin.tests.objects.Enemy;
import com.calin.tests.objects.Target;

public class Level09 extends Level{
	
	public Mushroom mushroomEasy, mushRoomHard;
	public Cherry cherry;
	
	public Bee bee;
	
	public Hint hint;
	boolean firstHint=false;
	
	ArrayList<TimeStamp> stamps;
	float elapsedTime=0;
	
	int tries=0;

	public Level09(Dragon dragon, int currentHighScore,
			boolean previouslyPassed) {
		super(dragon, currentHighScore, previouslyPassed, 50, 80, 9, LevelIconsDisplayModel.MEDOW_LEVELS);
		state=TUTORIAL_PAUSED;
		hint=new Hint(7, 4.5f, 7, 11);
		
		cherry=			new Cherry	(	0, 	0, 	1.5f, 	1.5f, 	10, 	1, 	3, 	Stake.TOP_MIDDLE, 	5);
		mushroomEasy=	new Mushroom(	0, 	0, 	1.5f, 	1.5f, 	 8, 	0.5f, 	5, 	Stake.TOP_FAR_LEFT, 	5);
		mushRoomHard=	new Mushroom(	0,	0, 	2, 		2, 		25, 	0.4f, 	1, 	Stake.TOP_MIDDLE, 	5);
		bee=new Bee(14, 1.5f, 0.75f, 0.75f, 10, 5, 25, 5, 10, super.dragon.position, super.dragon.bounds, new Vector2(3.2f, 2.6f)); 
		bee.subscribeDragonHitListeners(this);
		
		targetController=new TargetController(1, 2);
		targetController.food = new ArrayList<Food>();
		targetController.food.add(cherry);
		targetController.food.add(mushroomEasy);
		targetController.food.add(mushRoomHard);
		targetController.enemies=new ArrayList<Enemy>();
		targetController.enemies.add(bee);
		
		targets.add(bee);
		targets.add(mushroomEasy);
		targets.add(mushRoomHard);
		targets.add(cherry);
		
		for (Target t: targets){
			t.subscribeDeadTargetListeners(this, targetController);
		}
		
		super.setCurrentTarget(cherry);
		
		initTimeStamps();
		failNotifier=new FailNotifier(1, 8);
	}
	
	protected void updateRunning(float deltaTime, Vector2 touchPos, boolean isTouched){
		for (Target t : targets){
			t.update(deltaTime, isTouched);
			if (t.position.y<=0 && t.state==Target.ALIVE){
				t.kill(0);
				tries++;
				switch (tries) {
				case 1:
					failNotifier.pulseFirst();
					break;
				case 2:
					failNotifier.pulseSecond();
					break;
				case 3:
					failNotifier.pulseLast();
					state=COMPLETED;
					break;
				}
			}
		}
		elapsedTime+=deltaTime;
		timeStampListener();
		this.revealObjects(79.3f);
		super.updateRunning(deltaTime, touchPos, isTouched);
	}

	@Override
	void updateTutorial(float deltaTime) {
		if (passedTime>=0.5f && !firstHint){
			hint.show();
			firstHint=true;
		}
		passedTime+=deltaTime;
		hint.update(deltaTime);
	}

	@Override
	boolean onTouchUpTutorial(Circle touchPos) {
		if (hint.onTouchUp(touchPos)){
			return true;
		}
		return false;
	}

	@Override
	void initDragon(Dragon dragon) {
		dragon.setBoundsSizeOrientationAndPosition(13, 2.5f, 4, 4, Dragon.WEST);
	}
	
	void initTimeStamps(){
		stamps=new ArrayList<TimeStamp>();
		stamps.add(new TimeStamp(20) {
			
			@Override
			public void execute() {
				targetController.setMaxFoodNo(1);
				this.executed=true;
			}
		});
		stamps.add(new TimeStamp(10) {
			
			@Override
			public void execute() {
				targetController.setMaxFoodNo(2);
				this.executed=true;
			}
		});
		
		stamps.add(new TimeStamp(15) {
			
			@Override
			public void execute() {
				targetController.setMaxFoodNo(1);
				this.executed=true;
			}
		});
		
		stamps.add(new TimeStamp(10) {
			
			@Override
			public void execute() {
				targetController.setMaxFoodNo(2);
				this.executed=true;
			}
		});
		
		stamps.add(new TimeStamp(15) {
			
			@Override
			public void execute() {
				targetController.setMaxFoodNo(1);
				this.executed=true;
			}
		});
		
	}
	
	void timeStampListener(){
		if (stamps.size()>0){
			TimeStamp stamp=stamps.get(0);
			if (!stamp.executed){
				if (elapsedTime>=stamp.after){
					stamp.execute();
				}
			}
			else{
				stamps.remove(0);
				elapsedTime=0;
			}
		}
	}

	@Override
	boolean checkPassed() {
		return true;
	}

	@Override
	void registerEnemies() {
		enemies.add(bee);
	}
	
	public void registerDrgaonHitListeners(IDragonHitListener...listeners){
		for (Enemy e:enemies){
			e.subscribeDragonHitListeners(listeners);
		}
	}
}
