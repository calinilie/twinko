package com.calin.test.levels;

import java.util.ArrayList;

import com.calin.tests.IDragonHitListener;
import com.calin.tests.IDragonRendererListener;
import com.calin.tests.TargetController;
import com.calin.tests.food.*;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.*;

public class Level04 extends Level{

	public Cherry cherry;
	public Orange orange;
	
	public Bee bee;
	int requieredScore=400;
	
	public Level04(Dragon dragon, int currentHighScore,
			boolean previouslyPassed) {
		super(dragon, currentHighScore, previouslyPassed, 40, 70, 4, LevelIconsDisplayModel.MEDOW_LEVELS);
		
		//min 150 points!
		state=TUTORIAL_PAUSED;
		hint=new Hint(7, 4.5f, 7, 10);
		
		cherry=new Cherry(0, 0, 1.5f, 1.5f, 10, 8, 3, Stake.BOTTOM_LEFT, 5);
		orange=new Orange(0, 0, 1.6f, 1.6f, 45, 1, 5, Stake.TOP_FAR_LEFT, 20);
		bee=new Bee(14, 1.5f, 0.75f, 0.75f, 10, 5, 15, 5, 10, super.dragon.position, super.dragon.bounds, new Vector2(3.2f, 2.6f));
		bee.subscribeDragonHitListeners(this);
		
		targetController=new TargetController(1, 1);
		targetController.food=new ArrayList<Food>();
		targetController.food.add(orange);
		targetController.food.add(cherry);
		targetController.enemies=new ArrayList<Enemy>();
		targetController.enemies.add(bee);
		
		targets.add(bee);
		targets.add(orange);
		targets.add(cherry);
		
		bee.subscribeDeadTargetListeners(this, targetController);
		orange.subscribeDeadTargetListeners(this, targetController);
		cherry.subscribeDeadTargetListeners(this, targetController);
		
		super.setCurrentTarget(orange);
	}
	
	protected void updateRunning(float deltaTime, Vector2 touchPos, boolean isTouched){
		for (Target t : targets){
			t.update(deltaTime, isTouched);
		}
		this.revealObjects(69.3f);
		super.updateRunning(deltaTime, touchPos, isTouched);
	}

	@Override
	void initDragon(Dragon dragon) {
		dragon.setBoundsSizeOrientationAndPosition(13, 2.5f, 4, 4, Dragon.WEST);
	}

	@Override
	boolean checkPassed() {
		if (score>=requieredScore) return true;
		return false;
	}

	@Override
	void registerEnemies() {
		enemies.add(bee);
		
	}
	
	public void registerDrgaonHitListeners(IDragonHitListener...listeners){
		for (Enemy e:enemies){
			e.subscribeDragonHitListeners(listeners);
		}
	}
	
}
