package com.calin.test.levels;

import java.util.ArrayList;

import com.calin.tests.IDragonHitListener;
import com.calin.tests.TargetController;
import com.calin.tests.food.Broccoli;
import com.calin.tests.food.Mushroom;
import com.calin.tests.food.Stake;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.*;

public class Level1 extends Level{

		public static final int DRAGON_SIZE = 8;
		public static final float DRAGON_POSX=8;
		public static final float DRAGON_POSY=2;
		public final static int STAKE_SIZE = 1;

		public Mushroom mushroom;
		public Broccoli broccoli;
		public Stake stake3;
		
		public Bat bat;
		static float playTime=100;

		static final int XP=20;

				
		public Level1(Dragon dragon, int currentHighScore, boolean previouslyPassed){
			super(dragon, currentHighScore, previouslyPassed, XP, playTime, 1000, LevelIconsDisplayModel.MEDOW_LEVELS);
			dragon.setBoundsSizeOrientationAndPosition(DRAGON_POSX, DRAGON_POSY, DRAGON_SIZE, DRAGON_SIZE, Dragon.WEST);
			mushroom=new Mushroom(1, 1, STAKE_SIZE, STAKE_SIZE, 7, 2, 1, Stake.TOP_FAR_RIGHT, 10);
			broccoli=new Broccoli(1, 1, STAKE_SIZE, STAKE_SIZE, 5, 3, 1, Stake.TOP_FAR_LEFT, 15);
			stake3=new Stake(1, 1, STAKE_SIZE, STAKE_SIZE, 30, 1, 5, Stake.TOP_MIDDLE, 30);
			bat=new Bat(0, 5, 2, 1, 10, 5, this.dragon, 10, 2);

			targetController=new TargetController(1, 2);
			super.setCurrentTarget(this.mushroom);
//			targets=new Target[]{bat, mushroom, broccoli, stake3};
			targets=new ArrayList<Target>();
			targets.add(bat);
			targets.add(mushroom);
			targets.add(broccoli);
			targets.add(stake3);
			targetController.enemies=new ArrayList<Enemy>();
			targetController.enemies.add(bat);
//			targetController.food=new Food[]{mushroom, broccoli, stake3};
			bat.subscribeDeadTargetListeners(targetController, this);
			mushroom.subscribeDeadTargetListeners(targetController, this);
			broccoli.subscribeDeadTargetListeners(targetController, this);
			stake3.subscribeDeadTargetListeners(targetController, this);
			
		}
		
		protected void updateRunning(float deltaTime, Vector2 touchPos, boolean isTouched){
			super.updateRunning(deltaTime, touchPos, isTouched);
			
			mushroom.update(deltaTime, isTouched);
//			mushroom.checkCookedState(isTouched);
			broccoli.update(deltaTime, isTouched);
//			broccoli.checkCookedState(isTouched);
			stake3.update(deltaTime, isTouched);
//			stake3.checkCookedState(isTouched);
			
			bat.update(deltaTime, isTouched);
		}

		@Override
		void updateTutorial(float deltaTime) {
			// TODO Auto-generated method stub
			
		}

		@Override
		boolean onTouchUpTutorial(Circle touchPos) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		void initDragon(Dragon dragon) {
			// TODO Auto-generated method stub
			
		}

		@Override
		boolean checkPassed() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		void registerEnemies() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void registerDrgaonHitListeners(IDragonHitListener... listeners) {
			// TODO Auto-generated method stub
			
		}
}
