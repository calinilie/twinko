package com.calin.test.levels;

import java.util.ArrayList;

import com.calin.tests.DragonGame;
import com.calin.tests.DynamicGameObject;
import com.calin.tests.IDragonHitListener;
import com.calin.tests.TargetController;
import com.calin.tests.food.Food;
import com.calin.tests.food.Mushroom;
import com.calin.tests.food.Stake;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.SlideController;
import com.calin.tests.objects.Target;
import com.calin.tests.objects.Hint;

public class Level01 extends Level{
	
	private final static int XP=10;
	private final static float elapsedTime=5;
	
	boolean intro=true;
	boolean transition=false;
	boolean transitionfirst=true;
	
	public DynamicGameObject cameraModel;
	public float zoom=1;
	private final float zoomOutRate;
//	float passedTime=0;
	
	float vx, vy;
	
	boolean controllerHintPrevoislyDone=false;
	boolean mushroomHpPreviouslyDone=false;
	
	public Mushroom mushroom;

	public Level01(Dragon dragon, int currentHighScore,
			boolean previouslyPassed) {
		super(dragon, currentHighScore, previouslyPassed, XP, elapsedTime, 1, LevelIconsDisplayModel.MEDOW_LEVELS);
		mushroom=new Mushroom(1, 1, 2f, 2f, 25, 0.5f, 5, Stake.TOP_MIDDLE, 10);
		
		targetController=new TargetController(0, 1);
		targetController.food=new ArrayList<Food>();
		targetController.food.add(mushroom);
		
		this.targets=new ArrayList<Target>();
		targets.add(mushroom);
		
		mushroom.subscribeDeadTargetListeners(this, targetController);
		
		
		//camera stuff
		if (DragonGame.tutorial) cameraModel=new DynamicGameObject(6, 3, 0, 0);
		else cameraModel=new DynamicGameObject(8, 4.5f, 0, 0);
		Vector2 center=new Vector2(8, 4.5f);
		float speed=5;
		float angle=center.sub(cameraModel.position).angle()*Vector2.TO_RADIANS;
		vx =  (float) (speed*Math.cos(angle));
		vy = (float) (speed*Math.sin(angle));
		zoomOutRate=0.6f;
		//end camera stuff
		
		hint=new Hint(8, 3, 5, 1);
		
		if (!DragonGame.tutorial){
			intro=false;
		}
		
		super.setCurrentTarget(mushroom);
	}
	
	protected void updateRunning(float deltaTime, Vector2 touchPos, boolean isTouched){
		if (intro){
			zoomedIn();
		}
		if (transition){
			updateTransition(deltaTime);
		}
		if (!intro && !transition){
			mushroom.update(deltaTime, isTouched);
			if (DragonGame.tutorial) mushroomListening();
			else revealObjects(59.3f);
			super.updateRunning(deltaTime, touchPos, isTouched);
		}
	}
	
	private void mushroomListening(){
		if (mushroom.position.y<=8.5f && !controllerHintPrevoislyDone ) {
			state=Level.TUTORIAL_PAUSED;
			hint.show(8, 4.5f, 7, 2);
			controllerHintPrevoislyDone=true;
		}
		if (mushroom.curHP<mushroom.maxHP/2 && !mushroomHpPreviouslyDone){
			state=Level.TUTORIAL_PAUSED;
			hint.id=4;
			hint.show();
			mushroomHpPreviouslyDone=true;
		}
		
	}
	
	private void zoomedIn(){
		zoom=0.7f;
//		zoom=1;
		cameraModel.position.set(6, 3);
		state=Level.TUTORIAL_PAUSED;
		hint.show();
	}
	
	private void updateTransition(float deltaTime){
		cameraModel.position.add(vx*deltaTime, vy*deltaTime);
		if (zoom<=1){
			zoom+=zoomOutRate*deltaTime;
		}
		if (cameraModel.position.x>=8){
			transition=false;
			revealObjects(61);
			cameraModel.position.set(8, 4.5f);
		}
	}

	@Override
	void updateTutorial(float deltaTime) {
		dragon.update(this.currentTargetDummy, this.isTouched, tempStamina, deltaTime);
		controller.updatePulse(deltaTime);
		hint.update(deltaTime);
	}

	@Override
	boolean onTouchUpTutorial(Circle touchPos) {
		if (hint.onTouchUp(touchPos) && hint.id==1){
			intro=false;
			if (transitionfirst) transition=true;
			transitionfirst=false;
			hint.id=2;
			hint.reset();
			return true;
		}
		
		if (hint.onTouchUp(touchPos) && hint.id==2){
			hint.reset();
			hint.id=3;
			hint.show();
			controller.state=SlideController.PULSE;
			return false;
		}
		
		if (hint.onTouchUp(touchPos) && hint.id==3){
			hint.reset();
			controller.state=SlideController.NORMAL;
			return true;
		}
		
		if (hint.onTouchUp(touchPos) && hint.id==4){
			hint.reset();
			return true;
		}
		return false;
	}
	
	@Override
	void initDragon(Dragon dragon) {
		dragon.setBoundsSizeOrientationAndPosition(3, 2.5f, 4, 4, Dragon.EAST);
	}

	@Override
	boolean checkPassed() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	void registerEnemies() {
		//DO NOTHING - no enemies in this lvl
	}

	@Override
	public void registerDrgaonHitListeners(IDragonHitListener... listeners) {
		//DO NOTHING - no enemies in this lvl
	}
}
