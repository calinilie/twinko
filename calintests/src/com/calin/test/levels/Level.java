package com.calin.test.levels;

import java.util.ArrayList;

import android.util.Log;

import com.calin.tests.IDeadTargetListener;
import com.calin.tests.IDragonHitListener;
import com.calin.tests.IDragonRendererListener;
import com.calin.tests.DynamicGameObject;
import com.calin.tests.GameObject;
import com.calin.tests.IPauseListener;
import com.calin.tests.IUsePotionListener;
import com.calin.tests.TargetController;
import com.calin.tests.datasource.Facade;
import com.calin.tests.food.Food;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.OverlapTester;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.Coins;
import com.calin.tests.objects.Combo;
import com.calin.tests.objects.CookingStateBar;
import com.calin.tests.objects.DamagePotion;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.DragonBag;
import com.calin.tests.objects.Enemy;
import com.calin.tests.objects.FailNotifier;
import com.calin.tests.objects.GameMenuItem;
import com.calin.tests.objects.HealingPotion;
import com.calin.tests.objects.Hint;
import com.calin.tests.objects.MenuButton;
import com.calin.tests.objects.NumbersDisplay;
import com.calin.tests.objects.Potion;
import com.calin.tests.objects.ScoreDisplay;
import com.calin.tests.objects.SlideController;
import com.calin.tests.objects.StaminaPotion;
import com.calin.tests.objects.StatsBar;
import com.calin.tests.objects.Target;
import com.calin.tests.objects.Timer;
import com.calin.tests.objects.UseStaminaPotion;
import com.calin.tests.particles.Fire;
import com.calin.tests.particles.FireflyEmitter;

public abstract class Level implements IDeadTargetListener, IUsePotionListener, IDragonHitListener, IPauseListener{
	
	private int levelId;
	private int categoryId;
	
	public static final byte RUNNING=0;
	public static final byte COMPLETED=1;
	public static final byte IN_GAME_PAUSED=2;
	public static final byte TUTORIAL_PAUSED=3;
	public byte state;
	private boolean levelOver=false;
	public boolean passed=false;
	
	public Hint hint;
	boolean firstHint=false;
	public FailNotifier failNotifier;

	TargetController targetController;
//	Target[] targets;
	public ArrayList<Target> targets;
	public Target currentTarget;
	DynamicGameObject currentTargetDummy;
	public ArrayList<Enemy> enemies;
	
	public ScoreDisplay scoreDisplay;
	public NumbersDisplay increaseScoreEffect, dragonHitEffect;
	public Timer timer;
	public int score;
	
	public Dragon dragon;
	public FireflyEmitter fireflies;
	
	boolean objectsRevealed=false;
	public Combo combo;
	
	
	public SlideController controller;

	public StatsBar staminaBar;
	public StatsBar hpBar;
	public UseStaminaPotion staminaButton;
	//use health potion
		
	private Circle latestTouch;
	
	float tempStamina;
	float tempHP;
	private final int currentHighScore;
	private final boolean previouslyPassed;
	public CompleteDialog completeDialog;
	public PausedDialog pauseDialog;
	public MenuButton pauseButton;
	public StoreDisplayModel storeDisplayModel;
	private final int XP;
	protected float playTime;
	float passedTime=0;
	
	public boolean isTouched;
	
	public Level(Dragon dragon, int currentHighScore, boolean previouslyPassed, int XP, float playTime, int id, int categoryId){
		this.levelId = id;
		this.categoryId = categoryId;
		initDragon(dragon);
		hint=new Hint(8, 4.5f, 7, 0);
		this.dragon=dragon;		
		this.currentHighScore=currentHighScore;
		this.previouslyPassed=previouslyPassed;
		state=RUNNING;
		this.XP=XP;
		this.playTime=playTime;
		
//		fire=new Fire();
		fireflies=new FireflyEmitter();
		
		controller=new SlideController(14.5f, 4.5f, 17, 4.5f, 9, 1);
		staminaBar=new StatsBar(0.5f, 1, dragon.dragonStats.maxStamina);
		hpBar=new StatsBar(0.95f, 1, dragon.dragonStats.maxHP);
		staminaButton=new UseStaminaPotion(0.75f, 7, 1.5f, 1.5f);
		combo=new Combo(8, 4.5f, 6, 1.5f);
		
		dragon.dragonBag.subscribeListeners(this);
		storeDisplayModel=new StoreDisplayModel(dragon.dragonBag);
		pauseButton=new MenuButton(8, 1, 2, 2);
		tempStamina=dragon.dragonStats.stamina;
		tempHP=dragon.dragonStats.hp;
		currentTargetDummy=new DynamicGameObject(-1, -1, 0, 0);
		scoreDisplay=new ScoreDisplay(3, 7, -1, 7);
		increaseScoreEffect=new NumbersDisplay(3, 7, 1);
		dragonHitEffect=new NumbersDisplay(0, 0, 1);
		timer=new Timer(6, 1, 6, -3);
		latestTouch=new Circle(-1, -1, 0.65f);
		completeDialog=new CompleteDialog(8, 4.5f, 8, 8);
		pauseDialog = new PausedDialog(8, 4.5f, 9, 9, this.levelId);
		pauseDialog.setPausedListener(this);
		Log.d("stats", ">>level Start: "+this.previouslyPassed+" "+this.currentHighScore);
		
		targets=new ArrayList<Target>();
		enemies=new ArrayList<Enemy>();

	}
	
	public void update(float deltaTime, Vector2 touchPos, boolean isTouched){
		switch (state){
		case RUNNING:
			updateRunning(deltaTime, touchPos, isTouched);
			break;
		case COMPLETED:
			updateCompleted(deltaTime, touchPos, isTouched);
			break;
		case IN_GAME_PAUSED:
			updateInGamePaused(deltaTime, touchPos, isTouched);
			break;
		case TUTORIAL_PAUSED:
			updateTutorial(deltaTime);
			break;
		}
	}
		
	protected void updateRunning(float deltaTime, Vector2 touchPos, boolean isTouched){
		latestTouch.center.set(touchPos);
		this.isTouched=isTouched && OverlapTester.overlapCircleRectangle(latestTouch, controller.bar.bounds);
		
		controller.update(latestTouch, deltaTime, this.isTouched);
		
		playTime-=deltaTime;
		timer.update((int) playTime, deltaTime);
		
		targetController.update(deltaTime);
		
		if (currentTarget!=null){
			fireflies.update(deltaTime, currentTarget, currentTarget.SPEED);
			dragon.fireEmitter.setIntensity(controller.distance);
			
			this.setCurrentTargetDummy(this.currentTarget);
			checkFireColision();
		}
		
		dragon.update(this.currentTargetDummy, this.isTouched, tempStamina, deltaTime);			
		
		if (currentTarget instanceof Food)
			CookingStateBar.update((Food)currentTarget);
		
//		fire.update(deltaTime, this.currentTargetDummy, this.isTouched, tempStamina>0 ? true : false, dragon.head.mouth.fireOK);
		
		staminaBar.update(tempStamina);
		hpBar.update(tempHP);
		
		checkAndDropStamina(this.isTouched, dragon.fireEmitter.session);
		
		scoreDisplay.update(score, deltaTime);
		increaseScoreEffect.updateGrowFade(deltaTime);
		dragonHitEffect.updateGrowFade(deltaTime);
		combo.pulseTwice(deltaTime);
		
		if (pauseButton.update(deltaTime)){
			state = IN_GAME_PAUSED;
			pauseDialog.show();
		}
		
		if (failNotifier!=null) failNotifier.update(deltaTime);
		if (playTime<=0) this.state=COMPLETED;
	}
	
	protected void updateCompleted(float deltaTime, Vector2 touchPos, boolean isTouched){
		if (!levelOver){
			//TODO check how many times this is called
			Log.d(">>>>>>>>>>>>>>", "level complete called");
			passed=checkPassed() && (tempHP>0);
//			Log.d("dragon", passed+"");
			boolean levelUp=this.rewardAndCheckNewLvl(XP, passed);
			boolean isNewHighScore=false;
			if (passed){
				dragon.dragonStats.setSomeStats(tempStamina, tempHP);
				isNewHighScore=score>currentHighScore;
				LevelIconsDisplayModel.unlockNextLevels(this.levelId, this.categoryId);
			}
			completeDialog.setFinalData(passed, isNewHighScore, previouslyPassed, levelUp, score, XP, XP);
			completeDialog.pop();
//			Log.d("stats", "## Level over" + dragon.dragonStats.toString());
		}
		if (failNotifier!=null) failNotifier.update(deltaTime);
		completeDialog.update(deltaTime);
	}
	
	abstract boolean checkPassed();
	
	protected void updateInGamePaused(float deltaTime, Vector2 touchPos, boolean isTouched){
		pauseDialog.update(deltaTime);
//		storeDisplayModel.update(deltaTime);
	}
	
	public boolean onTouchUp(Vector2 touchPos){
		boolean result=false;
		this.latestTouch.center.set(touchPos);
		switch (state){
		case RUNNING:
			pauseButton.onTouchDown(latestTouch);
			if (OverlapTester.overlapCircleRectangle(latestTouch, staminaButton.bounds)){
				usePotion(DragonBag.STAMINA_POTION);
			}
			if (OverlapTester.overlapCircleRectangle(latestTouch, dragon.bounds)){
				usePotion(DragonBag.DAMAGE_POTION);
			} 
			for (Target t: targets) {
				if (t.checkCollision(latestTouch)) {
					setCurrentTarget(t);
					fireflies.init(t.position);
					break;
				}
			}
			break;
		case IN_GAME_PAUSED:
			pauseDialog.onTouchDown(latestTouch);
			break;
		case TUTORIAL_PAUSED:
			if (onTouchUpTutorial(latestTouch)) state=Level.RUNNING;
			break;
		}
		return result;
	}
	
	boolean onTouchUpTutorial(Circle touchPos) {
		if (hint.onTouchUp(touchPos)){
			return true;
		}
		return false;
	}
	
	void updateTutorial(float deltaTime) {
		dragon.update(this.currentTargetDummy, this.isTouched, tempStamina, deltaTime);
		if (passedTime>=0.5f && !firstHint){
			hint.show();
			firstHint=true;
		}
		passedTime+=deltaTime;
		hint.update(deltaTime);
	}
	
	private boolean onTouchUpStore(){
//		if (OverlapTester.overlapCircleRectangle(latestTouch, storeDisplayModel.buyStaminaPotion.bounds)){
//			dragon.buyPotion(DragonBag.STAMINA_POTION);
//			return false;
//		}
//		if (OverlapTester.overlapCircleRectangle(latestTouch, storeDisplayModel.buyHealingPotion.bounds)){
//			dragon.buyPotion(DragonBag.HEALING_POTION);
//			return false;
//		}
//		if (OverlapTester.overlapCircleRectangle(latestTouch, storeDisplayModel.buyDamagePotion.bounds)){
//			dragon.buyPotion(DragonBag.DAMAGE_POTION);
//			return false;
//		}
//		if (OverlapTester.overlapCircleRectangle(latestTouch, exitButton.bounds)) return true;
		return false;
	}
	
	private boolean rewardAndCheckNewLvl(int XP, boolean nowPassed){
		this.levelOver=true;
		boolean newHighScore=score>currentHighScore;
		if (nowPassed){
			if (previouslyPassed){
				if (newHighScore){
					Log.d("stats", "rewared * 2");
					return this.reward(score*2, XP*2);
				}
				else {
					Log.d("stats", "reward");
					return this.reward(score, XP);
				}
			}
			else {
				Log.d("stats", "reward * 5");
				return this.reward(score*5, XP*5);
			}
		}
		else{
			Log.d("stats", "reward / 2");
			return this.reward(score/2, XP/2);
		}
	}
	
	/**
	 * increases dragon.XP and dragonbag.goldCoins
	 * @param coins
	 * @param xp
	 * @return true if levelUp, false otherwise
	 */
	private boolean reward(int coins, int xp){
		dragon.dragonBag.increaseGoldCoins(coins);
		return dragon.dragonStats.checkNewLevel(xp);
	}
	
	abstract void initDragon(Dragon dragon);
	
	private void usePotion(byte id){
		switch(id){
		case DragonBag.HEALING_POTION:
			dragon.dragonBag.useHealingPotion();
			break;
		case DragonBag.STAMINA_POTION:
			dragon.dragonBag.useStaminaPotion();
			break;
		case DragonBag.DAMAGE_POTION:
			dragon.dragonBag.useDamagePotion();
			break;
		}
	}
	
	protected void setCurrentTarget(Target other){
		this.currentTarget=other;
	}
	
	protected void setCurrentTargetDummy(DynamicGameObject other){
		this.currentTargetDummy=other;
	}
	
	@Override
	public void onDeadTargetEvent(Target source, int score) {
		if (source==currentTarget){
			currentTarget=null;
			currentTargetDummy=null;
			int newscore=combo.onDeadtargetEvent(source, score);
			this.score+=newscore;
			if (score>0) increaseScoreEffect.animate(source.position.x, (source.position.y < 1) ? 1 :source.position.y, newscore, 0.06f, 1f);
			
		}
	}
	
	@Override
	public void onPotionUsedEvent(Potion source) {
		if (source instanceof HealingPotion){
			this.tempHP+=HealingPotion.plusHealing;
			if (tempHP>dragon.dragonStats.maxHP) tempHP=dragon.dragonStats.maxHP;
			return;
		}
		if (source instanceof StaminaPotion){
			this.tempStamina+=StaminaPotion.plusStamina;
			if (tempStamina>dragon.dragonStats.maxStamina) tempStamina=dragon.dragonStats.maxStamina;
			staminaBar.incBy(StaminaPotion.plusStamina); 
			return;
		}
		if (source instanceof DamagePotion){
			dragon.dragonStats.increaseDamageTemporarily();
			Log.d("stats", "&& damage potion " + dragon.dragonStats.toString());
		}
	}
	
	@Override
	public void onDragonHitEvent(Enemy source, float damage){
		tempHP-=damage;
		if (tempHP<=0){
			tempHP=0;
			state=COMPLETED;
		}
		hpBar.update(tempHP);
		dragonHitEffect.animate(hpBar.x+0.5f, hpBar.y+hpBar.currentHeight, damage, 0.03f, 0.6f);
//		Log.d("dragon", "dragon HIT >>>>>>>>>>"+tempHP);
	}
	
	private void checkFireColision(){
//		try{
		for (int i = 0; i < Fire.particleNumber; i++) {
			if (currentTarget!=null){
				if (OverlapTester.overlapRectangles(currentTarget.bounds, Fire.particles[i].bounds)) {
					Fire.particles[i].hit();
					currentTarget.takeDamage(dragon.dragonStats.damage);
				}
			}
		}
//		}
//		catch (NullPointerException e) {
//			Log.d("NULL", ">>>>>>>>>"+e.getMessage());
//		}
	}
	
	private void checkAndDropStamina(boolean isTouched, int particlesPerShot){
		if (isTouched && tempStamina>0 && currentTarget!=null) {
			tempStamina-=dragon.dragonStats.staminaDropRate*particlesPerShot;
		}
		else if (tempStamina<0) tempStamina=0;
	}
	
	public void revealObjects(float time){
		if (!objectsRevealed && playTime<time){
			timer.movingState=GameMenuItem.MOVE;
			scoreDisplay.movingState=GameMenuItem.MOVE;
			controller.movingState=GameMenuItem.MOVE;
			objectsRevealed=true;
		}
	}
	
	protected boolean removeTarget(Food food){
		if (targetController.removeFood(food)){
			targets.remove(food);
			return true;
		}
		return false;
	}
	
	public boolean isLevelOver(){
		return levelOver;
	}
	
	abstract void registerEnemies();	
	
	public abstract void registerDrgaonHitListeners(IDragonHitListener...listeners);
	
	@Override
	public void onContinuePressed() {
		state = RUNNING;
	}

	@Override
	public void onReplayPressed(int levelid) {
		
		
	}

	@Override
	public void onShopPressed() {
		// TODO Auto-generated method stub
		
	}
}
