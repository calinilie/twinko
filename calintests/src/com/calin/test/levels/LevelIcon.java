package com.calin.test.levels;

import android.util.Log;

import com.calin.tests.GameObject;

public class LevelIcon extends GameObject{

	byte state;
	public static final byte grow=1;
	public static final byte shrink=2;
	public static final byte normal=3;
	float maxSize;
	float incRate;
	public boolean isUnlocked;
	
	public LevelIcon(float x, float y, float width, float height) {
		super(x, y, 0, 0);
		maxSize=width;
		incRate=16;
	}
	
	public void update(float deltaTime, LevelIcon next){
		switch (state){
		case grow:
			this.bounds.width+=incRate*deltaTime;
			this.bounds.height+=incRate*deltaTime;
			if (bounds.width >= maxSize+maxSize*0.2){
				state=shrink;
				if (next!=null) next.state=grow;
			}
			break;
		case shrink:
			this.bounds.width-=incRate*deltaTime;
			this.bounds.height-=incRate*deltaTime;
			if (bounds.width <= maxSize) {
				state=normal;
			}
			break;
		}
	}
	
}