package com.calin.test.levels;

import java.util.ArrayList;

import android.util.Log;

import com.calin.tests.IDragonHitListener;
import com.calin.tests.IDragonRendererListener;
import com.calin.tests.TargetController;
import com.calin.tests.food.Food;
import com.calin.tests.food.Mushroom;
import com.calin.tests.food.Stake;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.Bee;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.Enemy;
import com.calin.tests.objects.Hint;
import com.calin.tests.objects.Target;

public class Level02 extends Level{
	
	public Mushroom mushroom;
	public Bee bee;
	boolean beeTutorial=false;
	boolean staminaTutorial=false;

	public Level02(Dragon dragon, int currentHighScore,
			boolean previouslyPassed) {
		super(dragon, currentHighScore, previouslyPassed, 30, 40, 2, LevelIconsDisplayModel.MEDOW_LEVELS);
		state=TUTORIAL_PAUSED;
		hint=new Hint(7, 4.5f, 7, 5);		
		
		mushroom=new Mushroom(1, 1, 2, 2, 25, 8f, 5, Stake.BOTTOM_LEFT, 10);
		bee=new Bee(14, 1.5f, 0.75f, 0.75f, 10, 5, 7, 10, 2,super.dragon.position, super.dragon.bounds, new Vector2(3.2f, 2.6f));		
		bee.subscribeDragonHitListeners(this);
		
		targetController=new TargetController(1, 1);
		targetController.food=new ArrayList<Food>();
		targetController.food.add(mushroom);
		targetController.enemies=new ArrayList<Enemy>();
		targetController.enemies.add(bee);
		
		targets=new ArrayList<Target>();
		targets.add(mushroom);
		targets.add(bee);
		registerEnemies();
		
		mushroom.subscribeDeadTargetListeners(this, targetController);
		bee.subscribeDeadTargetListeners(this, targetController);
		super.setCurrentTarget(mushroom);
	}
	
	@Override
	protected void updateRunning(float deltaTime, Vector2 touchPos, boolean isTouched){
		beeListener();
		staminaListener();
		
		bee.update(deltaTime, isTouched);
		mushroom.update(deltaTime, isTouched);
		this.revealObjects(39.3f);
		super.updateRunning(deltaTime, touchPos, isTouched);
	}
	
	private void beeListener(){
		if (super.playTime<30.6f && !beeTutorial){
			beeTutorial=true;
			state=TUTORIAL_PAUSED;
			hint.show(6);
		}
	}
	
	private void staminaListener(){
		if (super.playTime<15 && !staminaTutorial){
			staminaTutorial=true;
			tempStamina=10;
			state=TUTORIAL_PAUSED;
			hint.show(7);
		}
	}

	@Override
	void updateTutorial(float deltaTime) {
		dragon.update(this.currentTargetDummy, this.isTouched, tempStamina, deltaTime);
		if (hint.id>=7) staminaBar.updatePulse(deltaTime);
		if (passedTime>0.5f && !firstHint) {
			hint.show(5);
			firstHint=true;
		}
		passedTime+=deltaTime;
		hint.update(deltaTime);
	}

	@Override
	boolean onTouchUpTutorial(Circle touchPos) {
		if (hint.onTouchUp(touchPos) && hint.id==5){
			hint.reset();
			hint.id=6;
			return true;
		}
		
		if (hint.onTouchUp(touchPos)&& hint.id==6){
			hint.reset();
			hint.id=7;
			return true;
		}
		
		if (hint.onTouchUp(touchPos) && hint.id==7){
			hint.reset();
			hint.show(8);
			return false;
		}
		
		if (hint.onTouchUp(touchPos) && hint.id==8){
			hint.reset();
			return true;
		}
		return false;
	}

	@Override
	void initDragon(Dragon dragon) {
		dragon.setBoundsSizeOrientationAndPosition(13, 2.5f, 4, 4, Dragon.WEST);
	}

	@Override
	boolean checkPassed() {
		return true;
	}

	@Override
	void registerEnemies() {
		enemies.add(bee);
	}
	
	@Override
	public void registerDrgaonHitListeners(IDragonHitListener...listeners){
		for (Enemy e:enemies){
			e.subscribeDragonHitListeners(listeners);
		}
	}


}
