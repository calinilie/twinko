package com.calin.tests;

import com.calin.tests.objects.Potion;

public interface IUsePotionListener {

	public void onPotionUsedEvent(Potion source);
	
}
