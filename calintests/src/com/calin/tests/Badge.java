package com.calin.tests;

public class Badge extends GameObject{
		public boolean visible;
		public byte state;
		public static final byte HIDDEN=0;
		public static final byte NORMAL=2;
		public static final byte SHRINK=3;
		public float currentHeight;
		public float currentWidth;
		float heightDropRate;
		float widthDropRate;
		final float accelHeightDropRate;
		final float accelWidthDropRate;
		
		public Badge(float x, float y, float width, float height) {
			super(x, y, width, height);
			visible=false;
			state=SHRINK;
			this.currentHeight=height*3;
			this.currentWidth=width*3;
			this.heightDropRate=height;
			this.widthDropRate=width;
			this.accelHeightDropRate=heightDropRate*8;
			this.accelWidthDropRate=widthDropRate*8;
		}
		
		public void show(){
			visible=true;
		}
		
		public void tryShow(boolean show){
			if (show){
				visible = true;
			}
		}
		
		public void update(float deltaTime){
			if (visible){
				switch (state){
				case SHRINK:
					shrink(deltaTime);
					if (currentHeight<=bounds.height || currentWidth<=bounds.width){
						state=NORMAL;
					}
					break;
				}
			}
		}
		
		public void shrink(float deltaTime){
			widthDropRate+=accelWidthDropRate*deltaTime;
			heightDropRate+=accelHeightDropRate*deltaTime;
			this.currentWidth-=widthDropRate*deltaTime;
			this.currentHeight-=heightDropRate*deltaTime;
		}
		
		public boolean isNormalState(){
			return state == NORMAL;
		}
}


