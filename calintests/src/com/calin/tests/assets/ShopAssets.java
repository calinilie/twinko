package com.calin.tests.assets;

import com.calin.tests.framework.gl.Texture;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.framework.impl.GLGame;

public class ShopAssets {
	
	public static Texture background;
	public static TextureRegion backgroundRegion;
	
	public static Texture itemDescAtlas;
	public static TextureRegion itemDescriptionBackground;
	
	public static Texture atlas;
	public static TextureRegion staminaPotion;
	public static TextureRegion healingPotion;
	public static TextureRegion damagePotion;
	public static TextureRegion selected;
	public static TextureRegion smallGold;
	public static TextureRegion mediumGold;
	public static TextureRegion largeGold;
	public static TextureRegion potionsCategory;
	public static TextureRegion goldCategory;
	
	public static void load (GLGame glGame){
		atlas=new Texture(glGame, "potions.png");
		background=new Texture(glGame, "bluebackground.png");
		itemDescAtlas=new Texture(glGame, "item_desc_back.png");
		init(256);
	}
	
	static void init(int unit){
		backgroundRegion=new TextureRegion(background, 0, 0, unit*8, unit*4);
		
		itemDescriptionBackground=new TextureRegion(itemDescAtlas, 0, 0, 4*unit, 2*unit);
		
		staminaPotion=new TextureRegion(atlas, unit, 0, unit, unit);
		healingPotion=new TextureRegion(atlas, 0, 0, unit, unit);
		damagePotion=new TextureRegion(atlas, 0, unit, unit, unit);
		selected=new TextureRegion(atlas, unit, unit, unit, unit);
		largeGold=new TextureRegion(atlas, 3*unit, 0, unit, unit);
		mediumGold=new TextureRegion(atlas, 3*unit, unit, unit, unit);
		potionsCategory=new TextureRegion(atlas, 0, 2*unit, unit, unit);
		goldCategory=new TextureRegion(atlas, unit, 2*unit, unit, unit);
		
		unit/=2;
		
		smallGold=new TextureRegion(atlas, 4*unit, 0, unit, unit);
	}
	
	public static void reload(){
		atlas.reload();
		background.reload();
		itemDescAtlas.reload();
	}
}
