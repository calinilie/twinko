package com.calin.tests.assets;

import com.calin.tests.framework.gl.Animation;
import com.calin.tests.framework.gl.Texture;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.framework.impl.GLGame;

public class BatAssets {
	
	public static Texture batAtlas;
	public static TextureRegion wings1;
	public static TextureRegion wings2;
	public static TextureRegion wings3;
	public static TextureRegion wings4;
	public static TextureRegion wings5;
	public static TextureRegion headNormal;
	public static TextureRegion headNormalEyesClosed;
	public static TextureRegion headAngry;
	public static TextureRegion headAngryEyesClosed;
	public static Animation batAnimation;
	
	public static void load(GLGame glGame){
		batAtlas=new Texture(glGame, "bat_atlas_hd_big.png");
		init(512);
	}
	
	private static void init(int unit){
		wings1=new TextureRegion(batAtlas, 0, 0, unit, unit);
		wings2=new TextureRegion(batAtlas, unit, 0, unit, unit);
		wings3=new TextureRegion(batAtlas, 0, unit, unit, unit);
		wings4=new TextureRegion(batAtlas, unit, unit, unit, unit);
		wings5=new TextureRegion(batAtlas, 0, 2*unit, unit, unit);
		headAngry=new TextureRegion(batAtlas, unit, 2*unit, unit/2, unit/2);
		headNormal=new TextureRegion(batAtlas, (int) (unit*1.5), 2*unit, unit/2, unit/2);
		headAngryEyesClosed=new TextureRegion(batAtlas, unit, (int) (unit*2.5), unit/2, unit/2);
		headNormalEyesClosed=new TextureRegion(batAtlas, (int)(unit*1.5), (int)(unit*2.5), unit/2, unit/2);
		batAnimation=new Animation(0.05f, wings1, wings2, wings3, wings4, wings5, wings4, wings3, wings2, wings1);
	}

}
