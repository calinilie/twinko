package com.calin.tests.assets;

import com.calin.tests.framework.gl.Texture;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.framework.impl.GLGame;

public class StoreAssets {
	
	public static Texture atlas;
	public static TextureRegion shelfRegion;
	public static TextureRegion staminaPotionRegion;
	public static TextureRegion healingPotionRegion;
	public static TextureRegion damagePotionRegion;
	public static TextureRegion buyButtonRegion;
	
	public static Texture buttonsAtlas;
	public static TextureRegion backButtonRegion;
	public static TextureRegion coinRegion;
	public static TextureRegion[] digitsAssets;
	public static TextureRegion medow;
	public static TextureRegion jungle;
	public static TextureRegion cave;
	public static TextureRegion powerOff;
	public static TextureRegion redX;
	

	
	public static void load(GLGame game){
		atlas=new Texture(game, "store_atlas.png");
		shelfRegion=new TextureRegion(atlas, 0, 0, 128, 32);
		staminaPotionRegion=new TextureRegion(atlas, 0, 32, 32, 32);
		healingPotionRegion=new TextureRegion(atlas, 32, 32, 32, 32);
		damagePotionRegion=new TextureRegion(atlas, 64, 32, 32, 32);
		buyButtonRegion=new TextureRegion(atlas, 96, 32, 32, 32);
		
		buttonsAtlas = new Texture((GLGame) game, "digits.png");
		digitsAssets = new TextureRegion[11];
		int x=0;
		int y=0;
		for (int i = 0; i < digitsAssets.length; i++) {
			digitsAssets[i]=new TextureRegion(buttonsAtlas, x, y, 32, 32);
			x+=32;
			if (i==7) {
				y+=32;
				x=0;
			}
		}
		coinRegion=new TextureRegion(buttonsAtlas, 128, 352, 32, 32);
		medow=new TextureRegion(buttonsAtlas, 256, 128, 256, 128);
		jungle=new TextureRegion(buttonsAtlas, 256, 256, 256, 128);
		cave=new TextureRegion(buttonsAtlas, 256, 384, 256, 128);
		powerOff=new TextureRegion(buttonsAtlas, 64, 416, 64, 64);
		redX=new TextureRegion(buttonsAtlas, 128, 480, 32, 32);
	}
	
	public static void reload(){
		atlas.reload();
		buttonsAtlas.reload();
	}

}
