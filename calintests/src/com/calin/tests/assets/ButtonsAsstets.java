package com.calin.tests.assets;

import com.calin.tests.framework.Sound;
import com.calin.tests.framework.gl.Texture;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.framework.impl.GLGame;

public class ButtonsAsstets {
	
	public static Texture atlas;
	
	public static TextureRegion replay;
	public static TextureRegion ok;
	public static TextureRegion back;
	public static TextureRegion buy;

	public static void load (GLGame glGame){
		atlas=new Texture(glGame, "buttons.png");
		init(256);
	}
	
	static void init(int unit){
		replay=new TextureRegion(atlas, 0, 0, unit, unit);
		ok=new TextureRegion(atlas, unit, 0, unit, unit);
		back=new TextureRegion(atlas, unit, unit, unit, unit);
		buy=new TextureRegion(atlas, 0, unit, unit, unit);
	}
	
	public static void reload(){
		atlas.reload();
	}
	
	public static void playSound(Sound sound) {
        sound.play(1);
    }
	
}
