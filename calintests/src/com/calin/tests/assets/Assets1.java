package com.calin.tests.assets;

import com.calin.tests.framework.gl.Animation;
import com.calin.tests.framework.gl.Texture;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.framework.impl.GLGame;

public class Assets1 {
	public static final float FRUSTRUM_WIDTH = 16;
	public static final float FRUNSTRUM_HEIGHT = 9;
	
	public static Texture tutorialAtlas;
	public static TextureRegion hint1, hint2, hint3, hint4, okButton, hint6 ,hint5, hint7, hint8, noFoodShallPass, hintLevel4, hintLevel6, hintLevel7, hintLevel8;
	
	public static Texture medowBackgroundAtlas;
	public static TextureRegion medowBackground;
	
	public static Texture dragonAtlas;
	public static TextureRegion dragon;
	public static TextureRegion dragonBody;
	
	public static Texture particlesAtlas; 
	public static TextureRegion particleRegion; 
	
	public static Texture digitsAtlas; 
	public static TextureRegion dialog; 
	public static TextureRegion highscore; 
	public static TextureRegion maxStaminaTexture;
	public static TextureRegion yourScoreTexture; 
	public static TextureRegion broccoliRegion; 
	public static TextureRegion slideBarRegion; 
	public static TextureRegion slideButtonRegion;
	public static TextureRegion yellowRegion; 
	public static TextureRegion yellowDrop; 
	public static TextureRegion inStorePausedRegion; 
	public static TextureRegion exitButtonRegion; 
	public static TextureRegion stakeRegion; 
//	public static TextureRegion mushroomRegion; 
	public static TextureRegion lifeBarRegion;
	public static TextureRegion[] digits;
	public static TextureRegion fireflyRegionSmall; 
	public static TextureRegion fireflyRegion; 
	public static TextureRegion barContainer;
	public static TextureRegion redX;
	public static TextureRegion blueRegion;
	public static TextureRegion comboX2,comboX3,comboX4,comboX5;
	public static TextureRegion incTexture;
	public static TextureRegion dmgTexture;
	public static TextureRegion hpTexture;
	public static TextureRegion skillPointsTexture;

	public static Texture lifeBarAtlas;
	public static TextureRegion lifeBarTexture;

	
//	public static Texture black;
//	public static TextureRegion blackRegion;
	
	public static void load(GLGame game){
		int width = (game).getGLGraphics().getWidth();
		
		if (width<800){
			medowBackgroundAtlas=new Texture(game, "medow_background.png");
			medowBackground=new TextureRegion(medowBackgroundAtlas, 0, 0, 512, 256);
			
			dragonAtlas = new Texture((GLGame) game, "dragON_small.png");
			dragon = new TextureRegion(dragonAtlas, 0, 0, 256, 256);
			dragonBody=new TextureRegion(dragonAtlas, 256, 0, 128, 128);
			
			
		}
		else {
			medowBackgroundAtlas=new Texture(game, "medow_background_big.png");
			medowBackground=new TextureRegion(medowBackgroundAtlas, 0, 0, 1024, 512);
			
			dragonAtlas = new Texture((GLGame) game, "dragON.png");
			dragon = new TextureRegion(dragonAtlas, 0, 0, 512, 512);
			dragonBody=new TextureRegion(dragonAtlas, 512, 0, 256, 256);
		}
		
		tutorialAtlas=new Texture(game, "tutorial_atlas.png");
		hint1=new TextureRegion(tutorialAtlas, 0, 0, 256, 256);
		hint2=new TextureRegion(tutorialAtlas, 0, 256, 256, 256);
		hint3=new TextureRegion(tutorialAtlas, 0, 512, 256, 256);
		hint4=new TextureRegion(tutorialAtlas, 0, 768, 256, 256);
		okButton=new TextureRegion(tutorialAtlas, 256, 0, 64, 64);
		hint5=new TextureRegion(tutorialAtlas, 256, 256, 256, 256);
		hint7=new TextureRegion(tutorialAtlas, 256, 512, 256, 256);
		hint8=new TextureRegion(tutorialAtlas, 512, 512, 256, 256);
		hint6=new TextureRegion(tutorialAtlas, 512, 0, 256, 256);
		hintLevel4=new TextureRegion(tutorialAtlas, 512, 512, 256, 256);
		hintLevel6=new TextureRegion(tutorialAtlas, 512, 768, 256, 256);
		hintLevel8=new TextureRegion(tutorialAtlas, 768, 0, 256, 256);
		hintLevel7=new TextureRegion(tutorialAtlas, 768, 256, 256, 256);
		noFoodShallPass=new TextureRegion(tutorialAtlas, 512, 256, 256, 256);
		
//		particlesAtlas=new Texture((GLGame)game, "s1small.png");
		particlesAtlas=new Texture((GLGame)game, "particle.png");
		particleRegion = new TextureRegion(particlesAtlas, 0, 0, 16, 16);
//		blackRegion=new TextureRegion(particlesAtlas, 16, 0, 16, 16);

		lifeBarAtlas=new Texture((GLGame)game, "lifeBar.png");
		lifeBarTexture=new TextureRegion(lifeBarAtlas, 0, 0, 256, 16);
		
		
		
		digitsAtlas = new Texture((GLGame) game, "digits.png");
		fireflyRegionSmall=new TextureRegion(digitsAtlas, 160, 352, 16, 16);
		fireflyRegion=new TextureRegion(digitsAtlas, 192, 352, 32, 32);
		stakeRegion = new TextureRegion(digitsAtlas, 0, 416, 64, 64);
//		mushroomRegion = new TextureRegion(digitsAtlas, 128, 416, 64, 64);
		broccoliRegion=new TextureRegion(digitsAtlas, 192, 416, 64, 64);
//		lifeBarRegion = new TextureRegion(digitsAtlas, 160, 368, 32, 16);
		dialog=new TextureRegion(digitsAtlas, 256, 0, 128, 128);
		highscore=new TextureRegion(digitsAtlas, 384, 0, 128, 128);
		yourScoreTexture=new TextureRegion(digitsAtlas, 0, 128, 256, 64);
		slideBarRegion=new TextureRegion(digitsAtlas, 0, 352, 32, 32);
		slideButtonRegion=new TextureRegion(digitsAtlas, 32, 352, 32, 32);
		yellowRegion=new TextureRegion(digitsAtlas, 32, 512, 32, 256);
		blueRegion=new TextureRegion(digitsAtlas, 64, 512, 32, 256);
		yellowDrop=new TextureRegion(digitsAtlas, 128, 320, 32, 32);
		inStorePausedRegion=new TextureRegion(digitsAtlas, 0, 384, 128, 32);
		exitButtonRegion=new TextureRegion(digitsAtlas, 96, 352, 32, 32);
		barContainer=new TextureRegion(digitsAtlas, 0, 512, 32, 256);
		redX=new TextureRegion(digitsAtlas, 128, 480, 32, 32);
		comboX2=new TextureRegion(digitsAtlas, 256, 512, 256, 64);
		comboX3=new TextureRegion(digitsAtlas, 256, 576, 256, 64);
		comboX4=new TextureRegion(digitsAtlas, 256, 640, 256, 64);
		comboX5=new TextureRegion(digitsAtlas, 256, 704, 256, 64);
		maxStaminaTexture = new TextureRegion(digitsAtlas, 0, 256, 128, 32);
		dmgTexture=new TextureRegion(digitsAtlas, 128, 256, 128, 32);
		hpTexture=new TextureRegion(digitsAtlas, 0, 288, 128, 32);
		incTexture=new TextureRegion(digitsAtlas, 128, 288, 32, 32);
		skillPointsTexture=new TextureRegion(digitsAtlas, 0, 320, 128, 32);
		digits = new TextureRegion[11];
		int x=0;
		int y=0;
		for (int i = 0; i < digits.length; i++) {
			digits[i]=new TextureRegion(digitsAtlas, x, y, 32, 32);
			x+=32;
			if (i==7){
				x=0;
				y+=32;
			}
		}
	}
	
	public static void reload(){
		medowBackgroundAtlas.reload();
		dragonAtlas.reload();
		digitsAtlas.reload();
		particlesAtlas.reload();
		lifeBarAtlas.reload();
		tutorialAtlas.reload();
	}
}
