package com.calin.tests.assets;

import com.calin.tests.framework.gl.Animation;
import com.calin.tests.framework.gl.Texture;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.framework.impl.GLGame;

public class BeeAssets {
	
	public static Texture beeAtlas;
	public static TextureRegion headAngry;
	public static TextureRegion headAngryEyesClosed;
	public static TextureRegion handsAngry;
	public static TextureRegion headNormal;
	public static TextureRegion headNormalEyesClosed;
	public static TextureRegion handsNormal;
	public static TextureRegion body;
	public static TextureRegion wings1;
	public static TextureRegion wings2;
	public static TextureRegion wings3;
	public static TextureRegion wings4;
	public static TextureRegion wings5;
	public static Animation wings;
	
	public static void load(GLGame glGame){
		int width = (glGame).getGLGraphics().getWidth();
		if (width<800){
			beeAtlas=new Texture(glGame, "bee_atlas_small.png");
			initBeeAssets(32);
		}
		else{
			beeAtlas=new Texture(glGame, "bee_atlas_hd.png");
			initBeeAssets(128);
		}
	}
	
	static void initBeeAssets(int unit){
		headNormal=new TextureRegion(beeAtlas, 0, 0, unit, unit);
		headAngry=new TextureRegion(beeAtlas, unit, 0, unit, unit);
		body=new TextureRegion(beeAtlas, 2*unit, 0, unit, unit);
		wings5=new TextureRegion(beeAtlas, 3*unit, 0, unit, unit);
		wings4=new TextureRegion(beeAtlas, 3*unit, unit, unit, unit);
		wings3=new TextureRegion(beeAtlas, 2*unit, unit, unit, unit);
		wings2=new TextureRegion(beeAtlas, unit, unit, unit, unit);
		wings1=new TextureRegion(beeAtlas, 0, unit, unit, unit);
		wings=new Animation(0.01f, wings1, wings2, wings3, wings4, wings5, wings4, wings3, wings2, wings1);
		handsAngry=new TextureRegion(beeAtlas, 0, 2*unit, unit, unit);
		handsNormal=new TextureRegion(beeAtlas, unit, 2*unit, unit, unit);
		headNormalEyesClosed=new TextureRegion(beeAtlas, 2*unit, 2*unit, unit, unit);
		headAngryEyesClosed=new TextureRegion(beeAtlas, 3*unit, 2*unit, unit, unit);
	}
	
	public static void reload(){
		beeAtlas.reload();
	}
}
