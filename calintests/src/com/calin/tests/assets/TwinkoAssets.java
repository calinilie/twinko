package com.calin.tests.assets;

import com.calin.tests.framework.Sound;
import com.calin.tests.framework.gl.Animation;
import com.calin.tests.framework.gl.OneTimeAnimation;
import com.calin.tests.framework.gl.Texture;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.framework.impl.GLGame;

public class TwinkoAssets {
	
	public static Texture atlas;
	public static TextureRegion head;
	public static TextureRegion body;
	
	public static TextureRegion tail;
	public static TextureRegion wing;
	
	public static TextureRegion mouthClosed;
	public static TextureRegion mouthNormal2;
	public static TextureRegion mouthNormal3;
	public static TextureRegion mouthOpened;
	public static TextureRegion mouthFire3;
	public static TextureRegion mouthFire2;
	public static TextureRegion mouthFire;
	public static TextureRegion mouthSad;
	public static TextureRegion mouthConfused;
	public static TextureRegion mouthScared;
	
	public static TextureRegion eyesNormalOpened;
	public static TextureRegion eyesNormalClosed;
	public static TextureRegion eyesAngry;
	public static TextureRegion eyesFireBreathing;
	public static TextureRegion eyesConfused;
	public static TextureRegion eyesSad;
	public static TextureRegion eyesLeftOpened;
	public static TextureRegion eyesRightOpened;
	public static TextureRegion eyesClosed;
	
	public static TextureRegion rightFoot;
	public static TextureRegion leftFoot;
	
	public static TextureRegion leftArmBase;
	public static TextureRegion rightArmBase;
	public static TextureRegion rightArmBase2;
	
	static TextureRegion rightArmUp2;
 	static TextureRegion rightArmUp3;
 	static TextureRegion rightArmUp4;
 	static TextureRegion rightArmUp5;
 	public static TextureRegion rightArmUp6;

 	static TextureRegion rightArmConfused2;
 	static TextureRegion rightArmConfused3;
 	static TextureRegion rightArmConfused4;
 	static TextureRegion rightArmConfused5;

 	static TextureRegion rightArmFlex2;
 	static TextureRegion rightArmFlex3;
 	static TextureRegion rightArmFlex4;

 	static TextureRegion leftArmConfused2;
 	static TextureRegion leftArmConfused3;
 	static TextureRegion leftArmConfused4;

 	static TextureRegion leftArmUp2;
 	static TextureRegion leftArmUp3;
 	static TextureRegion leftArmUp4;
 	static TextureRegion leftArmUp5;
 	public static TextureRegion leftArmUp6;
 	
 	public static TextureRegion leftArmFlex2;
 	static TextureRegion leftArmFlex3;
 	static TextureRegion leftArmFlex4;

 	static TextureRegion leftArmWave1;
	
	public static OneTimeAnimation startBreatheFire;
	public static OneTimeAnimation rightArmUp;
	public static OneTimeAnimation leftArmUp;
	public static OneTimeAnimation rightArmConfused;
	public static OneTimeAnimation leftArmConfused;
	public static OneTimeAnimation rightArmFlex;
	public static OneTimeAnimation leftArmFlex;
	public static OneTimeAnimation leftArmUpScared;
	public static OneTimeAnimation rightArmUpScared;
	public static OneTimeAnimation ouch;
	
	
	public static Sound ouchSound;
	
	public static void load (GLGame glGame){
		if (glGame.getGLGraphics().getWidth()<600){
			atlas=new Texture(glGame, "twinko_small.png");
			init(128);
		}
		else{
			atlas=new Texture(glGame, "twinko_medium.png");
			init(256);
		}
		
		startBreatheFire = new OneTimeAnimation(0.05f, mouthClosed,
				mouthNormal2, mouthNormal3, mouthOpened, // mouthOpened, mouthOpened
				mouthFire3, mouthFire2, mouthFire);
		ouch = new OneTimeAnimation(0.05f, mouthClosed,
				mouthNormal2, mouthNormal3, mouthOpened, // mouthOpened, mouthOpened
				mouthFire3, mouthFire2, mouthFire, mouthFire, mouthFire, mouthFire, mouthFire);
		rightArmUp = new OneTimeAnimation(0.05f, rightArmBase, rightArmUp2,
				rightArmUp3, rightArmUp4, rightArmUp5, rightArmUp6,
				rightArmUp6, rightArmUp6, rightArmUp6, rightArmUp6,
				rightArmUp6, rightArmUp5, rightArmUp4, rightArmUp3,
				rightArmUp2, rightArmBase);
		leftArmUp = new OneTimeAnimation(0.05f, leftArmBase, leftArmUp2,
				leftArmUp3, leftArmUp4, leftArmUp5, leftArmUp6, leftArmUp6,
				leftArmUp6, leftArmUp6, leftArmUp6, leftArmUp6, leftArmUp5,
				leftArmUp4, leftArmUp3, leftArmUp2, leftArmBase);

		rightArmConfused = new OneTimeAnimation(0.05f, rightArmConfused2,
				rightArmConfused3, rightArmConfused4, rightArmConfused5,
				rightArmConfused5, rightArmConfused5, rightArmConfused5,
				rightArmConfused5, rightArmConfused5, rightArmConfused5,
				rightArmConfused5, rightArmConfused5, rightArmConfused5,
				rightArmConfused5, rightArmConfused4, rightArmConfused3,
				rightArmConfused2);
		leftArmConfused = new OneTimeAnimation(0.05f, leftArmConfused2,
				leftArmConfused2, leftArmConfused3, leftArmConfused4,
				leftArmConfused4, leftArmConfused4, leftArmConfused4,
				leftArmConfused4, leftArmConfused4, leftArmConfused4,
				leftArmConfused4, leftArmConfused4, leftArmConfused4,
				leftArmConfused4, leftArmConfused3, leftArmConfused2,
				leftArmConfused2);

		rightArmFlex = new OneTimeAnimation(0.05f, rightArmFlex2,
				rightArmFlex3, rightArmFlex4, rightArmFlex4, rightArmFlex4,
				rightArmFlex4, rightArmFlex4, rightArmFlex4, rightArmFlex4,
				rightArmFlex4, rightArmFlex4, rightArmFlex4, rightArmFlex4,
				rightArmFlex4, rightArmFlex4, rightArmFlex3, rightArmFlex2);
		leftArmFlex = new OneTimeAnimation(0.05f, leftArmFlex2, leftArmFlex3,
				leftArmFlex4, leftArmFlex4, leftArmFlex4, leftArmFlex4,
				leftArmFlex4, leftArmFlex4, leftArmFlex4, leftArmFlex4,
				leftArmFlex4, leftArmFlex4, leftArmFlex4, leftArmFlex4,
				leftArmFlex4, leftArmFlex3, leftArmFlex2);
		
		leftArmUpScared = new OneTimeAnimation(0.05f, leftArmBase, leftArmUp2,
				leftArmUp3, leftArmUp4, leftArmUp5, leftArmUp6);
		rightArmUpScared = new OneTimeAnimation(0.05f, rightArmBase,
				rightArmUp2, rightArmUp3, rightArmUp4, rightArmUp5,
				rightArmUp6);
		
		ouchSound=glGame.getAudio().newSound("ouch.mp3");
	}
	
	private static void init(int unit){
		head=new TextureRegion(atlas, 0, 0, unit, unit);
		body=new TextureRegion(atlas, unit, 0, unit, unit);
		tail=new TextureRegion(atlas, unit, 3*unit, unit, unit);
		wing=new TextureRegion(atlas, 0, 3*unit, unit, unit);
		
		unit/=2;
		
		eyesNormalOpened=new TextureRegion(atlas, unit, 2*unit, unit, unit);
		eyesFireBreathing=new TextureRegion(atlas, 3*unit, 2*unit, unit, unit);
		eyesSad=new TextureRegion(atlas, unit, 3*unit, unit, unit);
		eyesNormalClosed=new TextureRegion(atlas, 0, 2*unit, unit, unit);
		eyesConfused=new TextureRegion(atlas, 0, 3*unit, unit, unit);
		eyesAngry=new TextureRegion(atlas, 2*unit, 2*unit, unit, unit);
		eyesLeftOpened=new TextureRegion(atlas, 7*unit, unit, unit, unit);
		eyesRightOpened=new TextureRegion(atlas, 7*unit, 2*unit, unit, unit);
		eyesClosed=new TextureRegion(atlas, 0, 2*unit, unit, unit);
		
		mouthClosed=new TextureRegion(atlas, 2*unit, 3*unit, unit, unit);
		mouthNormal2=new TextureRegion(atlas, 3*unit, 3*unit, unit, unit);
		mouthNormal3=new TextureRegion(atlas, 0, 4*unit, unit, unit);
		mouthOpened=new TextureRegion(atlas, unit, 4*unit, unit, unit);
		mouthFire3=new TextureRegion(atlas, 2*unit, 4*unit, unit, unit);
		mouthFire2=new TextureRegion(atlas, 3*unit, 4*unit, unit, unit);
		mouthFire=new TextureRegion(atlas, 4*unit, 4*unit, unit, unit);
		mouthSad=new TextureRegion(atlas, 4*unit, 3*unit, unit, unit);
		mouthConfused=new TextureRegion(atlas, 5*unit, 3*unit, unit, unit);
		mouthScared=new TextureRegion(atlas, 6*unit, 3*unit, unit, unit);
		
		rightFoot=new TextureRegion(atlas, 0, 5*unit, unit, unit);
		leftFoot=new TextureRegion(atlas, unit, 5*unit, unit, unit);
		
		leftArmBase=new TextureRegion(atlas, 2*unit, 5*unit, unit, unit);
		rightArmBase=new TextureRegion(atlas, 3*unit, 5*unit, unit, unit);
		rightArmBase2=new TextureRegion(atlas, 5*unit, 6*unit, unit, unit);
		
		rightArmUp2=new TextureRegion(atlas, 4*unit, 5*unit, unit, unit);
		rightArmUp3=new TextureRegion(atlas, 5*unit, 5*unit, unit, unit);
		rightArmUp4=new TextureRegion(atlas, 6*unit, 5*unit, unit, unit);
		rightArmUp5=new TextureRegion(atlas, 7*unit, 5*unit, unit, unit);
		rightArmUp6=new TextureRegion(atlas, 4*unit, 6*unit, unit, unit);
		
		rightArmConfused2=new TextureRegion(atlas, 4*unit, 0, unit, unit);
		rightArmConfused3=new TextureRegion(atlas, 5*unit, 0, unit, unit);
		rightArmConfused4=new TextureRegion(atlas, 6*unit, 0, unit, unit);
		rightArmConfused5=new TextureRegion(atlas, 7*unit, 0, unit, unit);
		
		rightArmFlex2=new TextureRegion(atlas, 4*unit, 2*unit, unit, unit);
		rightArmFlex3=new TextureRegion(atlas, 5*unit, 2*unit, unit, unit);
		rightArmFlex4=new TextureRegion(atlas, 6*unit, 2*unit, unit, unit);
		
		leftArmConfused2=new TextureRegion(atlas, 4*unit, unit, unit, unit);
		leftArmConfused3=new TextureRegion(atlas, 5*unit, unit, unit, unit);
		leftArmConfused4=new TextureRegion(atlas, 6*unit, unit, unit, unit);
		
		leftArmUp2=new TextureRegion(atlas, 5*unit, 6*unit, unit, unit);
		leftArmUp3=new TextureRegion(atlas, 6*unit, 6*unit, unit, unit);
		leftArmUp4=new TextureRegion(atlas, 7*unit, 6*unit, unit, unit);
		leftArmUp5=new TextureRegion(atlas, 4*unit, 7*unit, unit, unit);
		leftArmUp6=new TextureRegion(atlas, 5*unit, 7*unit, unit, unit);
		
		leftArmFlex2=new TextureRegion(atlas, 5*unit, 4*unit, unit, unit);
		leftArmFlex3=new TextureRegion(atlas, 6*unit, 4*unit, unit, unit);
		leftArmFlex4=new TextureRegion(atlas, 7*unit, 4*unit, unit, unit);
		
		leftArmWave1=new TextureRegion(atlas, 0, 6*unit, unit, unit);
	}
	
	public static void reload(){
		atlas.reload();
	}
	
	public static void playSound(Sound sound) {
        sound.play(1);
    }

}
