package com.calin.tests.assets;

import com.calin.tests.framework.gl.Texture;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.framework.impl.GLGame;

public class FoodAssets {
	
	public static Texture foodAtlas;
	public static TextureRegion grape;
	public static TextureRegion peach;
	public static TextureRegion plum;
	public static TextureRegion cherry;
	public static TextureRegion lemon;
	public static TextureRegion orange;
	public static TextureRegion lime;
	public static TextureRegion strawberry;
	public static TextureRegion mushroom;
	
	public static void load(GLGame glGame){
		if (glGame.getGLGraphics().getWidth()<800){
			foodAtlas=new Texture(glGame, "fruits_atlas_small.png");
			init(64);
		}
		else{
			foodAtlas=new Texture(glGame, "fruits_atlas_hd.png");
			init(256);
		}
	}
	
	private static void init(int unit){
		grape=new TextureRegion(foodAtlas, 0, 0, unit, unit);
		peach=new TextureRegion(foodAtlas, unit, 0, unit, unit);
		plum=new TextureRegion(foodAtlas, 2*unit, 0, unit, unit);
		cherry=new TextureRegion(foodAtlas, 3*unit, 0, unit, unit);
		lemon=new TextureRegion(foodAtlas, 0, unit, unit, unit);
		orange=new TextureRegion(foodAtlas, unit, unit, unit, unit);
		lime=new TextureRegion(foodAtlas, 2*unit, unit, unit, unit);
		strawberry=new TextureRegion(foodAtlas, 3*unit, unit, unit, unit);
		mushroom=new TextureRegion(foodAtlas, 0, 2*unit, unit, unit);
	}
	
	public static void reload(){
		foodAtlas.reload();
	}

}
