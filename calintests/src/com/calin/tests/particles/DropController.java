package com.calin.tests.particles;

import com.calin.tests.GameObject;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.framework.math.WorkerVector2;

public class DropController extends GameObject{

	public byte state;
	public static final byte ACTIVE=1;
	public static final byte INACTIVE=0;
	
	public final int dropsNo=150;
	public final float MAX_AGE=1f;
	public float age;
	public int nextRealeaseIndex;
	public Drop[] drops;
	private Vector2 position;
	private WorkerVector2 destination;
	
	public DropController(float x, float y, float width, float height, Vector2 destination) {
		super(x, y, width, height);
		this.position=new Vector2(destination.x, destination.y+5.5f);
		this.destination=new WorkerVector2(destination);
		state=INACTIVE;
		age=0;
		nextRealeaseIndex=0;
		drops=new Drop[dropsNo];
		for (int i = 0; i < dropsNo; i++) {
			drops[i]=new Drop(5, 5, 0.5f, 0.5f);
		}
	}
	
	public void update(float deltaTime){
		if (state==ACTIVE){
			//release random no
			int session=(int) (1+Math.random()*2);
			int counter=0;
			
			while (counter<session){
				if (drops[nextRealeaseIndex].state==Drop.DEAD){
					drops[nextRealeaseIndex].reset(position, destination);
					drops[nextRealeaseIndex].revive();
				}
				nextRealeaseIndex++;
				if (nextRealeaseIndex>=dropsNo) nextRealeaseIndex=0;
				counter++;
			}
			//age
			age+=deltaTime;
			if (age>=MAX_AGE) state=INACTIVE;
		}
		//update drops
		for(Drop drop:drops){
			drop.update(deltaTime);
		}
	}
	
	
	public void resetOnTouch(Vector2 position){
//		this.position=position;
//		destination.set(position.x, position.y-2.1f);
		this.state=ACTIVE;
		this.age=0;
		this.resetAll();
		
	}
	
	private void resetAll(){
		for(Drop d: drops){
			d.reset(position, destination);
		}
	}
}
