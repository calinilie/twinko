package com.calin.tests.particles;

import android.util.Log;

import com.calin.tests.GameObject;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.Vector2;

public class FireflyEmitter {
	
	public Firefly[] fireflies;
	
	public static final float SPEED=9f;
	public float size=0.5f;
	
	private final byte noOfFireflies=18;
	
	public static final byte ALIVE=1;
	public static final byte DEAD=2;
	
	public byte state;
	
	private Circle dummyTarget;
	//when calculating the angle between 2 vectors one of them gets fucked up, so we need a temp/dummy vector which we will use for this
	private Vector2 dummyTargetCenter;
	
	public FireflyEmitter(){
		dummyTarget=new Circle(-1, -1, 0.1f);
		dummyTargetCenter=new Vector2();
		this.fireflies=new Firefly[this.noOfFireflies];
		for (int i = 0; i < fireflies.length; i++) {
			fireflies[i]=new Firefly(-10, -10, size, size);
		}
		this.state=DEAD;
	}
	
	public void init(Vector2 position){
		this.size=1;
		int angle=360/noOfFireflies;
		for (int i = 0; i < fireflies.length; i++) {
			fireflies[i].init(position, angle);
			angle+=360/noOfFireflies;
		}
		this.state=ALIVE;
	}
	
	public void update(float deltaTime, GameObject target, float initialSpeed){
		switch (this.state){
		case ALIVE:
			dummyTarget.center.set(target.position.x, target.position.y);
			dummyTargetCenter.set(dummyTarget.center);
			for (int i = 0; i < fireflies.length; i++) {
				fireflies[i].update(deltaTime, dummyTarget, dummyTargetCenter, initialSpeed);
				dummyTargetCenter.set(dummyTarget.center);
			}
			size-=0.075;
			if (this.size<=0) this.state=DEAD;
			break;
		case DEAD:
			break;
		}
	}
}
