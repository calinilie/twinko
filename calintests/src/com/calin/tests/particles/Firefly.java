package com.calin.tests.particles;

import com.calin.tests.DynamicGameObject;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.OverlapTester;
import com.calin.tests.framework.math.Vector2;

public class Firefly extends DynamicGameObject{

	public byte state;
	
	public static final byte ALIVE=1;
	public static final byte DEAD=2;
	
	private final float R=3;
	
	public Firefly(float x, float y, float width, float height) {
		super(x, y, width, height);
		this.state=Firefly.ALIVE;
	}
	
	public void init(Vector2 position, int angle){
		this.state=Firefly.ALIVE;
		float x=(float)(position.x+R*Math.cos(angle*Vector2.TO_RADIANS));
		float y=(float)(position.y+R*Math.sin(angle*Vector2.TO_RADIANS));
		this.position.set(x,y);
		
		float velocityX=-(float)(FireflyEmitter.SPEED*Math.cos(angle*Vector2.TO_RADIANS));
		float velocityY=-(float)(FireflyEmitter.SPEED*Math.sin(angle*Vector2.TO_RADIANS));
		this.velocity.set(velocityX, velocityY);
	}
	
	public void update(float deltaTime, Circle target, Vector2 targetCenter, float initialSpeed){
		switch (this.state) {
		case ALIVE:
			this.updateVelocity(deltaTime, targetCenter, initialSpeed);
			this.position.add(this.velocity.x*deltaTime, this.velocity.y*deltaTime);
			if (OverlapTester.pointInCircle(target, this.position)) this.state=DEAD;
			break;
		case DEAD:
			break;
		}
	}
	
	private void updateVelocity(float deltaTime, Vector2 target, float initialSpeed){
		float angle=target.sub(this.position).angle()*Vector2.TO_RADIANS;
		velocity.x=(float) (Math.cos(angle)*(FireflyEmitter.SPEED+initialSpeed));
		velocity.y=(float) (Math.sin(angle)*(FireflyEmitter.SPEED+initialSpeed));
	}
}
