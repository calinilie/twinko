package com.calin.tests.particles;


import android.util.Log;

import com.calin.tests.DynamicGameObject;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.framework.math.WorkerVector2;

public class Drop extends DynamicGameObject{

	public byte state;
	public static final byte DEAD=0;
	public static final byte ALIVE=1;
	public final float MAX_AGE=0.45f;
	public float age;

	public float angle;
	
	public final float SPEED=10;
	
//	private WorkerVector2 target=new WorkerVector2();
	
	
	public Drop(float x, float y, float width, float height) {
		super(x, y, width, height);
		state=DEAD;
		age=0;
	}
	
	public void update(float deltaTime){
		switch (state){
		case ALIVE:
			move(deltaTime);
			if (age>=MAX_AGE) state=DEAD;
			age+=deltaTime;
			break;
		case DEAD:
			break;
		}
	}
	
	private void move(float deltaTime){
		this.position.x+=velocity.x*deltaTime;
		this.position.y+=velocity.y*deltaTime;
	}
	
	public void reset(Vector2 startPostion, WorkerVector2 destination){
		float marge=0.25f;
		float x=(float) (startPostion.x-marge+Math.random()*2*marge);
		float y=(float) (startPostion.y-marge+Math.random()*2*marge);
		this.position.set(x, y);
		angle=destination.sub(this.position).angle();
		if (destination.temp.x==destination.x && destination.temp.y==destination.y) {
			Log.d("tests", destination.x+" "+destination.y+" temp:"+destination.temp.x+" "+destination.temp.y);
			throw new RuntimeException();
		}
		this.velocity.x=(float) (Math.cos(angle*Vector2.TO_RADIANS)*SPEED);
		this.velocity.y=(float) (Math.sin(angle*Vector2.TO_RADIANS)*SPEED);
		angle+=90;
		age=0;
	}
	
	public void revive(){
		state=ALIVE;
	}
	
	public String toString(){
		return "x:"+position.x+" y:"+position.y;
	}

}
