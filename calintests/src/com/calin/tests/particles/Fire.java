package com.calin.tests.particles;

import android.util.Log;

import com.calin.tests.DynamicGameObject;
import com.calin.tests.framework.math.Vector2;
/**
 * 
 * @author calin
 * should be moved to Dragon class; Twinko must control the way he breathes fire.
 */
public class Fire {
	
	public final static int particleNumber=150;
	public int session=0;//how many particles are released per frame
	public static Particle[] particles;
	private int counter=0;//how many particles have been released so far
	
	private Vector2 targetPos;//dummy position used for angle calculation and other shit
	private Vector2 targetPosPrediction;
	private Vector2 targetSpeed;
	
	public static boolean dead=true;
		
	public static float angle;
	
	public Fire(){
		counter=0;
		if (particles == null){
			Log.d("fire", "new fire particles");
			particles = new Particle[particleNumber];
			for (int i=0; i<particleNumber; i++)
				particles[i]=new Particle(-2, -2, 0.2f, 0.2f);
		}
		else {
			Log.d("fire", "reseting fire partices");
			for (int i = 0; i < particleNumber; i++) {
				particles[i].position.set(-2, -2);
				particles[i].state = Particle.STATE_DEAD;
			}
		}
		
		targetPos=new Vector2();
		targetPosPrediction=new Vector2();
		targetSpeed=new Vector2();
	}
	
	private float getNewAngle(DynamicGameObject currentTarget){
		//get distances to target
		float distanceToTargetOnX=Math.abs(Particle.POS_X-currentTarget.position.x);
		float distanceToTargetOnY=Math.abs(Particle.POS_Y-currentTarget.position.y);
		//get particle speed based on current angle
		targetPos.set(currentTarget.position);
		targetPosPrediction.set(currentTarget.position);
		float angle = (targetPos.sub(Particle.POS_X, Particle.POS_Y).angle())*Vector2.TO_RADIANS;
		float particleVelovityOnX=Math.abs((float)(Math.cos(angle)*Particle.MAX_SPEED));
		float particleVelovityOnY=Math.abs((float)(Math.sin(angle)*Particle.MAX_SPEED));
		//get travel time of particle from source to Target
		float deltaTimeForX=distanceToTargetOnX/particleVelovityOnX;
		float deltaTimeForY=distanceToTargetOnY/particleVelovityOnY;
		//get average travel time -> check for 0 values;
		float deltaTime=(deltaTimeForX+deltaTimeForY)/2;
		//predict future target position
		targetSpeed.set(currentTarget.velocity);
		targetSpeed.add(currentTarget.accel.x*deltaTime, currentTarget.accel.y*deltaTime);
		targetPosPrediction.x+=targetSpeed.x*deltaTime;
		targetPosPrediction.y+=targetSpeed.y*deltaTime;
		//get angle for future position
		float result=targetPosPrediction.sub(Particle.POS_X, Particle.POS_Y).angle();
		return result;
	}
	
	
	public void update(float deltaTime, DynamicGameObject sourcePos, boolean isTouched, boolean hasStamina, boolean fireOK){
		if (isTouched && sourcePos!=null){
			float angle=this.getNewAngle(sourcePos);
			Fire.angle=angle;
//			release next particles in line
			if (hasStamina && fireOK){
				for (int j = counter; j < counter+session; j++) {
					particles[j].resetState(angle);
				}
			}
			if (counter>particleNumber-session-5) counter=0;
			else counter+=session;
		}
		else dead=true;
		
		for (int i = 0; i < particleNumber; i++) {
			particles[i].update(deltaTime);
			dead &= particles[i].dead;
		}
	}
	
	public void update(float deltaTime, DynamicGameObject sourcePos, boolean isTouched, boolean hasStamina){
		if (isTouched && sourcePos!=null){
			float angle=this.getNewAngle(sourcePos);
			Fire.angle=angle;
//			release next particles in line
			if (hasStamina){
				for (int j = counter; j < counter+session; j++) {
					particles[j].resetState(angle);
				}
			}
			if (counter>particleNumber-session-5) counter=0;
			else counter+=session;
		}
		else dead=true;
		
		for (int i = 0; i < particleNumber; i++) {
			particles[i].update(deltaTime);
			dead &= particles[i].dead;
		}
	}
	
	public void setIntensity(int intensity){
		if (intensity>0 && intensity<3) {
			session=1;
		}
		else {
			if (intensity>=3 && intensity<5){
				session=2;
			}
			else {
				if (intensity>=5 && intensity <7) {
					session=3;
				}
			}
		}
	}
	
}
