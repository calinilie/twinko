package com.calin.tests.particles;

import com.calin.tests.DynamicGameObject;
import com.calin.tests.framework.math.Vector2;

public class Particle extends DynamicGameObject {
	
	public static final int STATE_ALIVE = 0;	// particle is alive
	public static final int STATE_DEAD = 1;		// particle is dead
	public static final int STATE_HIT=2;

	public static final int DEFAULT_LIFETIME 	= 6;	// play with this
	public static final int MAX_DIMENSION		= 5;	// the maximum width or height
	public static final int MAX_SPEED			= 13;	// maximum speed (per update)
//	public static final float MAX_SIZE			=0.2f;
	
	public static float POS_X=-10f;
	public static float POS_Y=-10f;
	
	private final float incRate=1.5f;
	private final float decAlphaFast=5.8f;
	private final float decAlphaSlow=0.15f;
	

	public int state;			// particle is alive or dead
	public int age;			    // current age of the particle
	private int lifetime;		// particle dies when it reaches this value
	public float alpha;			// the color of the particle
	
	public boolean dead=true;
	
	public float textureSize;
	
	public Particle(float x, float y, float width, float height) {
		super(x, y, width, height);
		this.state = STATE_DEAD;
		lifetime=DEFAULT_LIFETIME;
		this.age = 0;
		this.alpha = 0.6f;;
		textureSize=0.8f;
	}
	
	public void update(float deltaTime){
		switch (state){
		case STATE_ALIVE:
			updateAlive(deltaTime);
			break;
		case STATE_HIT:
			updateHit(deltaTime);
			break;
		}
	}
	
	private void updateAlive(float deltaTime){
		alpha-=decAlphaSlow*deltaTime;
		if (age>=lifetime) {
			if (alpha<=0) kill();
			else alpha-=decAlphaFast*deltaTime;				
		}
		else {
			age+=deltaTime;
			incSize(deltaTime);
		}
		move(deltaTime);
	}
	
	private void updateHit(float deltaTime){
		move(deltaTime);
		if (alpha<=0) kill();
		this.alpha-=decAlphaFast*deltaTime;
	}
	
	public void resetState(float angle){
		this.position.x=Particle.POS_X;
		this.position.y=Particle.POS_Y;
		float min=angle-10;
		float max=angle+10;
		this.velocity.x=(float)(Math.cos((min + Math.random()*(max-min + 1))*Vector2.TO_RADIANS)*MAX_SPEED);
		this.velocity.y=(float)(Math.sin((min + Math.random()*(max-min + 1))*Vector2.TO_RADIANS)*MAX_SPEED);
		age = 0;
		alpha = 0.6f;
		state = Particle.STATE_ALIVE;
		dead=false;
		setSize(0.8f);
	}
	
	private void move(float deltaTime){
		this.position.add(this.velocity.x*deltaTime, this.velocity.y*deltaTime);
		this.bounds.lowerLeft.set(position.x-textureSize/2, position.y-textureSize/2);
	}
	
	public void hit(){
		this.state=Particle.STATE_HIT;
//		if (alpha>0.3f) 
		this.alpha=0.3f;
	}
	
	private void setSize(float size){
		textureSize=size;
		this.bounds.width=textureSize;
		this.bounds.height=textureSize;
		this.bounds.lowerLeft.set(position.x-textureSize/2, position.y-textureSize/2);
	}
	
	private void incSize(float deltaTime){
		textureSize+=incRate*deltaTime;
		this.bounds.width=textureSize;
		this.bounds.height=textureSize;
		this.bounds.lowerLeft.set(position.x-textureSize/2, position.y-textureSize/2);
	}
	
	private void kill(){
		this.state=STATE_DEAD;
		dead=true;
		this.position.x=Particle.POS_X;
		this.position.y=Particle.POS_Y;
		this.bounds.lowerLeft.set(position.x-textureSize/2, position.y-textureSize/2);
//		Log.d("fire","IM DEAD!!!!");
	}
}

