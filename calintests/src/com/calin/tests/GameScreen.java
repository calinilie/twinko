package com.calin.tests;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import android.util.Log;

import com.calin.test.levels.*;
import com.calin.tests.assets.*;
import com.calin.tests.datasource.Facade;
import com.calin.tests.framework.*;
import com.calin.tests.framework.Input.*;
import com.calin.tests.framework.gl.*;
import com.calin.tests.framework.impl.*;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.Dragon;
import com.calin.tests.particles.Fire;
import com.calin.tests.renderers.*;

public class GameScreen extends GLScreen implements IPauseListener{
	GLGraphics graphics;
	Input input;
	Camera2D camera;
	
	Level level;
	int fireIntensity=0;
	
	Vector2 touchPos;
	Circle fingerTouch;
	
	byte previousLevelState;
	
	SpriteBatcher batcher, fireBatcher;
	Dragon dragon;
	FPSCounter fps;
	long i;
	int levelId;
	
	LevelRenderer levelRenderer;
	
	public GameScreen(Game game, int levelId) {  
		super(game);
		graphics=((GLGame)game).getGLGraphics();
		input=game.getInput();
		dragon=GameObjectsController.getInstance(game).getDragon();
		camera=new Camera2D(graphics, Assets1.FRUSTRUM_WIDTH, Assets1.FRUNSTRUM_HEIGHT);
		touchPos=new Vector2();
		batcher=new SpriteBatcher(graphics, 150);
		fireBatcher=new SpriteBatcher(graphics, Fire.particleNumber+50, true);
		fps=new FPSCounter();
		fingerTouch=new Circle(-10, -10, 0.65f);
		this.levelId = levelId;
		initLevel(levelId);
	}
	
	private void initLevel(int levelId){
		switch (levelId){
		case -1:
			level=new Level1(dragon, getCurrentHighScore(1), getLevelPreviouslyPassed(1));
			levelRenderer=new Level1Renderer(level, graphics, batcher, fireBatcher);
			break;
		case 1:
			level=new Level01(dragon, getCurrentHighScore(1), getLevelPreviouslyPassed(1));
			levelRenderer=new Level01Renderer(level, graphics, batcher, fireBatcher);
			level.dragon.head.mouth.registerMouthRendererListeners(levelRenderer.dragonRenderer);
			level.dragon.setRendererListener(levelRenderer.dragonRenderer);
			break;
		case 2:
			level=new Level02(dragon, getCurrentHighScore(2), getLevelPreviouslyPassed(levelId));
			levelRenderer=new Level02Renderer(level, graphics, batcher, fireBatcher);
			((Level02)level).registerDrgaonHitListeners(level, dragon, levelRenderer.dragonRenderer);
			level.dragon.head.mouth.registerMouthRendererListeners(levelRenderer.dragonRenderer);
			level.dragon.setRendererListener(levelRenderer.dragonRenderer);
			break;
		case 3:
			level=new Level03(dragon, getCurrentHighScore(3), getLevelPreviouslyPassed(levelId));
			levelRenderer=new Level03Renderer(level, graphics, batcher, fireBatcher);
			level.dragon.head.mouth.registerMouthRendererListeners(levelRenderer.dragonRenderer);
			level.dragon.setRendererListener(levelRenderer.dragonRenderer);
			break;
		case 4:
			level=new Level04(dragon, getCurrentHighScore(4), getLevelPreviouslyPassed(levelId));
			levelRenderer=new Level04Renderer(level, graphics, batcher, fireBatcher);
			((Level04)level).registerDrgaonHitListeners(level, dragon, levelRenderer.dragonRenderer);
			level.dragon.head.mouth.registerMouthRendererListeners(levelRenderer.dragonRenderer);
			level.dragon.setRendererListener(levelRenderer.dragonRenderer);
			break;
		case 5:
			level=new Level05(dragon, getCurrentHighScore(5), getLevelPreviouslyPassed(levelId));
			levelRenderer=new Level05Renderer(level, graphics, batcher, fireBatcher);
			((Level05)level).registerDrgaonHitListeners(level, dragon, levelRenderer.dragonRenderer);
			level.dragon.head.mouth.registerMouthRendererListeners(levelRenderer.dragonRenderer);
			level.dragon.setRendererListener(levelRenderer.dragonRenderer);
			break;
		case 6:
			level=new Level06(dragon, getCurrentHighScore(6), getLevelPreviouslyPassed(levelId));
			levelRenderer=new Level06Renderer(level, graphics, batcher, fireBatcher);
			((Level06)level).registerDrgaonHitListeners(level, dragon, levelRenderer.dragonRenderer);
			level.dragon.head.mouth.registerMouthRendererListeners(levelRenderer.dragonRenderer);
			level.dragon.setRendererListener(levelRenderer.dragonRenderer);
			break;
		case 7:
			level=new Level07(dragon, getCurrentHighScore(7), getLevelPreviouslyPassed(levelId));
			levelRenderer=new Level07Renderer(level, graphics, batcher, fireBatcher);
			((Level07)level).registerDrgaonHitListeners(level, dragon, levelRenderer.dragonRenderer);
			level.dragon.head.mouth.registerMouthRendererListeners(levelRenderer.dragonRenderer);
			level.dragon.setRendererListener(levelRenderer.dragonRenderer);
			break;
		case 8:
//			level = new LevelArcade(dragon, getCurrentHighScore(), getLevelPreviouslyPassed(levelId));
//			levelRenderer=new LevelArcadeRenderer(level, graphics, batcher, fireBatcher);
			level = new Level08(dragon, getCurrentHighScore(8), getLevelPreviouslyPassed(levelId));
			levelRenderer=new Level08Renderer(level, graphics, batcher, fireBatcher);
			((Level08)level).registerDrgaonHitListeners(level, dragon, levelRenderer.dragonRenderer);
			level.dragon.head.mouth.registerMouthRendererListeners(levelRenderer.dragonRenderer);
			level.dragon.setRendererListener(levelRenderer.dragonRenderer);
			break;
		case 9:
			level=new Level09(dragon, getCurrentHighScore(9), getLevelPreviouslyPassed(levelId));
			levelRenderer=new Level09Renderer(level, graphics, batcher, fireBatcher);
			((Level09)level).registerDrgaonHitListeners(level, dragon, levelRenderer.dragonRenderer);
			level.dragon.head.mouth.registerMouthRendererListeners(levelRenderer.dragonRenderer);
			level.dragon.setRendererListener(levelRenderer.dragonRenderer);
			break;
		}
	}
	

	@Override
	public void update(float deltaTime) {
		List<TouchEvent> touchEvents=input.getTouchEvents();
		List<KeyEvent> keyEvents = input.getKeyEvents();
		int len=keyEvents.size();
		for (int i = 0; i < len; i++) {
			KeyEvent event=keyEvents.get(i);
			if (event.keyCode==android.view.KeyEvent.KEYCODE_BACK && event.type==KeyEvent.KEY_UP){
//				if (level.state==Level.IN_STORE) level.state=previousLevelState;
//				else {
//					previousLevelState=level.state;
//					level.state=Level.IN_STORE;
//				}
				game.setScreen(new MainScreen(game));
			}
		}
		len=touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event= touchEvents.get(i);
			if (event.type==TouchEvent.TOUCH_UP){
				touchPos.set(event.x, event.y);
				camera.touchToWorld(touchPos);
				if (level.onTouchUp(touchPos)) game.setScreen(new MainScreen(game));
			}
		}
		
		boolean isTouched;
		if (isTouched=input.isTouchDown(0)){
			touchPos.set(input.getTouchX(0), input.getTouchY(0));
			camera.touchToWorld(touchPos);
		}
		level.update(deltaTime, touchPos, isTouched);
	}

	@Override
	public void present(float deltaTime) {
		GL10 gl = graphics.getGL();
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT); 
		if (level instanceof Level01){
			Level01 level01=(Level01) level;
			camera.zoom=level01.zoom;
			camera.position.set(level01.cameraModel.position);
		}
		camera.setViewportAndMatrices();
		
		
		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glEnable(GL10.GL_BLEND);
		
		switch(level.state){
		default:
			levelRenderer.render(deltaTime);
			
			batcher.beginBatch(Assets1.digitsAtlas);
			batcher.drawSprite(level.controller.bar.position.x, level.controller.bar.position.y, level.controller.bar.bounds.width, level.controller.bar.bounds.height, Assets1.slideBarRegion, SpriteBatcher.NORMAL);
			batcher.drawSprite(level.controller.button.position.x, level.controller.button.position.y, level.controller.button.bounds.width, level.controller.button.bounds.height, Assets1.slideButtonRegion, SpriteBatcher.NORMAL);
			batcher.endBatch();
			break;
//		case Level.IN_GAME_PAUSED:
//			batcher.beginBatch(Assets1.digitsAtlas);
//			batcher.drawSprite(5, 7, 8, 2, Assets1.inStorePausedRegion, SpriteBatcher.NORMAL);
//			batcher.drawSprite(8, 2, 2, 2, Assets1.exitButtonRegion, SpriteBatcher.NORMAL);
//			batcher.endBatch();
//			StoreRenderer.renderStore(level.storeDisplayModel, batcher);
//			break;
		}
		
		
		gl.glDisable(GL10.GL_BLEND);
		fps.logFrame();
	}

	@Override
	public void pause() {			
	}

	@Override
	public void resume() {
	}
	
	private int getCurrentHighScore(int levelId){
		Facade facade=((GLGame)game).getDataSourceFacade();
		facade.open();
		int result=facade.getCurrentHighScore(levelId);
		facade.close();
		return result;
	}
	
	private boolean getLevelPreviouslyPassed(int levelId){
		Facade facade=((GLGame)game).getDataSourceFacade();
		facade.open();
		boolean result=facade.getLevelPreviouslyPassed(levelId);
		facade.close();
		return result;
	}

	@Override
	public void dispose() {
		Facade facade=((GLGame)game).getDataSourceFacade();
		facade.open();
		if (level.isLevelOver()){
			if (level.passed) {
				Log.d("levels", "i was passed");
				switch(levelId){
				case 6:
					facade.setCategoryUnlocked(LevelIconsDisplayModel.JUNGLE_LEVELS);
					LevelCategoriesDiplayModel.unlockJungleCategory();
					break;
				case 16:
					facade.setCategoryUnlocked(LevelIconsDisplayModel.CAVE_LEVELS);
					LevelCategoriesDiplayModel.unlockCaveCategory();
					break;
				case 26:
					facade.setCategoryUnlocked(LevelIconsDisplayModel.CAVE_LEVELS);
					break;
				}
				facade.setLevelPassed(this.levelId);
				facade.updateMaxScore(level.score, this.levelId);
			}
			facade.updateDragonStatsOnLevelComplete(dragon.dragonStats);
		}
		facade.updateDragonBag(dragon.dragonBag);
		facade.close();
		Log.d("stats", "## STATS Level over : " + dragon.dragonStats.toString());
		Log.d("stats", "## BAG   Level over : " + dragon.dragonBag.toString());
	}

	@Override
	public void onContinuePressed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReplayPressed(int levelId) {
		initLevel(levelId);
		
	}

	@Override
	public void onShopPressed() {
		// TODO Auto-generated method stub
		
	}
		
}
