package com.calin.tests;

import android.util.FloatMath;

import com.calin.tests.framework.Input.TouchEvent;
import com.calin.tests.framework.math.Vector2;

public abstract class Drag {
	
	private static float startX;
	private static float startY;
	private static float intensity=-1;
	
	public static float calculateIntensity(TouchEvent event, Vector2 position){
		switch (event.type){
		case (TouchEvent.TOUCH_DOWN):
			startX=position.x;
			startY=position.y;
			break;
		case (TouchEvent.TOUCH_DRAGGED):
			float distX=position.x-startX;
			float distY=position.y-startY;
			//TODO implement drag down check!
			intensity=FloatMath.sqrt(distX*distX+distY*distY);
			break;
		default:
			intensity=-1;
		}
		return intensity;
	}
	
	
	public static byte listenFling(TouchEvent event, Vector2 position){
		switch (event.type) {
		case TouchEvent.TOUCH_DOWN:
			startX=position.x;
			startY=position.y;
			break;
		case TouchEvent.TOUCH_UP:
			if (Math.abs(startY-position.y)>0.5f){
				if (startY<position.y) return 1;
				return -1;
			}
		}
		return 0;
	}

}
