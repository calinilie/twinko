package com.calin.tests;

public abstract class TimeStamp {
	
	public boolean executed=false;
	public int after;
	
	public TimeStamp(int after){
		this.after=after;
	}
	
	public abstract void execute();

}
