package com.calin.tests;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import android.util.Log;

import com.calin.test.levels.LevelCategoriesDiplayModel;
import com.calin.test.levels.LevelIconsDisplayModel;
import com.calin.test.levels.StatsDisplayModel;
import com.calin.test.levels.StoreDisplayModel;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.ButtonsAsstets;
import com.calin.tests.assets.StoreAssets;
import com.calin.tests.datasource.Facade;
import com.calin.tests.framework.Game;
import com.calin.tests.framework.Input;
import com.calin.tests.framework.Input.KeyEvent;
import com.calin.tests.framework.Input.TouchEvent;
import com.calin.tests.framework.gl.Camera2D;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.gl.Texture;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.framework.impl.GLGame;
import com.calin.tests.framework.impl.GLGraphics;
import com.calin.tests.framework.impl.GLScreen;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.OverlapTester;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.Digit;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.DragonBag;
import com.calin.tests.objects.Enemy;
import com.calin.tests.objects.NumbersDisplay;
import com.calin.tests.particles.Fire;
import com.calin.tests.renderers.LevelsCategoriesRenderer;
import com.calin.tests.renderers.LevelsRenderer;
import com.calin.tests.renderers.RendererHelper;
import com.calin.tests.renderers.StatsRenderer;
import com.calin.tests.renderers.StoreRenderer;
import com.calin.tests.renderers.TwinkoRenderer;

public class MainScreen extends GLScreen{

	final float FRUSTRUM_WIDTH = 16f;
	final float FRUNSTRUM_HEIGHT=9f;
	
	//game specifics
	GLGraphics graphics;
	Input input;
	Camera2D camera;
	SpriteBatcher batcher;
	Facade facade;
	
	//states
	public static final byte NORMAL=1;
	public static final byte STORE=2;
	public static final byte LEVEL_CATEGORIES=3;
	public static final byte PLAYER_STATS = 5;
	public static final byte LEVELS = 6;
	static byte state=NORMAL;
	
	//textures
	Texture playAtlas;
	TextureRegion playAsset;
	
	//displayModels
	GameObject skillNotification;
	GameObject shopButton;
	NumbersDisplay skillNotificationNo;
	StoreDisplayModel storeDisplayModel;
	StatsDisplayModel statsDisplayModel;
	LevelCategoriesDiplayModel levelCategories;
	LevelIconsDisplayModel levels;
	Play play;
	Dragon twinko;
	
	//input
	Vector2 touchPos;
	Circle fingerTouch;
	
	//renderers
	TwinkoRenderer twinkoRenderer;
	
	float elapsedTime;
	float timeSinceLastRandomAnimation;
	float timeTillNextRandomAnimation;
	
	DynamicGameObject fireBreathTarget;
	
	public MainScreen(Game game) {
		super(game);
		graphics=((GLGame)game).getGLGraphics();
		input=game.getInput();
		camera=new Camera2D(graphics, FRUSTRUM_WIDTH, FRUNSTRUM_HEIGHT);
		this.facade=((GLGame)game).getDataSourceFacade();
		this.play=new Play(12f, 2 , 6, 1.5f);
		batcher=new SpriteBatcher(graphics, 250);
		touchPos=new Vector2();
		twinko=GameObjectsController.getInstance(game).getDragon();
		twinko.setBoundsSizeOrientationAndPosition(4, 3.5f, 6, 6, Dragon.EAST);
		twinkoRenderer = new TwinkoRenderer(twinko, batcher);
		skillNotification=new GameObject(6.5f, 14.5f, 1, 1);
		skillNotificationNo=new NumbersDisplay(skillNotification.position.x, skillNotification.position.y, 1);
		storeDisplayModel=new StoreDisplayModel(twinko.dragonBag);
		statsDisplayModel = new StatsDisplayModel(twinko.dragonStats);
		shopButton=new GameObject(12, 4.5f, 2, 2);
		fingerTouch=new Circle(-1, -1, 0.65f);
		
		levelCategories = new LevelCategoriesDiplayModel(this.facade);
		levels=new LevelIconsDisplayModel(this.facade);
		
		twinko.fireEmitter.setIntensity(3);
		fireBreathTarget = new DynamicGameObject(0,0,1,1);
		twinko.setRendererListener(twinkoRenderer);
		twinko.head.mouth.registerMouthRendererListeners(twinkoRenderer);
	}

	@Override
	public void update(float deltaTime) {
		List<TouchEvent> touchEvents=input.getTouchEvents();
		List<KeyEvent> keyEvents = input.getKeyEvents();
		
		int len=keyEvents.size();
		for (int i = 0; i < len; i++) {
			KeyEvent event=keyEvents.get(i);
			if (event.keyCode==android.view.KeyEvent.KEYCODE_BACK && event.type==KeyEvent.KEY_UP){
				switch (state){
				case NORMAL:
					//TODO implement "are you sure?"
					((GLGame)game).finish();
					break;
				case LEVELS:
					state = LEVEL_CATEGORIES;
					break;
				default:
					state = NORMAL;
					break;
				}
			}
		}
		
		len=touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event= touchEvents.get(i);
			touchPos.set(event.x, event.y);
			camera.touchToWorld(touchPos);
			fingerTouch.center.set(touchPos);
			switch (state){
			case STORE:
				if (storeDisplayModel.onTouchEvent(event, fingerTouch)){
					facade.open();
					facade.updateDragonBag(twinko.dragonBag);
					facade.close();
					Log.d("stats", twinko.dragonBag.toString());
				}
				break;
			case PLAYER_STATS:
				if (statsDisplayModel.onTouchEvent(event, fingerTouch)){
					state = NORMAL;
					facade.open();
					facade.updateDragonStatsOnSkillInc(twinko.dragonStats);
					facade.close();
				}
				if (event.type==TouchEvent.TOUCH_UP){
					onTwinkoTouch(fingerTouch);
				}
				break;
			case NORMAL:
				if (event.type==TouchEvent.TOUCH_UP){
					onTouchUp(fingerTouch);
					onTwinkoTouch(fingerTouch);
				}
				break;
			case LEVEL_CATEGORIES:
				if (event.type==TouchEvent.TOUCH_UP){
					onTouchUpLevelCategories(fingerTouch);
				}
				break;
			case LEVELS:
				if (event.type==TouchEvent.TOUCH_UP){
					onTouchUpLevels(fingerTouch);
				}
				break;
			}
		}
		
		listenForRandomAnimation(deltaTime);
		twinko.update(deltaTime);

		switch (state){
		case NORMAL:
			skillNotificationNo.update(deltaTime, twinko.dragonStats.unusedSkillPoints);
			play.update(deltaTime);//TODO remove crap play class!!
			break;
		case STORE:
			storeDisplayModel.update(deltaTime);
			break;
		case LEVEL_CATEGORIES:
			levelCategories.update(deltaTime);
			break;
		case PLAYER_STATS:
			statsDisplayModel.update(deltaTime);
			break;
		case LEVELS:
			levels.update(deltaTime);
			break;
		}
		//Can't quite remember what this does - some fix for touch event, probably??
		touchPos.set(-1, -1);
		fingerTouch.center.set(touchPos);
	}
	
	private void onTouchUp(Circle fingerTouch){
		if (OverlapTester.overlapCircleRectangle(fingerTouch, play.bounds)){
			state=LEVEL_CATEGORIES;
			return;
		}
		
		if (OverlapTester.pointInRectangle(twinko.bounds, touchPos)){
			state = PLAYER_STATS;
			Log.d("touch", "touched dragon state: "+state);
			return;
		}
		
		if (OverlapTester.overlapCircleRectangle(fingerTouch, shopButton.bounds)){
			state=STORE;
			return;
		}
	}
	
	/**
	 * captures the categoryId for the chosen category
	 * and passes the categoryId to the LevelIconsDisplayModel
	 * @param touch
	 */
	private void onTouchUpLevelCategories(Circle touch){
		byte categoryId = levelCategories.onTouchUp(touch);
		if (categoryId==LevelCategoriesDiplayModel.CATEGORY_JUNGLE){
			game.setScreen(new TestScreen(game));
		}
		if (categoryId != 0){
			state = LEVELS;
			levels.setState(categoryId);
		}
		
	}
	
	private void onTouchUpLevels(Circle touch){
		int levelId;
		if ((levelId=levels.onTouchUp(fingerTouch))!=0) {
			if (levelId==100) game.setScreen(new TestScreen(game));
			else game.setScreen(new GameScreen(game, levelId));
		}
	}

	@Override
	public void present(float deltaTime) {
		GL10 gl = graphics.getGL();
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT); 
		camera.setViewportAndMatrices();
		
		gl.glEnable(GL10.GL_TEXTURE_2D);
		
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
		
		batcher.beginBatch(Assets1.medowBackgroundAtlas);
		batcher.drawSprite(8, 4.5f, 20.48f, 10.24f, Assets1.medowBackground, SpriteBatcher.NORMAL);
		batcher.endBatch();
		
		twinkoRenderer.render(deltaTime, false);
		
		switch (state){
		case NORMAL:
			batcher.beginBatch(playAtlas);
			batcher.drawSprite(play.position.x, play.position.y, play.width, play.height, playAsset, SpriteBatcher.NORMAL);
			batcher.endBatch();
			
			batcher.beginBatch(Assets1.digitsAtlas);
//			if (twinko.dragonStats.unusedSkillPoints>0){
//				batcher.drawSprite(skillNotification.position.x, skillNotification.position.y, 2, 2, skillNotificationRegion, SpriteBatcher.NORMAL);
//				RendererHelper.renderNoDisplay(Assets1.digits, batcher, skillNotificationNo);
//			}
//			RendererHelper.renderNoDisplay(digitsAssets, batcher, staminaNo);
			batcher.drawSprite(shopButton.position.x, shopButton.position.y, 2, 2, Assets1.broccoliRegion, SpriteBatcher.NORMAL);
			batcher.endBatch();
			break;
		case STORE:
			StoreRenderer.renderStore(storeDisplayModel, batcher);
			break;
		case LEVEL_CATEGORIES:
			LevelsCategoriesRenderer.render(levelCategories, batcher);
			break;
		case LEVELS:
			LevelsRenderer.render(levels, batcher);
			break;
		case PLAYER_STATS:
			StatsRenderer.render(statsDisplayModel, batcher);
			break;
		}
		
		if (Fire.dead==false){
			gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE);
			batcher.beginBatch(Assets1.particlesAtlas);
			for (int i = 0; i < Fire.particleNumber; i++) {
				batcher.drawSprite(Fire.particles[i].position.x, Fire.particles[i].position.y, Fire.particles[i].textureSize, Fire.particles[i].textureSize, Assets1.particleRegion, SpriteBatcher.NORMAL);
			}
			batcher.endBatch();
		}
		
		gl.glDisable(GL10.GL_BLEND);
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
		playAtlas=new Texture((GLGame)game, "play_button_small.png");
		playAsset=new TextureRegion(playAtlas, 0, 0, 256, 64);
	}

	@Override
	public void dispose() {
		playAtlas.dispose();
	}
	
	public static void setState(byte state){
		MainScreen.state=state;
	}

	private class Play extends GameObject{

		public final byte hidden = 0;
		public final byte growing = 1;
		public final byte normal = 2;
		public final byte growSomeMore=3;
		
		public byte state;
		
		private float increaseBy=6f;
		
		public float width;
		public float height;
		
		public Play(float x, float y, float width, float height) {
			super(x, y, width, height);
			this.state=hidden;
			this.width=0;
			this.height=0;
		}
		
		public void update(float deltaTime){
			switch (state){
			case hidden:
				this.state=growing;
				break;
			case growing:
				grow(deltaTime);
				if (width>8.8f || height>2.2f) state=normal;
				break;
			case normal:
				break;
			case growSomeMore:
				grow(deltaTime);
				break;
			}
		}
		
		private void grow(float deltaTime){
			this.width+=4*increaseBy*deltaTime;
			this.height+=increaseBy*deltaTime;
		}
	}
	
	private void listenForRandomAnimation(float deltaTime){
		timeSinceLastRandomAnimation+=deltaTime;
		if (timeSinceLastRandomAnimation>=timeTillNextRandomAnimation){
			notifyListeners(Dragon.randomAnimation());
			timeSinceLastRandomAnimation = 0;
			timeTillNextRandomAnimation = (float) (3 + Math.random() * 5); 
		}
	}
	
	private void onTwinkoTouch(Circle finger){
		if (twinko.onTouch(finger)){
			notifyListeners(Dragon.randomAnimation());
			timeSinceLastRandomAnimation = 0;
			timeTillNextRandomAnimation = (float) (3 + Math.random() * 5);
		}
	}
	
	private void notifyListeners(byte animationId){
		switch(animationId){
		case Dragon.HAND_WAVE:
			twinko.setHandWaveState((float) (1.5f+Math.random()*2), false);
			break;
		case Dragon.CONFUSED:
			twinko.setConfusedState(false);
			break;
		case Dragon.FLEX:
			twinko.setFlexState(false);
			break;
		case Dragon.SHY:
			twinko.setShyState(false);
			break;
		case Dragon.FIRE_BREATH:
			twinko.setFireBreathState(null, false);
			break;
		case Dragon.SURPRISED:
			twinko.setSurprisedState(0.75f, false);
			break;
		}
	}
}
