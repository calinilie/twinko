package com.calin.tests;

public interface IPauseListener {

	public void onContinuePressed();
	
	public void onReplayPressed(int levelId);
	
	public void onShopPressed();
	
}
