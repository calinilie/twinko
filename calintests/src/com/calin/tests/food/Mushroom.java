package com.calin.tests.food;

public class Mushroom extends Stake{

	public float size;
	
	public Mushroom(float x, float y, float width, float height, float maxHP,
			float speed, float errorMarge, byte spot, int points) {
		super(x, y, width, height, maxHP, speed, errorMarge, spot, points);
		size=width;
		bounds.width=width*0.6f;
		bounds.height=height*0.6f;
	}

}
