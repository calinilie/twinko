package com.calin.tests.food;

import android.util.Log;

import com.calin.tests.framework.math.Vector2;

public class Stake extends Food{
	
	public float size;
		
	public static final byte TOP_FAR_LEFT=11;
	public static final byte TOP_LEFT=20;
	public static final byte TOP_LEFT_MIDDLE=22;
	public static final byte TOP_MIDDLE=10;
	public static final byte TOP_RIGHT_MIDDLE=23;
	public static final byte TOP_RIGHT=21; 
	public static final byte TOP_FAR_RIGHT=12;
	
	public static final byte BOTTOM_MIDDLE=13;
	public static final byte BOTTOM_RIGHT=14;
	public static final byte BOTTOM_RIGHT_MIDDLE=16;
	public static final byte BOTTOM_LEFT_MIDDLE=17;
	public static final byte BOTTOM_LEFT=15;
	public final byte spot;
	
	public String name;
	
	private Vector2 gravity;
	
	private Vector2 forCalculatingVelocity; 
	float angleForCalculatingVelocity;
	
	public float angle;
	
	public Stake(float x, float y, float width, float height, float maxHP, float speed, float errorMarge, byte spot, int points) {
		super(x, y, width, height, maxHP, speed, errorMarge, points);
		this.size = 1;
		this.gravity = new Vector2();
		this.spot=spot;
		forCalculatingVelocity=new Vector2();
		resetVelocityAndPositions(this.spot);
		angle=0;
	}
	
	@Override
	public void update(float deltaTime, boolean isTouched){
		move(deltaTime);
		super.update(deltaTime, isTouched);
		
	}
	
	@Override
	public void revive(){
		super.revive();
		resetVelocityAndPositions(this.spot);
	}
	
	void move(float deltaTime){
		velocity.add(gravity.x*deltaTime, gravity.y*deltaTime);
		angle += Math.max(0.9f, (1+ Math.abs(velocity.y))/3);
		position.add(velocity.x*deltaTime, velocity.y*deltaTime);
		bounds.lowerLeft.set(position).sub(bounds.width/2, bounds.height/2);
	}
	
	void resetVelocityAndPositions(short spot) {
		switch (spot) {
		case (Stake.TOP_MIDDLE):
			position.y = 10;
			//7-9
			position.x = (float) (7 + Math.random() * 2);
			
			gravity.set(0, -SPEED);
			velocity.set(0, -SPEED);
			break;
		case (Stake.TOP_FAR_LEFT):
			position.y = 10;
			//2-5
			position.x = (float)(2+Math.random()*3);
		
			gravity.set(0, -SPEED);
			velocity.set(0, -SPEED);
			break;
		case TOP_LEFT:
			position.y=10;
			//4-7
			position.x=(float) (4+Math.random()*3);
			
			gravity.set(0, -SPEED);
			velocity.set(0, -SPEED);
			break;
		case TOP_LEFT_MIDDLE:
			position.y=10;
			//6-8
			position.x=(float) (6+Math.random()*2);
			
			gravity.set(0, -SPEED);
			velocity.set(0, -SPEED);
			break;
		case TOP_RIGHT_MIDDLE:
			position.y=10;
			//8-10
			position.x=(float) (8+Math.random()*2);
			
			gravity.set(0, -SPEED);
			velocity.set(0, -SPEED);
			break;
		case TOP_RIGHT:
			position.y=10;
			//9-12
			position.x=(float) (9+Math.random()*3);
			
			gravity.set(0, -SPEED);
			velocity.set(0, -SPEED);
			break;
		case (Stake.TOP_FAR_RIGHT):
			position.y = 10;
			//11-14
			position.x = (float) (11 + Math.random() * 3);
			
			gravity.set(0, -SPEED);
			velocity.set(0, -SPEED);
			break;
		case (Stake.BOTTOM_LEFT):
			position.y=-1;
			position.x=1;
			
			forCalculatingVelocity.set((float) (position.x*2+Math.random()*2), 9);
			angleForCalculatingVelocity=forCalculatingVelocity.sub(this.position).angle()*Vector2.TO_RADIANS;
			velocity.x=(float)(Math.cos(angleForCalculatingVelocity)*SPEED);
			velocity.y=(float)(Math.sin(angleForCalculatingVelocity)*SPEED);
			
			gravity.y=-velocity.y/2;
			gravity.x=-velocity.x/10;
			break;
		case (Stake.BOTTOM_RIGHT):
			position.y=-1;
			position.x=15;
			
			forCalculatingVelocity.set((float) (position.x-1-Math.random()*2), 9);
			angleForCalculatingVelocity=forCalculatingVelocity.sub(this.position).angle()*Vector2.TO_RADIANS;
			velocity.x=(float)(Math.cos(angleForCalculatingVelocity)*SPEED);
			velocity.y=(float)(Math.sin(angleForCalculatingVelocity)*SPEED);
			
			gravity.y=-velocity.y/2;
			gravity.x=-velocity.x/10;
			break;
		case (Stake.BOTTOM_MIDDLE):
			position.y=-1;
			//7-9 8
			position.x=(float) (7+Math.random()*2);
			
			forCalculatingVelocity.set(8, 9);
			angleForCalculatingVelocity=forCalculatingVelocity.sub(this.position).angle()*Vector2.TO_RADIANS;
			velocity.x=(float)(Math.cos(angleForCalculatingVelocity)*SPEED);
			velocity.y=(float)(Math.sin(angleForCalculatingVelocity)*SPEED);
			
			gravity.y=-velocity.y/2;
			gravity.x=-velocity.x/10;
			break;
		case BOTTOM_LEFT_MIDDLE:
			position.y=-1;
			//6-8 8
			position.x=(float)(6+Math.random()*2);
			forCalculatingVelocity.set(8, 9);
			
			angleForCalculatingVelocity=forCalculatingVelocity.sub(this.position).angle()*Vector2.TO_RADIANS;
			velocity.x=(float)(Math.cos(angleForCalculatingVelocity)*SPEED);
			velocity.y=(float)(Math.sin(angleForCalculatingVelocity)*SPEED);
			
			gravity.y=-velocity.y/2;
			gravity.x=-velocity.x/10;
			break;
		case BOTTOM_RIGHT_MIDDLE:
			position.y=-1;
			//8-10 8
			position.x=(float)(8+Math.random()*2);
			forCalculatingVelocity.set(8, 9);
			
			angleForCalculatingVelocity=forCalculatingVelocity.sub(this.position).angle()*Vector2.TO_RADIANS;
			velocity.x=(float)(Math.cos(angleForCalculatingVelocity)*SPEED);
			velocity.y=(float)(Math.sin(angleForCalculatingVelocity)*SPEED);
			
			gravity.y=-velocity.y/2;
			gravity.x=-velocity.x/10;
			break;
		}
	}

//	public String toString(){
//		return ">>I am a "+name+". My curent State is: "+(state==Target.ALIVE ? "ALIVE":"DEAD");
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
	
	
	
//	public void inBasket(){
//	this.cookingState=Stake.MONEY_STATE;
//	angle = this.destination.position.sub(this.position).angle();
//	this.velocity.x=(float) Math.cos(angle*Vector2.TO_RADIANS)*(30);
//	this.velocity.y=(float) Math.sin(angle*Vector2.TO_RADIANS)*(30);
//}
}
