package com.calin.tests.food;

import com.calin.tests.objects.Target;

public abstract class Food extends Target{

	public byte cookingState;
	
	public static final byte RAW_STATE=9;
	public static final byte COOKED_STATE=6;
	public static final byte BURNED_STATE=3;
	
	public final float LOWER_MARGIN;
	public final float UPPER_MARGIN;
	
	public Food(float x, float y, float width, float height, float maxHP, float speed, float errorMarge, int points) {
		super(x, y, width, height, maxHP, speed, points);
		this.cookingState = Stake.RAW_STATE;
		this.LOWER_MARGIN = - errorMarge;
		this.UPPER_MARGIN =   errorMarge;

	}
	
	private void checkCookedState(boolean isTouched){
		if (!isTouched && cookingState==Stake.COOKED_STATE){
			this.kill(points);
		}
	}
	
	private void checkCookedHP(){
		if (curHP <= UPPER_MARGIN && curHP >= LOWER_MARGIN){
			this.cookingState = Stake.COOKED_STATE;
		}
	}
	
	private void checkBurnedHP(){
		if (curHP < LOWER_MARGIN)
			this.cookingState = Stake.BURNED_STATE;
	}
	
	@Override
	public void update(float deltaTime, boolean isTouched){
		if (state==ALIVE){
			switch (cookingState){
				case (Stake.RAW_STATE):
					checkCookedHP();
				break;
				case (Stake.COOKED_STATE):
					checkBurnedHP();
					checkCookedState(isTouched);
					break;
				case (Stake.BURNED_STATE):
					if (!isTouched) this.kill(0);
					break;
			}
			if (position.y<-1) this.kill(0);
		}
		else if (state==DEAD){
			this.position.set(-10, -10);
			setBounds(-10, -10, bounds.width, bounds.height);
		}
		
	}
	
	@Override
	public void revive(){
		super.revive();
		cookingState=Stake.RAW_STATE;
	}
}
