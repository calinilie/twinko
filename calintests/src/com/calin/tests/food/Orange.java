package com.calin.tests.food;

public class Orange extends Stake{

	public Orange(float x, float y, float width, float height, float maxHP,
			float speed, float errorMarge, byte spot, int points) {
		super(x, y, width, height, maxHP, speed, errorMarge, spot, points);
		size=width;
	}
	
	public String toString(){
		return "orange";
	}

}
