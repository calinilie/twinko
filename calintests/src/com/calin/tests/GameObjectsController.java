package com.calin.tests;
import android.util.Log;

import com.calin.tests.datasource.Facade;
import com.calin.tests.framework.Game;
import com.calin.tests.framework.impl.GLGame;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.DragonBag;
import com.calin.tests.objects.DragonStats;

public class GameObjectsController {
	
	
	private static GameObjectsController instance;

	
	private Dragon dragon;
	
	private GameObjectsController(Game game){
		dragon=new Dragon(0, 0, 0, 0);
		dragon.setBag(loadDragonBag(game));
		dragon.setStats(loadDragonStats(game));
	}
	
	public static GameObjectsController getInstance(Game game){
		if (instance==null){
			instance=new GameObjectsController(game);
			return instance;
		}
		return instance;
	}
	
//	public Dragon setDragonSizeAndPosition(float x, float y, float width, float height){
//		dragon.setBoundsSizeAndPosition(x, y, width, height);		
//		return dragon;
//	}
	
	public Dragon getDragon(){
		dragon.resetFire();
		return dragon;
	}
	
	private DragonStats loadDragonStats(Game game){
		Facade facade=((GLGame)game).getDataSourceFacade();
		facade.open();
		DragonStats dragonStats = facade.getDragonStats();
		facade.close();
		Log.d("stats", dragonStats.toString());
		return dragonStats;
	}
	
	private DragonBag loadDragonBag(Game game){
		Facade facade=((GLGame)game).getDataSourceFacade();
		facade.open();
		DragonBag bag=facade.getBag();
		facade.close();
		Log.d("stats", bag.toString());
		return bag;
	}
	
}
