package com.calin.tests;

public interface IDragonRendererListener {
	
	public boolean onConfusedEvent();
	
	public boolean onFlexStart();

	public boolean onShy();

}
