package com.calin.tests;

import com.calin.tests.objects.Enemy;

public interface IDragonHitListener {

	public void onDragonHitEvent(Enemy source, float damage);
	
}
