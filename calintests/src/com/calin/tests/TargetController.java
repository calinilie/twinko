package com.calin.tests;

import java.util.ArrayList;

import android.util.Log;

import com.calin.tests.food.Food;
import com.calin.tests.objects.Enemy;
import com.calin.tests.objects.Target;

public class TargetController implements IDeadTargetListener{
	
//	public Food[] food;
//	public Enemy[] enemies;
	public ArrayList<Food> food;
	public ArrayList<Enemy> enemies;
//	float passedTime=0;//time since last enemy was released
	private int maxEnemiesNo;//max enemies allowed on screen at once
	private int maxFoodNo;//max food allowed on screen at once
	byte currentFoodNo;//how many food targets are currently on screen
	byte currentEnemiesNo;//how many enemies are currently on screen
	byte nextEnemyReleaseIndex;//an index in the enemies array that represents which enemy will be released next
	byte nextFoodReleaseIndex;//an index in the food array that represents which food target will be released next
	
	Target currentTarget;
	
	public TargetController(int maxEnemiesNo, int maxFoodNo){
		this.maxEnemiesNo=maxEnemiesNo;
		this.maxFoodNo=maxFoodNo;
		currentEnemiesNo=0;
		currentFoodNo=0;
		nextEnemyReleaseIndex=0;
		nextFoodReleaseIndex=0;
	}
	
	private void nextEnemy(){
		nextEnemyReleaseIndex++;
		if (nextEnemyReleaseIndex>=enemies.size()) nextEnemyReleaseIndex=0;
	}
	
	public void update(float deltaTime){
//		Log.d("target_controller", currentEnemiesNo+" "+maxEnemiesNo+" "+nextEnemyReleaseIndex);
		if (maxEnemiesNo!=0){
			Enemy enemy=enemies.get(nextEnemyReleaseIndex);
			if (enemy.timePassed>=enemy.getReleaseAfter() && currentEnemiesNo<maxEnemiesNo){
				if (enemy.state==Target.DEAD){
					enemy.revive();
					nextEnemy();
//					for (Enemy e:enemies){
//						Log.d("target_controller", e.toString());
//					}
					currentEnemiesNo++;
				}
				else nextEnemy();
			}
			else nextEnemy();
		}
		
		while (currentFoodNo<maxFoodNo){
//			Log.d("target_controller", currentFoodNo+" "+maxFoodNo);
			Food f=food.get(nextFoodReleaseIndex);
			if (f.state==Target.DEAD){
				f.revive();
				currentFoodNo++;
			}
			nextFoodReleaseIndex++;
			if (nextFoodReleaseIndex>=food.size()) nextFoodReleaseIndex=0;
		}
	}
	
	public boolean removeFood(Food food){
		if (food.state==Food.DEAD){
			nextFoodReleaseIndex=0;
			this.food.remove(food);
			return true;
		}
		return false;
	}

	@Override
	public void onDeadTargetEvent(Target source, int score) {
		if (source instanceof Enemy) {
			currentEnemiesNo--;
			Log.d("target_controller", "ENEMY died, no: "+currentEnemiesNo);
		}
		else if (source instanceof Food) {
			currentFoodNo--;
			Log.d("target_controller", source.toString().toUpperCase()+" died, current food number: "+currentFoodNo);
		}
	}

	public int getMaxEnemiesNo() {
		return maxEnemiesNo;
	}

	public void setMaxEnemiesNo(int maxEnemiesNo) {
		this.maxEnemiesNo = maxEnemiesNo;
	}

	public int getMaxFoodNo() {
		return maxFoodNo;
	}

	public void setMaxFoodNo(int maxFoodNo) {
		this.maxFoodNo = maxFoodNo;
	}
	
	
}
