package com.calin.tests.framework.math;

public class WorkerVector2 extends Vector2{
	public Vector2 temp;
	
	public WorkerVector2(){
		super();
		temp=new Vector2();
	}
	
	public WorkerVector2(Vector2 other){
		super(other);
		temp=new Vector2(other);
	}
	
	public Vector2 set(float x, float y) {
		super.set(x, y);
		temp.set(x, y);
		return this;
	}

	public Vector2 set(Vector2 other) {
		super.set(other);
		temp.set(other);
		return this;
	}
	
	public Vector2 sub(float x, float y) {
		temp.set(this);
		temp.x -= x;
		temp.y -= y;
		return temp;
	}

	public Vector2 sub(Vector2 other) {
		temp.set(this);
		temp.x -= other.x;
		temp.y -= other.y;
		return temp;
	}
}
