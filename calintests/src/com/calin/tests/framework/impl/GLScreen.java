package com.calin.tests.framework.impl;

import com.calin.tests.framework.Game;
import com.calin.tests.framework.Screen;


public abstract class GLScreen extends Screen {
    protected final GLGraphics glGraphics;
    protected final GLGame glGame;
    
    public GLScreen(Game game) {
        super(game);
        glGame = (GLGame)game;
        glGraphics = ((GLGame)game).getGLGraphics();
    }

}
