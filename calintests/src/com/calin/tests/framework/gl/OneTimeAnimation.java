package com.calin.tests.framework.gl;

public class OneTimeAnimation {
	public byte state;
	float stateTime=0;
	
	public static final byte ANIMATION_PLAYING = 0;
	public static final byte ANIMATION_NOTPLAYING = 1;
	
	final TextureRegion[] keyFrames;
	final float frameDuration;
	 
	public OneTimeAnimation(float frameDuration,TextureRegion ... keyFrames) {
	   this.frameDuration = frameDuration;
	   this.keyFrames = keyFrames;
	}
	 
	public TextureRegion getKeyFrame(float deltaTime) {
		if (state==ANIMATION_PLAYING){
			this.stateTime+=deltaTime;
			int frameNumber = (int)(this.stateTime / frameDuration);
			frameNumber = frameNumber % keyFrames.length;
			if (frameNumber>=keyFrames.length-1) {
				this.state=ANIMATION_NOTPLAYING;
//				this.stateTime=0;
			}
			return keyFrames[frameNumber];
		}
		return null;
	}
	
	public void play(){
		state=ANIMATION_PLAYING;
		stateTime=0;
	}
}
