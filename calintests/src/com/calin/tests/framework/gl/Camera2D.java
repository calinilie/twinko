package com.calin.tests.framework.gl;

import javax.microedition.khronos.opengles.GL10;

import android.os.Vibrator;

import com.calin.tests.framework.impl.GLGraphics;
import com.calin.tests.framework.math.Rectangle;
import com.calin.tests.framework.math.Vector2;

public class Camera2D {
	public final Vector2 position;
	public float zoom;
	public final float frustumWidth;
	public final float frustumHeight;
	final GLGraphics glGraphics;
//	private static final byte SHAKIN_STATE = 3;
//	private static final byte SHAKIN_RIGHT = 1;
//	private static final byte SHAKIN_LEFT = 2;
//	private static final byte NORMAL = 0;
//	private static byte state;
//	private static byte shakin_direction;
	private final float ASPECT_RATIO;
	public Rectangle viewport;
	
	public Camera2D(GLGraphics glGraphics, float frustumWidth,
			float frustumHeight) {
		this.glGraphics = glGraphics;
		this.frustumWidth = frustumWidth;
		this.frustumHeight = frustumHeight;
		this.ASPECT_RATIO=frustumWidth/frustumHeight;
		this.position = new Vector2(frustumWidth / 2, frustumHeight / 2);
		this.zoom = 1f;
//		Camera2D.stopShake();
//		Camera2D.shakin_direction = Camera2D.SHAKIN_RIGHT;
		viewport=new Rectangle(0, 0, glGraphics.getWidth(), glGraphics.getHeight());
		resize(glGraphics.getWidth(), glGraphics.getHeight());
	}
	
	public void setViewportAndMatrices() {
        GL10 gl = glGraphics.getGL();
        gl.glViewport((int) viewport.lowerLeft.x, (int) viewport.lowerLeft.y,
                (int) viewport.width, (int) viewport.height);       
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrthof(position.x - frustumWidth * zoom / 2, 
                    position.x + frustumWidth * zoom/ 2, 
                    position.y - frustumHeight * zoom / 2, 
                    position.y + frustumHeight * zoom/ 2, 
                    1, -1);
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

	public void touchToWorld(Vector2 touch) {
		touch.x = (touch.x / (float) glGraphics.getWidth()) * frustumWidth * zoom;
		touch.y = (1 - touch.y / (float) glGraphics.getHeight()) * frustumHeight * zoom;
		touch.add(position).sub(frustumWidth * zoom / 2, frustumHeight * zoom / 2);
	}
//
//	private void move(float deltaTime, Vibrator vibrator) {
//		if (Camera2D.state == Camera2D.SHAKIN_STATE) {
//			zoom=0.98f;
//			vibrator.vibrate(2);
//			switch (Camera2D.shakin_direction) {
//			case Camera2D.SHAKIN_LEFT:
//				this.position.x -= 8f * deltaTime;
//				if (this.position.x < this.frustumWidth / 2 - 0.2f)
//					Camera2D.shakin_direction = Camera2D.SHAKIN_RIGHT;
//				break;
//			case Camera2D.SHAKIN_RIGHT:
//				this.position.x += 8f * deltaTime;
//				if (this.position.x > this.frustumWidth / 2 + 0.2f)
//					Camera2D.shakin_direction = Camera2D.SHAKIN_LEFT;
//				break;
//			}
//		} else {
//			zoom=1;
//			vibrator.cancel();
//			this.position.x = frustumWidth / 2;
//			this.position.y = frustumHeight / 2;
//		}
//	}
//
//	public static void shake() {
//		Camera2D.state = Camera2D.SHAKIN_STATE;
//	}
//
//	public static void stopShake() {
//		Camera2D.state = Camera2D.NORMAL;
//	}
	
	private void resize(int widthInPixels, int heightInPixels){
		float aspectRatio=(float)widthInPixels/(float)heightInPixels;
		float scale=1f;
		Vector2 crop= new Vector2(0, 0);
		if(aspectRatio > ASPECT_RATIO)
        {
            scale = (float)heightInPixels/(float)frustumHeight;
            crop.x = (widthInPixels - frustumWidth*scale)/2f;
        }
        else if(aspectRatio < ASPECT_RATIO)
        {
            scale = (float)widthInPixels/(float)frustumWidth;
            crop.y = (heightInPixels - frustumHeight*scale)/2f;
        }
        else
        {
            scale = (float)widthInPixels/(float)frustumWidth;
        }

        float w = (float)frustumWidth*scale;
        float h = (float)frustumHeight*scale;
        viewport = new Rectangle(crop.x, crop.y, w, h);
	}

}
