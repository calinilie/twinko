package com.calin.tests.framework.gl;

import javax.microedition.khronos.opengles.GL10;

import android.util.FloatMath;

import com.calin.tests.framework.impl.GLGraphics;
import com.calin.tests.framework.math.Vector2;


public class SpriteBatcher {      
	
	public final static byte NORMAL=0;
	public final static byte EXPAND_RIGHT=1;
	public final static byte EXPAND_TOP=2;
	
    final float[] verticesBuffer;
    int bufferIndex;
    final Vertices vertices;
    int numSprites;    
    
    public SpriteBatcher(GLGraphics glGraphics, int maxSprites) {                      
        this.vertices = new Vertices(glGraphics, maxSprites*4, maxSprites*6, false, true);
		this.verticesBuffer = new float[this.vertices.hasColor ? maxSprites * 4 * 8	: maxSprites * 4 * 4];
        this.bufferIndex = 0;
        this.numSprites = 0;
                
        short[] indices = new short[maxSprites*6];
        int len = indices.length;
        short j = 0;
        for (int i = 0; i < len; i += 6, j += 4) {
                indices[i + 0] = (short)(j + 0);
                indices[i + 1] = (short)(j + 1);
                indices[i + 2] = (short)(j + 2);
                indices[i + 3] = (short)(j + 2);
                indices[i + 4] = (short)(j + 3);
                indices[i + 5] = (short)(j + 0);
        }
        vertices.setIndices(indices, 0, indices.length);                
    }
    
    public SpriteBatcher(GLGraphics glGraphics, int maxSprites, boolean hasColor) {                      
        this.vertices = new Vertices(glGraphics, maxSprites*4, maxSprites*6, hasColor, true);
		this.verticesBuffer = new float[this.vertices.hasColor ? maxSprites * 4 * 8	: maxSprites * 4 * 4];
        this.bufferIndex = 0;
        this.numSprites = 0;
                
        short[] indices = new short[maxSprites*6];
        int len = indices.length;
        short j = 0;
        for (int i = 0; i < len; i += 6, j += 4) {
                indices[i + 0] = (short)(j + 0);
                indices[i + 1] = (short)(j + 1);
                indices[i + 2] = (short)(j + 2);
                indices[i + 3] = (short)(j + 2);
                indices[i + 4] = (short)(j + 3);
                indices[i + 5] = (short)(j + 0);
        }
        vertices.setIndices(indices, 0, indices.length);                
    }  
    
    public void beginBatch(Texture texture) {
        texture.bind();
        numSprites = 0;
        bufferIndex = 0;
    }
    
    public void endBatch() {
        vertices.setVertices(verticesBuffer, 0, bufferIndex);
        vertices.bind();
        vertices.draw(GL10.GL_TRIANGLES, 0, numSprites * 6);
        vertices.unbind();
    }
    
    public void drawSprite(float x, float y, float width, float height, TextureRegion region, byte increase) {
    	float halfWidth = width / 2;
        float halfHeight = height / 2;
        float x1, y1, x2, y2;
        switch (increase){
        case EXPAND_TOP:
        case EXPAND_RIGHT:
        	x1 = x;
            y1 = y;
            x2 = x + width;
            y2 = y + height;
        	break;
        case NORMAL:
        default:
        	 x1 = x - halfWidth;
             y1 = y - halfHeight;
             x2 = x + halfWidth;
             y2 = y + halfHeight;
        	break;
        }
        
        verticesBuffer[bufferIndex++] = x1;
        verticesBuffer[bufferIndex++] = y1;
        verticesBuffer[bufferIndex++] = region.u1;
        verticesBuffer[bufferIndex++] = region.v2;
        
        verticesBuffer[bufferIndex++] = x2;
        verticesBuffer[bufferIndex++] = y1;
        verticesBuffer[bufferIndex++] = region.u2;
        verticesBuffer[bufferIndex++] = region.v2;
        
        verticesBuffer[bufferIndex++] = x2;
        verticesBuffer[bufferIndex++] = y2;
        verticesBuffer[bufferIndex++] = region.u2;
        verticesBuffer[bufferIndex++] = region.v1;
        
        verticesBuffer[bufferIndex++] = x1;
        verticesBuffer[bufferIndex++] = y2;
        verticesBuffer[bufferIndex++] = region.u1;
        verticesBuffer[bufferIndex++] = region.v1;
        
        numSprites++;
    }
       
    public void drawSprite(float x, float y, float width, float height, float angle, TextureRegion region) {
        float halfWidth = width / 2;
        float halfHeight = height / 2;
        
        float rad = angle * Vector2.TO_RADIANS;
        float cos = FloatMath.cos(rad);
        float sin = FloatMath.sin(rad);
                
        float x1 = -halfWidth * cos - (-halfHeight) * sin;
        float y1 = -halfWidth * sin + (-halfHeight) * cos;
        float x2 = halfWidth * cos - (-halfHeight) * sin;
        float y2 = halfWidth * sin + (-halfHeight) * cos;
        float x3 = halfWidth * cos - halfHeight * sin;
        float y3 = halfWidth * sin + halfHeight * cos;
        float x4 = -halfWidth * cos - halfHeight * sin;
        float y4 = -halfWidth * sin + halfHeight * cos;
        
        x1 += x;
        y1 += y;
        x2 += x;
        y2 += y;
        x3 += x;
        y3 += y;
        x4 += x;
        y4 += y;
        
        verticesBuffer[bufferIndex++] = x1;
        verticesBuffer[bufferIndex++] = y1;
        verticesBuffer[bufferIndex++] = region.u1;
        verticesBuffer[bufferIndex++] = region.v2;
        
        verticesBuffer[bufferIndex++] = x2;
        verticesBuffer[bufferIndex++] = y2;
        verticesBuffer[bufferIndex++] = region.u2;
        verticesBuffer[bufferIndex++] = region.v2;
        
        verticesBuffer[bufferIndex++] = x3;
        verticesBuffer[bufferIndex++] = y3;
        verticesBuffer[bufferIndex++] = region.u2;
        verticesBuffer[bufferIndex++] = region.v1;
        
        verticesBuffer[bufferIndex++] = x4;
        verticesBuffer[bufferIndex++] = y4;
        verticesBuffer[bufferIndex++] = region.u1;
        verticesBuffer[bufferIndex++] = region.v1;
        
        numSprites++;
    }
    /**
     * 
     * @param x position on x axis
     * @param y position on y axis
     * @param width width of object in meters
     * @param height height of object in meter
     * @param red red channel
     * @param green green channel 
     * @param blue blue channel
     * @param alpha alpha channel
     * @param region texture region
     */
    public void drawSpriteRGBA(float x, float y, float width, float height, float red, float green, float blue, float alpha, TextureRegion region, int increase) {
        float halfWidth = width / 2;
        float halfHeight = height / 2;
        float x1, y1, x2, y2;
        switch (increase){
        case EXPAND_TOP:
        case EXPAND_RIGHT:
        	x1 = x;
            y1 = y;
            x2 = x + width;
            y2 = y + height;
        	break;
        case NORMAL:
        default:
        	 x1 = x - halfWidth;
             y1 = y - halfHeight;
             x2 = x + halfWidth;
             y2 = y + halfHeight;
        	break;
        }
        
        verticesBuffer[bufferIndex++] = x1;
        verticesBuffer[bufferIndex++] = y1;
        verticesBuffer[bufferIndex++] = red;
        verticesBuffer[bufferIndex++] = green;
        verticesBuffer[bufferIndex++] = blue;
        verticesBuffer[bufferIndex++] = alpha;
        verticesBuffer[bufferIndex++] = region.u1;
        verticesBuffer[bufferIndex++] = region.v2;
        
        verticesBuffer[bufferIndex++] = x2;
        verticesBuffer[bufferIndex++] = y1;
        verticesBuffer[bufferIndex++] = red;
        verticesBuffer[bufferIndex++] = green;
        verticesBuffer[bufferIndex++] = blue;
        verticesBuffer[bufferIndex++] = alpha;
        verticesBuffer[bufferIndex++] = region.u2;
        verticesBuffer[bufferIndex++] = region.v2;
        
        verticesBuffer[bufferIndex++] = x2;
        verticesBuffer[bufferIndex++] = y2;
        verticesBuffer[bufferIndex++] = red;
        verticesBuffer[bufferIndex++] = green;
        verticesBuffer[bufferIndex++] = blue;
        verticesBuffer[bufferIndex++] = alpha;
        verticesBuffer[bufferIndex++] = region.u2;
        verticesBuffer[bufferIndex++] = region.v1;
        
        verticesBuffer[bufferIndex++] = x1;
        verticesBuffer[bufferIndex++] = y2;
        verticesBuffer[bufferIndex++] = red;
        verticesBuffer[bufferIndex++] = green;
        verticesBuffer[bufferIndex++] = blue;
        verticesBuffer[bufferIndex++] = alpha;
        verticesBuffer[bufferIndex++] = region.u1;
        verticesBuffer[bufferIndex++] = region.v1;
        
        numSprites++;
    }
}