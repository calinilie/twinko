package com.calin.tests.objects;

import com.calin.tests.GameObject;
import com.calin.tests.framework.math.OverlapTester;
import com.calin.tests.framework.math.Vector2;

public class Bat extends Enemy{

	public final float DMG=10; 
	private static final float targetY=13;
	private static final float targetX=4;
	
	private final float initX;
	private final float initY;
	
	public float flyingTime;
	public final GameObject destination;
	
	private GameObject dummyDragon;
	
	private Dragon dragon;
		
	public Bat(float x, float y, float width, float height, float maxHP, float speed, Dragon dragon, int releaseAfter, int points) {
		super(x, y, width, height, maxHP, speed, releaseAfter, points, 0);
		this.destination=new GameObject(Bat.targetX, Bat.targetY, 0.5f, 0.5f);
		this.initX=x;
		this.initY=y;
//		super.state=Target.ALIVE;
		this.dragon=dragon;
		dummyDragon=new GameObject(0, 0, 0, 0);
		this.changeDirection(destination);
	}
	
	public void update(float deltaTime, boolean isTouched){
		switch (super.state) {
		case Target.ALIVE:
			this.move(deltaTime);
			if (OverlapTester.overlapRectangles(this.bounds, this.destination.bounds)){
				this.changeDirection(dummyDragon);
			}
			if (OverlapTester.overlapRectangles(this.bounds, dragon.bounds)){
				this.resetTopTarget();
				this.changeDirection(this.destination);
			}
			this.flyingTime+=deltaTime;
			break;
		case Target.DEAD:
			this.reset();
			break;
		}
	}
	
	private void move(float deltaTime){
		position.add(velocity.x*deltaTime, velocity.y*deltaTime);
		bounds.lowerLeft.set(position).sub(bounds.width/2, bounds.height/2);
	}
	
	private void changeDirection(GameObject target){
		float angle=target.position.sub(this.position).angle();
		this.velocity.x=(float)Math.cos(angle*Vector2.TO_RADIANS)*SPEED;
		this.velocity.y=(float)Math.sin(angle*Vector2.TO_RADIANS)*SPEED;
		//try to rethink this, as it is not necessary to reset dummy position when dragon is not the target
		dummyDragon.position.set(dragon.position);
	}
	
	private void resetTopTarget(){
		this.destination.position.set((float)(1+Math.random()*7), (float)(8+Math.random()*8));
		this.destination.bounds.lowerLeft.set(this.destination.position).sub(destination.bounds.width/2, destination.bounds.height/2);
	}
	
	private void reset(){
		this.position.set(this.initX, this.initY);
		bounds.lowerLeft.set(position.x-bounds.width/2, position.y-bounds.height/2);
		destination.position.set(targetX, targetY);
		destination.bounds.lowerLeft.set(destination.position);
		this.changeDirection(destination);
	}
	
//	public String toString(){
//		return "I am a BAT. My curent State is: "+(state==Target.ALIVE ? "ALIVE":"DEAD");
//	}
}