package com.calin.tests.objects;

import com.calin.tests.DynamicGameObject;
import com.calin.tests.IDeadTargetListener;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.OverlapTester;

public abstract class Target extends DynamicGameObject {

	private IDeadTargetListener[] listeners;
	
	public byte state;
	public static final byte ALIVE=1;
	public static final byte DEAD=2;
	
	public final float maxHP;
	public float curHP;
	
	public float SPEED; 
	
	public final int points;
	
	public Target(float x, float y, float width, float height, float maxHP, float speed,int points) {
		super(x, y, width, height);
		this.maxHP=maxHP;
		this.curHP=this.maxHP;
		this.state=DEAD;
		this.SPEED=speed;
		this.points=points;
	}
	
	public void takeDamage(float dmg){
		this.curHP-=dmg;
	}
	
	public void kill(int score){
		if (state==ALIVE){
			state=DEAD;
			this.notifiyListeners(score);
		}
	}
	
	public boolean checkCollision(Circle touchPos){
		if (OverlapTester.overlapCircleRectangle(touchPos, this.bounds)) return true;
		return false;
	}
	
	public abstract void update(float deltaTime, boolean isTouched);
		
	public void revive(){
		this.state=ALIVE;
		curHP=maxHP;
	}

	public void subscribeDeadTargetListeners(IDeadTargetListener ... listener) {
		this.listeners = listener;
	}
	
	private void notifiyListeners(int score){
		for (IDeadTargetListener d : listeners){
			d.onDeadTargetEvent(this, score);
		}
	}
	
}
