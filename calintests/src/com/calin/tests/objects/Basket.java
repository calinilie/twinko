package com.calin.tests.objects;

import com.calin.tests.DynamicGameObject;

public class Basket extends DynamicGameObject {

	public int capacity = 3;
	public int occupied = 0;

	public final float SPEED = 40 ;

	public Basket(float x, float y, float width, float height) {
		super(x, y, width, height);
		this.bounds.lowerLeft.x+=0.7f;
		this.bounds.lowerLeft.y+=0.8f;
		this.bounds.width=1.8f;
		this.bounds.height=0.1f;
	}

	public void update(float deltaTime, float accelX, float width) {
		if ((bounds.lowerLeft.x <= 0.5f && (-accelX) > 0)
				|| ((bounds.lowerLeft.x + bounds.width) >= width-0.5f && (-accelX) < 0)
				|| (bounds.lowerLeft.x > 0.5f && (bounds.lowerLeft.x + bounds.width) < width-0.5f)) {
			this.velocity.x = -accelX / 10 * SPEED;
			this.position.x += velocity.x * deltaTime;
			this.bounds.lowerLeft.x=this.position.x-bounds.width/2;
		}
	}

}
