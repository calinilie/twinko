package com.calin.tests.objects;

import android.util.Log;

public class FailNotifier {
	
	public LostFruit first;
	public LostFruit second;
	public LostFruit last;

	public FailNotifier(float x, float y){
		first=new LostFruit(x, y, 1, 1);
		second=new LostFruit(x+1, y, 0.8f, 0.8f);
		last=new LostFruit(x+1.8f, y, 0.6f, 0.6f);
	}
	
	public void update(float deltaTime){
		first.pulseOnce(deltaTime);
//		Log.d("notifier", String.format("height %1$f width %2$f", first.pulseHeight, first.pulseWidth));
		second.pulseOnce(deltaTime);
		last.pulseOnce(deltaTime);
		
	}
	
	public void pulseFirst(){
		first.pulseState=PulseObject.PULSE;
	}
	
	public void pulseSecond(){
		second.pulseState=PulseObject.PULSE;
	}
	
	public void pulseLast(){
		last.pulseState=PulseObject.PULSE;
	}
	
	public class LostFruit extends PulseObject{
		
		public float x;
		public float y;
		
		public LostFruit(float x, float y, float width, float height) {
			super(0, 0);
			this.x=x;
			this.y=y;
			
			
			pulseState=NORMAL;
			minHeight=height;
			maxHeight=minHeight+0.2f;
			barWidthInc=minHeight/2;
			barHeightInc=minHeight/2;
			SPEED=10f;
			
//			Log.d("notifier", String.format("minHeight: %1$f maxHeight: %2$f widthinc %3$f heightinc %4$f", minHeight, maxHeight, barWidthInc, barHeightInc));
		}

		@Override
		public void setSizesAndRates() {
		}
		
	}
}
