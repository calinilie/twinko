package com.calin.tests.objects;

public class Coins {
	private int goldQty;
	private int silverQty;

	public Coins(int goldQty, int silverQty) {
		this.goldQty = goldQty;
		this.silverQty = silverQty;
	}

	public boolean descrease(int value) {
		if (goldQty - value >= 0) {
			this.goldQty -= value;
			return true;
		}
		return false;
	}

	public void increaseBy(int value) {
		this.goldQty += value;
	}

	public void incSilverBy(int value) {
		silverQty += value;
		if (silverQty % 100 > 0) {
			silverQty = silverQty & 100;
			goldQty++;
		}
	}
	
	public int getGoldQty() {
		return goldQty;
	}

	public int getSilverQty() {
		return silverQty;
	}

	@Override
	public String toString() {
		return "Coins [goldQty=" + goldQty + ", silverQty=" + silverQty + "]";
	}
	
	
}
