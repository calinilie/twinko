package com.calin.tests.objects;

public abstract class PulseObject {
	
	public static final byte PULSE=1;
	public static final byte NORMAL=2;
	public byte pulseState=PULSE;
	
	private final byte grow=11;
	private final byte shrink=12;
	private final byte shrinkToZero=13;
	private byte growState=grow;
	
	protected float barWidthInc;
	protected float barHeightInc;
	protected float maxHeight;
	protected float minHeight;
	protected float SPEED;
	
	public float pulseHeight;
	public float pulseWidth;
	
	private int counter=0;
	
	public PulseObject(float width, float height){
		pulseWidth=width;
		pulseHeight=height;
		this.setSizesAndRates();
	}
	
	
	public void pulse(){
		counter=0;
		growState=grow;
		this.pulseState=PULSE;
	}
	
	public void stopPulse(){
		this.pulseState=NORMAL;
	}
	
	public void updatePulse(float deltaTime){
		if (pulseState==PULSE){
			switch(growState){
			case grow:
				if (grow(deltaTime)) this.growState=shrink;
				break;
			case shrink:
				if (shrink(deltaTime)) this.growState=grow;
				break;
			}
		}
		else {
			pulseHeight=minHeight;
		}
	}
	
	public void pulseOnce(float deltaTime){
		if (pulseState==PULSE){
			switch(growState){
			case grow:
				if (grow(deltaTime)) this.growState=shrink;
				break;
			case shrink:
				if (shrink(deltaTime)) {
					this.pulseState=NORMAL;
					pulseHeight=minHeight;
				}
				break;
			}
		}
	}
	
	public void pulseTwice(float deltaTime){
		if (pulseState==PULSE){
			switch(growState){
			case grow:
				if (grow(deltaTime)) this.growState=shrink;
				break;
			case shrink:
				if (shrink(deltaTime)) {
					this.growState=grow;
					counter++;
					if (counter==2) this.growState=shrinkToZero;
				}
				break;
			case shrinkToZero:
				if (shrinkToZero(deltaTime)) this.pulseState=NORMAL;
				break;
			}
		}
	}
	
	private boolean grow(float deltaTime){
		pulseHeight+=barHeightInc*deltaTime*SPEED;
		pulseWidth+=barWidthInc*deltaTime*SPEED;
		
		return pulseHeight>=maxHeight;
	}
	
	private boolean shrink(float deltaTime){
		pulseHeight-=barHeightInc*deltaTime*SPEED;
		pulseWidth-=barWidthInc*deltaTime*SPEED;
		
		return pulseHeight<=minHeight;
	}
	
	private boolean shrinkToZero(float deltaTime){
		pulseHeight-=barHeightInc*deltaTime*SPEED;
		pulseWidth-=barWidthInc*deltaTime*SPEED;
		
		return pulseHeight<=0;
	}
	
	public abstract void setSizesAndRates();
}
