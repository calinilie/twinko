package com.calin.tests.objects;

import android.util.Log;

import com.calin.tests.IDragonHitListener;
import com.calin.tests.IDragonRendererListener;
import com.calin.tests.framework.math.OverlapTester;
import com.calin.tests.framework.math.Rectangle;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.framework.math.WorkerVector2;

public class Bee extends Enemy{

	public byte flyingState;
	public static final byte FLYING=1;
	public static final byte NOT_FLYING=2;
	float noFlyingTimer=0;
	public float flyingTime=0;
	
	public static final byte MOOD_ANGRY=6;
	public static final byte MOOD_NEUTRAL=7; 
	public byte mood=MOOD_NEUTRAL;
	
	public static final byte EYES_CLOSED=11;
	public static final byte EYES_OPENED=12;
	public byte eyes=EYES_CLOSED;
	
	public byte sceneState;
	public static final byte IN_BACKGROUND=16;
	public static final byte IN_FOREGROUND=17;
	
	public byte orientation;
	public static final byte WEST=21;
	public static final byte EAST=22;
	
	public static final byte INC=26;
	public static final byte DEC=27;
	public static final byte NORMAL_SIZE=28;
	public byte sizeState=NORMAL_SIZE;
	
	private float incRate=0;
	
	public float size;
	public final float MAX_SIZE;
	public final float MIN_SIZE;
	
	float passedTime=0;
	private float closeEyesAfter=1;
	private float openEyesAfter=0.3f;

	
	private Vector2 dragonPosition;
	private Rectangle dragonBounds;
	private Vector2 initialPosition;
	private Vector2 intermediateBeePosition;
	
	private WorkerVector2 destinationPosition=new WorkerVector2();
	float destMinY, destMaxY, destYInc, lowerLimit;

	public Bee(float x, float y, float width, float height, float maxHP,
			float speed, int releaseAfter, int points, float damage,Vector2 dragonPosition, Rectangle dragonBounds, Vector2 intermediatePosition) {
		super(x, y, width, height, maxHP, speed, releaseAfter, points, damage);
		MAX_SIZE=width;
		MIN_SIZE=width/3;
		initialPosition=new WorkerVector2();
		initialPosition.set(position);
		
		this.dragonPosition=new WorkerVector2();
		this.dragonPosition.set(dragonPosition.x, dragonPosition.y-1);
		this.dragonBounds=dragonBounds;
		
		this.intermediateBeePosition=intermediatePosition;
		reset(initialPosition, intermediateBeePosition, MIN_SIZE, true, Bee.MOOD_NEUTRAL);
	}

	@Override
	public void update(float deltaTime, boolean isTouched) {
		timePassed+=deltaTime;
		if (state==ALIVE){
			this.checkCollisions();
			updateEyes(deltaTime);
			switch (flyingState){
			case FLYING:
				move(deltaTime);
				switch (sizeState){
				case INC:
					updateGrowState(deltaTime);
					break;
				case DEC:
	//				updateShrinkState(deltaTime);
					break;
				case NORMAL_SIZE:
					break;
				}
				break;
			case NOT_FLYING:
				updateNotFlying(deltaTime);
				break;
			}
		}
	}
	
	void checkCollisions(){
		if (OverlapTester.overlapRectangles(this.bounds, this.dragonBounds) && mood==Bee.MOOD_ANGRY){
			this.reset(position, intermediateBeePosition, size, false, Bee.MOOD_NEUTRAL);
			this.notifyListeners();
		}
		if (OverlapTester.pointInRectangle(this.bounds, intermediateBeePosition)){
			this.reset(position, dragonPosition, size, false, Bee.MOOD_ANGRY);
		}
	}
	
	private void notifyListeners(){
		for (IDragonHitListener listener: listeners){
			listener.onDragonHitEvent(this, this.damage);
		}
	}
	
	@Override
	public void kill(int score) {
		super.kill(score);
		this.reset(initialPosition, intermediateBeePosition, MIN_SIZE, true, Bee.MOOD_NEUTRAL);
//		Log.d("bee", "bee died "+initialPosition.toString()+ " cp "+position.toString());
	}
	
	void updateEyes(float deltaTime){
		switch (eyes) {
		case EYES_CLOSED:
			if (passedTime>=openEyesAfter){
				eyes=EYES_OPENED;
				passedTime=0;
			}
			break;
		default:
			if (passedTime>=closeEyesAfter){
				eyes=EYES_CLOSED;
				passedTime=0;
			}
			break;
		}
		passedTime+=deltaTime;
	}
	
	void updateGrowState(float deltaTime){
		this.size+=incRate*deltaTime;
		if (size>MAX_SIZE) {
			size=MAX_SIZE;
			sizeState=NORMAL_SIZE;
//			mood=MOOD_ANGRY;
		}
	}
	
//	void updateShrinkState(float deltaTime){
//		this.size-=incRate*deltaTime;
//		if (size<=MIN_SIZE) {
//			size=MIN_SIZE;
//			noMoreFlying();
//		}
//	}
	
//	private void noMoreFlying(){
//		this.flyingState=NOT_FLYING;
//		this.noFlyingTimer=0;
//	}
	
	void updateNotFlying(float deltaTime){
		this.orientation=EAST;
		noFlyingTimer+=deltaTime;
//		if (noFlyingTimer>3) reset();
	}
	
	private void grow(WorkerVector2 targetPosition){
		if (sizeState!=INC){
			this.sizeState=INC;
			this.getIncRate(targetPosition, false, MAX_SIZE);
		}
	}
	
//	public void shrink(Vector2 targetPosition){
//		if (sizeState!=DEC){
//			this.computeVelocity(initialPosition);
//			this.sizeState=DEC;
//			accel.set(0, 0);
//			this.getIncRate(initialPosition, false, MIN_SIZE);
//		}
//	}
	
	private void computeVelocity(WorkerVector2 taregtPosition){
		float angle=taregtPosition.sub(position).angle()*Vector2.TO_RADIANS;
		velocity.x=(float) (Math.cos(angle)*SPEED);
		velocity.y=(float) (Math.sin(angle)*SPEED);
		
		if (velocity.x<0) this.orientation=WEST;
		else orientation=EAST;
	}
	
	private void move(float deltaTime){
		updateTargetPosition(deltaTime);
		
		this.computeVelocity(destinationPosition);
		
		flyingTime+=deltaTime;
		this.position.add(velocity.x*deltaTime, velocity.y*deltaTime);
		this.setBounds(position.x, position.y, size, size);
	}
	
	void updateTargetPosition(float deltaTime){
		if (destinationPosition.y<=destMinY) destYInc=8;
		else if (destinationPosition.y>=destMaxY) destYInc=-8;
		
//		if (destMinY<lowerLimit){
//			destMinY+=0.1*deltaTime;
//			destMaxY-=0.1*deltaTime;
//		}
		
		this.destinationPosition.y+=destYInc*deltaTime;
	}
	
	public void reset(Vector2 startposition, Vector2 destinationPosition, float size ,boolean grow, byte mood){
		this.position.set(startposition);		
		this.size=size;
		setBounds(position.x, position.y, size, size);
		
		
		destMinY=destinationPosition.y-(MAX_SIZE*2);//carefull with max size!
		destMaxY=destinationPosition.y+(MAX_SIZE*2);
		this.destinationPosition.set(destinationPosition.x, destMinY);
		lowerLimit=destinationPosition.y-MAX_SIZE/2;
		
		
		flyingState=FLYING;
		sizeState=NORMAL_SIZE;
		eyes=EYES_OPENED;
		this.mood=mood;
		
		if (grow) {
			this.grow(this.destinationPosition);
		}
	}
	
	private void getIncRate(WorkerVector2 targetPosition, boolean half, float desiredSize){
		float time=(float) this.getTimeToTarget(targetPosition);
		if (half) time /= 3;
		this.incRate=((float) (Math.abs(desiredSize-size)) )/ ((float)time);
	}
	
	private float getTimeToTarget(WorkerVector2 targetPosition){
		this.computeVelocity(targetPosition);
		//get distance
		float distanceToTargetOnX=Math.abs(targetPosition.x-position.x);
		float distanceToTargetOnY=Math.abs(targetPosition.y-position.y);
		//get time
		float deltaTimeForX=distanceToTargetOnX/velocity.x;
		float deltaTimeForY=distanceToTargetOnY/velocity.y;
		//get average travel time -> check for 0 values;
		float deltaTime=(Math.abs(deltaTimeForX)+Math.abs(deltaTimeForY))/2;
		return deltaTime;
	}
	
}
