package com.calin.tests.objects;

public interface BuyListener {
	
	public void onBuy(int id);

}
