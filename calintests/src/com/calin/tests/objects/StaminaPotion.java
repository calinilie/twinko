package com.calin.tests.objects;

public class StaminaPotion extends Potion{
	
	public static final int plusStamina=25;
	
	public StaminaPotion(int price, int quantity){
		super(price, quantity);
	}
	
}
