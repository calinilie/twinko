package com.calin.tests.objects;

import com.calin.tests.IDragonRendererListener;
import com.calin.tests.IMouthRendererListener;
import com.calin.tests.framework.math.Vector2;

public class DragonMouth{
	
	public static Vector2 pos;
	public static float width;
	public static float height;
	
	public boolean fireAnimationStarted=false;
	public boolean isHit=false;
	public boolean fireOK=false;
	public boolean isConfused=false;
	
	float passedTime=0;
	
	IMouthRendererListener[] listeners;

	public DragonMouth(float size){
		pos=new Vector2();
		width=(Dragon.orientation==Dragon.WEST) ? -size/2 : size/2;
		height=size/2;
	}
	
	public void update(float deltaTime, float headAngle, Vector2 headPos, boolean isTouched){
		if (!isHit) listenAnimationAndFire(isTouched, deltaTime);
		
	}
	
	public static void updatePosition(float deltaTime, float headAngle, Vector2 headPos){
		switch (Dragon.orientation) {
		case Dragon.WEST:
			updateOrientationWest(deltaTime, headAngle, headPos);
			break;
		case Dragon.EAST:
			updateOrientationEast(deltaTime, headAngle, headPos);
			break;
		}
	}
	
	void listenAnimationAndFire(boolean isTouched, float deltaTime){
		if (isTouched && !fireAnimationStarted) {
			fireAnimationStarted=true;
			notifyListener();
		}
		else if (!isTouched){
			fireAnimationStarted=false;
			fireOK=false;
		}
		
		if (fireAnimationStarted && !fireOK){
			passedTime+=deltaTime;
			if (passedTime>=0.4f){
				fireOK=true;
				passedTime=0;
			}
		}
	}
	
	static void updateOrientationWest(float deltaTime, float headAngle, Vector2 headPos){
		float angle=headAngle+180+42;
		pos.x = (float) (headPos.x - (width/1.25)*Math.cos((angle)*Vector2.TO_RADIANS));
		pos.y = (float) (headPos.y + (height/1.25)*Math.sin((angle)*Vector2.TO_RADIANS));
	}
	
	static void updateOrientationEast(float deltaTime, float headAngle, Vector2 headPos){
		pos.x = (float) (headPos.x + (width/1.25)*Math.cos((headAngle-42)*Vector2.TO_RADIANS));
		pos.y = (float) (headPos.y + (height/1.25)*Math.sin((headAngle-42)*Vector2.TO_RADIANS));
	}
	
	void notifyListener(){
		for (IMouthRendererListener d :listeners)
			d.startFireBreathingAnimation();
	}
	
	public void registerMouthRendererListeners(IMouthRendererListener...listeners){
		this.listeners=listeners;
	}
	
	public void afteHit(){
		isHit=false;
	}

	public void onDragonHit() {
		isHit=true;
		fireOK=false;
		fireAnimationStarted=false;
	}
	
	public void onConfused(){
		isConfused=true;
	}
	
	public void afterConfused(){
		isConfused=false;
	}
}
