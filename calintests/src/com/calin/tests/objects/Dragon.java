package com.calin.tests.objects;

import android.util.Log;

import com.calin.tests.Drag;
import com.calin.tests.IDragonHitListener;
import com.calin.tests.IDragonRendererListener;
import com.calin.tests.DynamicGameObject;
import com.calin.tests.GameObject;
import com.calin.tests.TestScreen;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.OverlapTester;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.particles.Fire;
import com.calin.tests.particles.Particle;

public class Dragon extends GameObject implements IDragonHitListener{
	
	public static final byte WEST=101;
	public static final byte EAST=102;
	
	public static final byte FIRE_BREATHING=1;
	public static final byte NORMAL=2;
	public static final byte WASTED=4;
	public static final byte LOW_STAMINA=3;
	public static final byte NO_HP=5;
	public static final byte HIT=6;
	public static final byte CONFUSED=7;
	public static final byte FLEX=8;
	public static final byte SHY=9;
	public static final byte HAND_WAVE=10;
	public static final byte FIRE_BREATH=11;
	public static final byte SURPRISED = 12;
	public byte state;
	
	public DragonStats dragonStats;
	public DragonBag dragonBag;
	
	public DragonHead head;
	public Fire fireEmitter;
	private DynamicGameObject fireBreathTarget;
	public DragonHand leftHand;
	private IDragonRendererListener rendererListener;
	
	public float width;
	public float height;
	public static byte orientation;
	
	public float tailAngle=0;
	float tailIncRate=5;
	public float wingAngle=0;
	float wingIncRate=2;
	
	static final float UP_DOWN_MOVE_SPEED=0.05f;
	private float maxY, minY;
	final byte MOVE_UP=1;
	final byte MOVE_DOWN=2;
	final byte NO_MOVING=3;
	byte movingState=MOVE_UP;
	float upAndDowPassedTime=0;
	
	private static byte nextState = 0;
	private float surprisedDuration = 1.5f; 
	public static final float hitDuration=0.80f;
	public static final float confusedDuration=0.75f;
	public static final float flexDuration=0.75f;
	public static final float scaredDuration=1.8f;
	private float handWaveDuration=20;
	float elapsedTime=0;
			
	public Dragon(float x, float y, float width, float height) {
		super(x, y, width, height);
		fireEmitter = new Fire();
		fireBreathTarget = new DynamicGameObject(-1, -1, 1, 1);
		state=Dragon.NORMAL;
	}
	
	public void update(float deltaTime){
		this.update(fireBreathTarget, false, 100, deltaTime);
	}
	
	public void update(DynamicGameObject target, boolean isTouched, float tempStamina, float deltaTime){
		//listenRandomAnimation - does not interrupt current animation
		switch (state) {
		case HIT:
			updateHit(deltaTime);
			break;
		case CONFUSED:
			updateConfused(deltaTime);
			break;
		case FLEX:
			updateFlex(deltaTime);
			break;
		case SHY:
			updateScared(deltaTime);
			break;
		case HAND_WAVE:
			leftHand.update(deltaTime);
			updateHandWave(deltaTime);
			break;
		case FIRE_BREATH:
			isTouched = true;
			updateFireBreathState(deltaTime);
			break;
		case SURPRISED:
			updateSurprised(deltaTime);
			break;
		case NORMAL:
		default:
			updateNormal(deltaTime, isTouched, target);
			break;
		}
		fireEmitter.update(deltaTime, target, isTouched, tempStamina>0, head.mouth.fireOK);
		head.update(deltaTime, (target!=null) ? target.position : null, isTouched, position, elapsedTime);
		dragonBag.update(deltaTime);
		if (state!=SURPRISED) moveUpAndDown(deltaTime);
	}
	
	//external init
	public void setStats(DragonStats dragonStats){
		this.dragonStats=dragonStats;
		dragonBag.subscribeDamagePotionOverListeners(this.dragonStats);
	}
	
	public void setBag(DragonBag dragonBag){
		this.dragonBag=dragonBag;
	}
	
	public void setBoundsSizeOrientationAndPosition(float x, float y, float width, float height, byte orientation){
		Dragon.orientation=orientation;
		position.x=x;
		position.y=y;
		maxY=y+0.10f;
		minY=y;
		
		if (Dragon.orientation==EAST) this.width=width;
		else this.width=-width;
		this.height=height;
		
		if (orientation==Dragon.WEST) setBounds(x, y, Math.abs(this.width/3), this.height);
		else setBounds(x, y, this.width/3, this.height);
		
		if (orientation==Dragon.WEST){
			tailIncRate=-5;
			wingIncRate=-2;
		}
		else {
			tailIncRate=5;
			wingIncRate=2;
		}
	
		head=new DragonHead(this.position, this.width, this.height);
		leftHand = new DragonHand(orientation);
	}
	
	//external events fired

	public boolean setConfusedState(boolean overrideCurrentState) {
		if (state==NORMAL){
			resetState();
			head.eyes.confused();
			head.mouth.onConfused();
			state=CONFUSED;
			rendererListener.onConfusedEvent();
			Log.d("state", "onConfusedEvent");
			return true;
		}
		return false;
	}

	public boolean setFlexState(boolean overrideCurrentState) {
		if (state==NORMAL){
			resetState();
			head.eyes.angry();
			state=FLEX;
			rendererListener.onFlexStart();
			Log.d("state", "onFlexStart");
			return true;
		}
		return false;
 	}
	
	public boolean setShyState(boolean overrideCurrentState){
		if (state==Dragon.NORMAL){
			resetState();
			head.eyes.shy();
			head.scared();
			state=SHY;
			rendererListener.onShy();
			Log.d("state", "onShy");
			return true;
		}
		return false;
	}
	
	public boolean setHandWaveState(float duration, boolean overrideCurrentState){
		if (state==NORMAL){
			resetState();
			handWaveDuration = duration;
			leftHand.wave();
			state=HAND_WAVE;
			Log.d("state", "onHandWave");
			return true;
		}
		return false;
	}
	
	public boolean setFireBreathState(Vector2 target, boolean overrideCurrentState){
		if (state==Dragon.NORMAL){
			if (target != null) fireBreathTarget.position.set(target);
			else {
				if (Dragon.orientation == Dragon.WEST){
					fireBreathTarget.position.set(15, (float) (1 + Math.random()*8));
				}
				else{
					fireBreathTarget.position.set(15, (float) (1 + Math.random()*8));
				}
			}
			elapsedTime = 0;
			state = Dragon.FIRE_BREATH;
			head.eyes.angry();
			return true;
		}
		return false;
	}
	
	@Override
	public void onDragonHitEvent(Enemy source, float damage) {
		resetState();
		head.onDragonHit();
		head.mouth.onDragonHit();
		head.eyes.onDragonHit();
		state=HIT;
	}
	
	public boolean onTouch(Circle fingerTouch){
		return (OverlapTester.overlapCircleRectangle(fingerTouch, bounds) || OverlapTester.overlapCircleRectangle(fingerTouch, head.bounds));
	}
	
	public boolean setSurprisedState(float duration, boolean ovverrideCurrentState) {
		if (state==NORMAL){
			resetState();
			surprisedDuration = duration;
			state=SURPRISED;
			head.eyes.surprised();
			position.y = maxY;
			Log.d("state", "onSurprised");
			return true;
		}
		return false;
	}
	
	public static byte randomAnimation(){
		byte animation = (byte) (7 + Math.random()*5);
		switch(animation){
		case Dragon.FIRE_BREATH:
			nextState = Dragon.FIRE_BREATH;
//			break;
			return Dragon.SURPRISED;
		case Dragon.FLEX:
			nextState = Dragon.FLEX;
//			break;
			return Dragon.SURPRISED;
		case Dragon.CONFUSED:
			nextState = Dragon.CONFUSED;
//			break;
			return Dragon.SURPRISED;
		default:
			return animation;
		}
//		return animation;
	}
	
	//state updates
	private void updateHit(float deltaTime){
		if (elapsedTime<hitDuration){
			elapsedTime+=deltaTime;
		}
		else {
			resetState();
//			onScared();
		}
	}
	
	private void updateConfused(float deltaTime){
		if (elapsedTime<confusedDuration){
			elapsedTime+=deltaTime;
//			head.angle=50;
		}
		else {
			resetState();
		}
	}
	
	private void updateFlex(float deltaTime){
		if (elapsedTime<flexDuration){
			elapsedTime+=deltaTime;
		}
		else {
			resetState();
		}
	}
	
	private void updateScared(float deltaTime){
		if (elapsedTime<scaredDuration){
			elapsedTime+=deltaTime;
		}
		else{
			resetState();
		}
	}

	private void updateNormal(float deltaTime, boolean isTouched, DynamicGameObject target){
		if (isTouched && target!=null){
			state=Dragon.FIRE_BREATHING;
		}
		else {
			state=Dragon.NORMAL;
		}
	}
	
	private void updateSurprised(float deltaTime){
		if (elapsedTime<surprisedDuration){
			elapsedTime += deltaTime;
		}
		else {
			resetState();
			setState(nextState, false);
		}
	}
	
	private void moveUpAndDown(float deltaTime){
		if (upAndDowPassedTime>=0.2f){
			if (movingState==MOVE_UP){
				this.position.add(0, UP_DOWN_MOVE_SPEED*deltaTime);
				tailAngle-=tailIncRate*deltaTime;
				wingAngle-=wingIncRate*deltaTime;
				if (position.y>=maxY) movingState=MOVE_DOWN;
			}
			if (movingState==MOVE_DOWN){
				this.position.add(0, -UP_DOWN_MOVE_SPEED*deltaTime);
				tailAngle+=tailIncRate*deltaTime;
				wingAngle+=wingIncRate*deltaTime;
				if (position.y<=minY) {
					movingState=MOVE_UP;
					upAndDowPassedTime=0;
					tailAngle=0;
					wingAngle=0;
				}
			}
		}
		else upAndDowPassedTime+=deltaTime;
	}
	
	private void updateHandWave(float deltaTime){
		if (elapsedTime<=handWaveDuration){
			elapsedTime+=deltaTime;
		}
		else {
			resetState();
		}
	}
	
	private void updateFireBreathState(float deltaTime){
		elapsedTime+=deltaTime;
		if (elapsedTime>TestScreen.totalFireBreathDuration){
			elapsedTime=0;
			resetState();
		}
	}
	
	private void setState(byte stateId, boolean overrideCurrentState){
		switch (stateId) {
		case CONFUSED:
			setConfusedState(overrideCurrentState);
			break;
		case FLEX:
			setFlexState(overrideCurrentState);
			break;
		case FIRE_BREATH:
			setFireBreathState(null, overrideCurrentState);
			break;
		}
	}
	
	public void resetState(){
		if (state == Dragon.HIT || state==Dragon.SHY){
			head.setNormalState();
			head.mouth.afteHit();
		}
		state=NORMAL;
		head.eyes.setNormalState();
		head.mouth.afterConfused();
		leftHand.restState();
		elapsedTime=0;
	}

	public void resetFire() {
		for (Particle p: Fire.particles){
			p.position.set(-2, -2);
			p.state = Particle.STATE_DEAD;
			p.dead = true;
		}
		
	}

	public void setRendererListener(IDragonRendererListener rendererListener){
		this.rendererListener = rendererListener;
	}
	
}
