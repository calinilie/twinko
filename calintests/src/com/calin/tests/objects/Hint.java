package com.calin.tests.objects;

import com.calin.tests.GameObject;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.OverlapTester;
import com.calin.tests.framework.math.Vector2;

public class Hint {
	
	boolean visible=false;
	
	public Vector2 position;
	public float MAX_SIZE;
	public float size;
	public OkButton button;
	
	float passedTime=0;
	
	
	private final float incRate=15;
	public int id;
	
	public Hint(float x, float y, float MAX_SIZE, int id){
		position=new Vector2(x ,y);
		this.MAX_SIZE=MAX_SIZE;
		this.id=id;
		size=0;
		button=new OkButton(x+MAX_SIZE/3, y-MAX_SIZE/3, MAX_SIZE/5, MAX_SIZE/5);
	}
	
	public void update(float deltaTime){
		if (visible){
			if (size<MAX_SIZE)
				size+=incRate*deltaTime;
			else {
				size=MAX_SIZE;
				passedTime+=deltaTime;
				if (passedTime >= 0.7f)
					button.enabled=true;
			}
		}
	}
	
	public boolean onTouchUp(Circle touch){
		return button.onTouchUp(touch);
	}
	
	public void show(float x, float y, float MAX_SIZE, int id){
		position.set(x, y);
		button.setPosBoundsSize(x, y, MAX_SIZE);
		this.MAX_SIZE=MAX_SIZE;
		this.id=id;
		visible=true;
	}
	
	public void reset(){
		button.enabled=false;
		passedTime=0;
		visible=false;
		size=0;
	}
	
	public void show(int id){
		this.id=id;
		this.visible=true;
	}
	
	public void show(){
		visible=true;
	}
	
	public class OkButton extends GameObject{

		public boolean enabled=false;
		public OkButton(float x, float y, float width, float height) {
			super(x, y, width, height);
		}
		
		public boolean onTouchUp(Circle touch){
			return OverlapTester.overlapCircleRectangle(touch, bounds) && enabled;
		}
		
		public void setPosBoundsSize(float x, float y, float size){
			this.bounds.height=size/5;
			this.bounds.width=size/5;
			this.position.set(x + size/3, y - size/3);
			this.bounds.lowerLeft.set(this.position.x - this.bounds.width/2, this.position.y - this.bounds.height/2);
		}
		
	}

}
