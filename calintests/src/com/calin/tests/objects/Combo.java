package com.calin.tests.objects;

import com.calin.tests.food.Food;

public class Combo extends PulseObject{
	
	public float x, y;
	
	private int counter=0;
	
	public final static byte NO_COMBO=1;
	public final static byte X2=2;
	public final static byte X3=3;
	public final static byte X4=4;
	public final static byte X5=5;
	public byte comboState=NO_COMBO;

	public Combo(float x, float y, float width, float height) {
		super(0, 0);
		this.x=x;
		this.y=y;
		pulseState=NORMAL;
		
		comboState=NO_COMBO;
		minHeight=height;
		maxHeight=minHeight+1;
		barWidthInc=width/2;
		barHeightInc=minHeight/2;
		SPEED=5f;
	}

	@Override
	public void setSizesAndRates() {		
	}
	
	public int onDeadtargetEvent(Target source, int score){
		int result=score;
		if (source instanceof Food){
			if (score>0){
				counter++;
				switch (counter){
				case 0:
				case 1:
				case 2:
					this.comboState=NO_COMBO;
					result=score;
					break;
				case 3:
					this.comboState=X2;
					result = score*2;
					this.pulse();
					break;
				case 4:
					this.comboState=X3;
					result = score *3;
					this.pulse();
					break;
				case 5:
					this.comboState=X4;
					result = score*4;
					this.pulse();
					break;
				case 6:
					this.comboState=X5;
					result= score*5;
					this.pulse();
					break;
				default:
					this.comboState=X5;
					result=score*5;
					break;
				}
			}
			else {
				counter=0;
				this.comboState=NO_COMBO;
			}
		}
		return result;
	}

}
