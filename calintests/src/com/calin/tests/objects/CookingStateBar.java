package com.calin.tests.objects;

import com.calin.tests.food.Food;

public abstract class CookingStateBar {
	public static final float WIDTH = 13;
	public static final float HEIGHT = 1;
	public static final float posX = 8f;
	public static final float posY = 8f;
	public static float red = 0;
	public static float green = 0;
	public static float blue = 1;
	public static float curentWidth=0;
	
	static byte foodState;
	static float maxTargetLife;
	static float curentTargetLife;
	static float ratio;
	
	public static void update(Food target){
		foodState=target.cookingState;
		maxTargetLife=target.maxHP;
		curentTargetLife=target.curHP;
		switch (foodState){
		case (Food.RAW_STATE):
			blue=1;
			red=0;
			green=0;
			break;
		case (Food.COOKED_STATE):
			blue=0;
			green=1;
			red=0;
			break;
		case (Food.BURNED_STATE):
			blue=0;
			green=0;
			red=1;
			break;
		}
		ratio=WIDTH/maxTargetLife;
		curentWidth = (curentTargetLife > 0) ? ratio*(maxTargetLife-curentTargetLife) : WIDTH;
	}
	
	public static void reset(){
		curentWidth=0;
		red=0;
		green=0;
		blue=0;
	}

}
