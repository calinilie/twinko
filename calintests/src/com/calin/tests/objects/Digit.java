package com.calin.tests.objects;

import com.calin.tests.GameObject;

public class Digit extends GameObject{
	
	public int value;
	public boolean display;
	
	public Digit(float x, float y, float width, float height) {
		super(x, y, width, height);
		value=0;
		display=false;
	}

}
