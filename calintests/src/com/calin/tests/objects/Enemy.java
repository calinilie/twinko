package com.calin.tests.objects;

import com.calin.tests.IDragonHitListener;
import com.calin.tests.IDragonRendererListener;

public abstract class Enemy extends Target {
	
	private int releaseAfter;
	public float timePassed;
	public float damage; 
	
	IDragonHitListener[] listeners;

	public Enemy(float x, float y, float width, float height, float maxHP, float speed, int releaseAfter, int points, float damage) {
		super(x, y, width, height, maxHP, speed, points);
		this.releaseAfter=releaseAfter;
		this.damage=damage;
	}

	@Override
	public void revive(){
		super.revive();
		this.timePassed=0;
	}
	
	public int getReleaseAfter() {
		return releaseAfter;
	}

	public void setReleaseAfter(int releaseAfter) {
		this.releaseAfter = releaseAfter;
	}
	
	@Override
	public void takeDamage(float dmg){
		super.takeDamage(dmg);
		if (this.curHP <= 0 && state==ALIVE) {
			this.kill(points);
		}
	}
	
	public void subscribeDragonHitListeners(IDragonHitListener...dragonListeners){
		this.listeners=dragonListeners;
	}
	
	public String toString(){
		return "passed "+timePassed+" releaseAfter "+releaseAfter;
	}
}
