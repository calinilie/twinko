package com.calin.tests.objects;

import com.calin.test.levels.StoreDisplayModel;

public class Item extends MenuButton{
	
	
	public final int price;
	
	public Item(float x, float y, float width, float height, int price){
		super(x, y, width, height);
		this.price = price;
	}
	
	public Item(int price){
		super(-100, -100, StoreDisplayModel.objectSize, StoreDisplayModel.objectSize);
		this.price=price;
	}

	public int getPrice() {
		return price;
	}
	
}
