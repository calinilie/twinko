package com.calin.tests.objects;

public class DragonStats implements DamagePotionOverListener{
	
	public float maxHP;
	public float hp;
	public float maxStamina;
	public float staminaDropRate;
	public float stamina;
	public float damage;
	public float XP;
	public float level;
	public int unusedSkillPoints;
	public boolean buffedDamage = false;
	
	public DragonStats(float MAX_HP, float hp, float MAX_STAMINA, float STAMINA_DROP_RATE, float stamina, float dmg, float XP, float level, int unusedSkillPoints){
		this.maxHP=MAX_HP;
		this.hp=hp;
		this.maxStamina=MAX_STAMINA;
		this.staminaDropRate=STAMINA_DROP_RATE;
		this.stamina=stamina;
		this.XP=XP;
		this.level=level;
		this.damage=dmg;
		this.unusedSkillPoints=unusedSkillPoints;
	}

	private void setStamina(float stamina) {
		this.stamina = stamina;
	}
	
	public void setSomeStats(float stamina, float HP){
		setStamina(stamina);
		setHp(HP);
		if (buffedDamage) this.decreaseDamageToNormal();
	}
	
	public void incStaminaBy(float value){
		stamina+=value;
		if (stamina>maxStamina) stamina=maxStamina;
	}
	
	public void incHPBy(float value){
		hp+=value;
		if (hp>maxHP) hp=maxHP;
	}
	
	/**
	 * 
	 * @param earnedXP xp erned after current level;
	 * If current xp + earned xp exceeds required xp then dragon level up + update remaining xp,
	 * else inc xp with current earned xp. 
	 * NOTE: this method also updates current XP!
	 * @return TRUE if levelUP FALSE otherwise
	 * 
	 */
	public boolean checkNewLevel(int earnedXP){
		int xpRequiredForNextLevel=(int)Math.pow(2, level)*100;
		if (XP+earnedXP>=xpRequiredForNextLevel){
			increaseLevel();
			XP=(XP+earnedXP)-xpRequiredForNextLevel;
			return true;
		}
		else {
			XP+=earnedXP;
		}
		return false;
	}
	
	private void increaseLevel(){
		this.stamina=this.maxStamina;
		this.hp=this.maxHP;
		this.unusedSkillPoints+=3;
		level++;
	}
	
	public boolean increaseMaxHp(){
		if (decreaseSkillPoints()){
			maxHP+=3;
			return true;
		}
		return false;
	}
	
	public boolean increaseMaxStamina(){
		if (decreaseSkillPoints()){
			maxStamina+=10;
			return true;
		}
		return false;
	}

	public boolean increaseDmg(){
		if (decreaseSkillPoints()) {
			damage+=0.001;
			staminaDropRate+=0.001;
			return true;
		}
		return false;
	}
	
	public void increaseDamageTemporarily(){
		if (!buffedDamage){
			damage=damage*DamagePotion.INCREASE_RATE;
			staminaDropRate*=DamagePotion.INCREASE_RATE;
			buffedDamage=true;
		}
	}
	
	@Override
	public String toString() {
		return "DragonStats [MAX_HP=" + maxHP + ", hp=" + hp
				+ ", MAX_STAMINA=" + maxStamina + ", STAMINA_DROP_RATE="
				+ staminaDropRate + ", stamina=" + stamina + ", dmg=" + damage
				+ ", XP=" + XP + ", level=" + level + ", unusedSkillPoints="
				+ unusedSkillPoints + "]";
	}

	private boolean decreaseSkillPoints(){
		if (unusedSkillPoints>0) {
			unusedSkillPoints--;
			return true;
		}
		return false;
	}

	@Override
	public void onDamagePotionOver() {
		this.decreaseDamageToNormal();
	}
	
	public float getHp() {
		return hp;
	}

	public void setHp(float hp) {
		this.hp = hp;
	}

	private void decreaseDamageToNormal(){
		this.damage=this.damage/DamagePotion.INCREASE_RATE;
		this.staminaDropRate=staminaDropRate/DamagePotion.INCREASE_RATE;
		buffedDamage=false;
	}
	
}
