package com.calin.tests.objects;


public class ScoreDisplay extends GameMenuItem{
	
	public static final int GROW_STATE=1;
	public static final int SHRINK_STATE=2;
	public static final int NORMAL_STATE=3;
	
	public Digit[] digits;
	public final float digitsize;
	public int state;
	
	public ScoreDisplay(float x, float y, float initialX, float initialY){
		super(x, y, initialX, initialY);
		state=ScoreDisplay.NORMAL_STATE;
		digitsize=1;
		digits=new Digit[4];
		for (int i=0; i<digits.length; i++)
			digits[i]=new Digit(initialX, initialY, 1, 1);
	}
	
	private void prepScore(int scoreInt){
		int score=scoreInt;
		int index=0;
		while (score!=0){
			int ld=score % 10;
			digits[index].value=ld;
			index++;
			score=score/10;
		}
	}
	
	public void update(int scoreint, float deltaTime){
		super.update(deltaTime);
		float xpos=super.position.x;
		for (Digit d: digits){
			d.position.y=super.position.y;
			d.position.x=xpos;
			xpos-=0.7f;
		}
		switch (state){
		case (ScoreDisplay.NORMAL_STATE):
			this.prepScore(scoreint);
			break;
//		case (ScoreDisplay.GROW_STATE):
//			digitsize+=0.1f;
//			if (digitsize>=1.6f) state=ScoreDisplay.SHRINK_STATE;
//			break;
//		case (ScoreDisplay.SHRINK_STATE):
//			digitsize-=0.1f;
//			if (digitsize<1) {
//				digitsize=1;
//				state=ScoreDisplay.NORMAL_STATE;
//			}
		}
	}
	
	public void animate(){
		state=ScoreDisplay.GROW_STATE;
	}

}
