package com.calin.tests.objects;

import android.test.IsolatedContext;

import com.calin.tests.framework.math.Circle;

public class SlideController extends GameMenuItem{
	
	public static final byte PULSE=1;
	public static final byte NORMAL=2;
	public byte state;
	
	private final byte grow=11;
	private final byte shrink=12;
	private byte pusleState=grow;

	public SlideBar bar;
	public SlideButton button;
	public int distance;
	
	private float barWidthInc;
	private float barHeightInc;
	private float buttonWidthInc;
	private float buttonHeightInc;
	private float maxHeight;
	private float minHeight;
	private final float SPEED=7;
//	
	public SlideController(float destX, float destY, float x, float y, float height, float width){
		super(destX, destY,x, y);
		bar=new SlideBar(x, y, width, height);
		button=new SlideButton(x, y, 2*width, 1);
		maxHeight=height+height*0.2f;
		minHeight=height;
		barWidthInc=width/3;
		barHeightInc=height/10;
		buttonHeightInc=0.1f;
		buttonWidthInc=button.bounds.width/10;
	}
	
	public void update(Circle fingerTouch, float deltaTime, boolean isTouched){
		super.update(deltaTime);
		moveButtonBar();
		if (isTouched){
			if (fingerTouch.center.y>=bar.minY && fingerTouch.center.y<=bar.maxY){
				setButtonBoundsAndPosition(fingerTouch.center.y);
				distance=(int) (button.position.y-bar.minY);
				return;
			}
			if (fingerTouch.center.y<bar.minY) {
				setButtonBoundsAndPosition(bar.minY);
				distance=0;
				return;
			}
			if (fingerTouch.center.y>bar.maxY){
				setButtonBoundsAndPosition(bar.maxY);
				distance=(int) (bar.maxY-bar.minY);
				return;
			}
		}
		else this.reset();
	}
	
	public void updatePulse(float deltaTime){
		if (state==PULSE){
			switch(pusleState){
			case grow:
				bar.bounds.height+=barHeightInc*deltaTime*SPEED;
				bar.bounds.width+=barWidthInc*deltaTime*SPEED;
				
				button.bounds.height+=buttonHeightInc*deltaTime*SPEED;
				button.bounds.width+=buttonWidthInc*deltaTime*SPEED;
				
//				button.position.y+=barHeightInc*deltaTime*SPEED;
				
				if (bar.bounds.height>=maxHeight) pusleState=shrink;
				break;
			case shrink:
				bar.bounds.height-=barHeightInc*deltaTime*SPEED;
				bar.bounds.width-=barWidthInc*deltaTime*SPEED;
				
				button.bounds.height-=buttonHeightInc*deltaTime*SPEED;
				button.bounds.width-=buttonWidthInc*deltaTime*SPEED;
				
//				button.position.y-=barHeightInc*deltaTime*SPEED;
				
				if (bar.bounds.height<=minHeight) pusleState=grow;
				break;
			}
		}
		else {
			bar.bounds.height=minHeight;
		}
	}
	
	private void moveButtonBar(){
		bar.position.x=super.position.x;
		button.position.x=super.position.x;
		
		bar.setBounds(super.position.x, bar.position.y, bar.bounds.width, bar.bounds.height);
		button.setBounds(super.position.x, button.position.y, button.bounds.width, button.bounds.height);
	}
	
	private void reset(){
		 setButtonBoundsAndPosition(bar.minY);
		 distance=0;
	}
	
	private void setButtonBoundsAndPosition(float y){
		button.position.y=y;
		button.bounds.lowerLeft.y=y-button.bounds.height/2;
	}
}
