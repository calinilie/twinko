package com.calin.tests.objects;

import android.util.Log;

import com.calin.tests.framework.math.OverlapTester;
import com.calin.tests.framework.math.Rectangle;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.framework.math.WorkerVector2;

public abstract class GameMenuItem{
	
	public byte movingState;
	public static final byte NORMAL=0;
	public static final byte MOVE=1;
	
	Vector2 v;
	public Vector2 position;
	public WorkerVector2 finalPosition;
	public Rectangle bounds;
	
	private float angle;
	public float SPEED=5;
	
	public GameMenuItem(float destX, float destY, float x, float y, float width, float height, float speed){
		this.SPEED=speed;
		init(destX, destY, x, y);
		bounds=new Rectangle(x-width/2, y-height/2, width, height);
	}
	
	
	public GameMenuItem(float destX, float destY, float x, float y){
		init(destX, destY, x, y);
		bounds=new Rectangle(x-0.5f, y-0.5f, 1, 1);
	}
	
	private void init(float destX, float destY, float x, float y){
		v=new Vector2();
		finalPosition=new WorkerVector2();
		finalPosition.set(destX, destY);
		position=new Vector2(x, y);
		
		this.movingState=NORMAL;
		
		angle=finalPosition.sub(position).angle()*Vector2.TO_RADIANS;
		v.x=(float) (SPEED*Math.cos(angle));
		v.y=(float) (SPEED*Math.sin(angle));
	}
	
	public void update(float deltaTime){
		switch (movingState){
		case NORMAL:
			break;
		case MOVE:
			updateMove(deltaTime);
			break;
		}
	}
	
	private void updateMove(float deltaTime){
		this.position.add(v.x*deltaTime, v.y*deltaTime);
		setBounds(position);
		if (OverlapTester.pointInRectangle(bounds, finalPosition)) movingState=NORMAL;
	}
	
	private void setBounds(Vector2 other){
		bounds.lowerLeft.set(other.x-bounds.width/2, other.y-bounds.height/2);
	}
	

}
