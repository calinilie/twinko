package com.calin.tests.objects;

import com.calin.tests.IUsePotionListener;

public class DragonBag{
	public static final byte HEALING_POTION=1;
	public static final byte STAMINA_POTION=2;
	public static final byte DAMAGE_POTION=3;
	private StaminaPotion staminaPotion;
	private HealingPotion healingPotion;
	private DamagePotion damagePotion;
	private Coins coins;
	
	
	public DragonBag(int spq, int hpq, int dpq, int gcq, int scq){
		this.staminaPotion=new StaminaPotion(5 ,spq);
		this.healingPotion=new HealingPotion(10, hpq);
		this.damagePotion=new DamagePotion(15 ,dpq);
		coins=new Coins(gcq, scq);
	}
	
	public boolean useHealingPotion(){
		return this.healingPotion.use();
	}
	
	public boolean useStaminaPotion(){
		return this.staminaPotion.use();
	}
	
	public boolean useDamagePotion(){
		return this.damagePotion.use();
	}
	
	public void subscribeListeners(IUsePotionListener...listeners){
		healingPotion.subscribeListeners(listeners);
		staminaPotion.subscribeListeners(listeners);
		damagePotion.subscribeListeners(listeners);
	}
	
	public void subscribeDamagePotionOverListeners(DamagePotionOverListener...listeners){
		this.damagePotion.subscribeDamagePotionOverListeners(listeners);
	}
	
	public void update(float deltaTime){
		damagePotion.updateActive(deltaTime);
	}
	
	public boolean tryBuy(int value){
		return coins.descrease(value);
	}
	
	public void incStaminaPotionQ(){
		this.staminaPotion.increaseQuantity(1);
	}
	
	public void incHealingPotionQ(){
		this.healingPotion.increaseQuantity(1);
	}
	
	public void incDamagePotionQ(){
		this.damagePotion.increaseQuantity(1);
	}
	
	public int getGoldCoins(){
		return coins.getGoldQty();
	}
	
	public int getStaminaPotionsQ(){
		return staminaPotion.getQuantity();
	}
	
	public int getHealingPotionQ(){
		return healingPotion.getQuantity();
	}
	
	public int getDamagePotionQ(){
		return damagePotion.getQuantity();
	}
	
	@Override
	public String toString() {
		return "DragonBag [staminaPotion=" + staminaPotion.getQuantity() + ", healingPotion="
				+ healingPotion.getQuantity() + ", damagePotion=" + damagePotion.getQuantity() + ", coins="
				+ coins.toString() + "]";
	}
	
	public StaminaPotion getStaminaPotion() {
		return staminaPotion;
	}

	public HealingPotion getHealingPotion() {
		return healingPotion;
	}

	public DamagePotion getDamagePotion() {
		return damagePotion;
	}
	
	public void increaseGoldCoins(int value){
		coins.increaseBy(value);
	}
}
