package com.calin.tests.objects;

import com.calin.test.levels.StoreDisplayModel;
import com.calin.tests.DynamicGameObject;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.OverlapTester;

public class MenuButton extends DynamicGameObject{

	public static final byte PRESSED=1;
	public static final byte PRESSED_SHRINK=10;
	public static final byte PRESSED_GROW=11;
//	public static final byte PROCEED=5;
//	public static final byte MOVE_OUT=2;
//	public static final byte MOVE_IN=3;
	public static final byte NORMAL=4;
	public static final byte INVISIBLE=5;
	public static final byte POP=6;
	public static final byte MOVE_UP=51;
	public static final byte MOVE_DOWN=52;
	public static final byte MOVE_NORMAL=53;
	public static final byte MOVE = 50;
	
	/**
	 * max predefined state in MenuButton is 53, recommended to define states > 60 in subclasses 
	 */
	public byte state=NORMAL;
	public byte pressedState=PRESSED_SHRINK;
	public byte movingState=MOVE_NORMAL;

	boolean trying=false;
	float initialY;
	
	private float targetY=0;
	final float SPEED=20;
	
//	public float height;
//	public float width;
	public float minWidth;
	public float maxWidth;
	public float widthIncRate;
	public float heightIncRate;
	
	public MenuButton(float x, float y, float size, boolean initCollapsed){
		super(x, y, initCollapsed ? 0 : size, initCollapsed ? 0 : size);
		init(size);
		state = INVISIBLE;
	}
	
	public MenuButton(float x, float y, float size){
		super(x,y,size,size);
		init(size);
		state = NORMAL;
	}
	
	public MenuButton(float x, float y, float width, float height) {
		super(x, y, width, height);
		init(width);
		state = NORMAL;
	}
	
	public boolean onTouchDown(Circle finger){
		if (OverlapTester.overlapCircleRectangle(finger, this.bounds)){
			state=PRESSED;
			pressedState=PRESSED_SHRINK;
			return true;
		}
		return false;
	}
	
	public boolean update(float deltaTime){
		switch(state){
		case PRESSED:
			return updatePressed(deltaTime);
//		case MOVE_IN:
//			break;
		case NORMAL:
			break;
		case POP:
			updatePop(deltaTime);
			break;
		case MOVE:
			updateMove(deltaTime);
			break;
		}
		return false;
	}

	public void setPositionAndBounds(float x, float y, float width, float height){
		super.position.set(x, y);
		super.setBounds(x, y, width, height);
	}
	
	//external state changes
	public void pop(){
		state=POP;
	}
	
	public void collapse(){
		state=INVISIBLE;
		bounds.width=0;
		bounds.height=0;
	}
	
	public void hide(){
		state = INVISIBLE;
	}
	
	public void move(int factor){
		if (movingState==MOVE_NORMAL){
			targetY=position.y+(StoreDisplayModel.unit)*factor;
			state = MOVE;
			movingState=(factor>0) ? MOVE_UP :MOVE_DOWN;
			if (movingState==MOVE_UP){
				this.velocity.y=SPEED;
				this.accel.y=-SPEED*4.5f;
			}
			else {
				this.velocity.y=-SPEED;
				this.accel.y=SPEED*4.5f;
			}
		}
	}
	
	public void moveDistance(float dist){
		if (movingState==MOVE_NORMAL){
			targetY=position.y+dist;
			state = MOVE;
			movingState=(targetY>position.y) ? MOVE_UP :MOVE_DOWN;
			if (movingState==MOVE_UP){
				this.velocity.y=SPEED*2;
				this.accel.y=-SPEED*2*4.5f;
			}
			else {
				this.velocity.y=-SPEED*2;
				this.accel.y=SPEED*4.5f*2;
			}
		}
	}
	
	public void tryMove(int factor){
		if (movingState==MOVE_NORMAL){
			targetY=position.y+(StoreDisplayModel.unit)*factor;
			state = MOVE;
			movingState=(factor>0) ? MOVE_UP :MOVE_DOWN;
			trying=true;
			if (movingState==MOVE_UP){
				initialY=position.y+0.1f;
				this.velocity.y=SPEED;
				this.accel.y=-SPEED*6.5f;
			}
			else {
				initialY=position.y-0.1f;
				this.velocity.y=-SPEED;
				this.accel.y=SPEED*6.5f;
			}
		}
	}
	
	/**
	 * incRate is by default 2*size (only square buttons accepted so far)
	 * use this method to override the incRate
	 * @param rate
	 */
	public void setIncRate(float rate){
		heightIncRate = maxWidth * rate;
		widthIncRate = maxWidth * rate;
	}	
	
	//update methods
	private void updateMove(float deltaTime){
		switch (movingState) {
		case MOVE_UP:
			updateMovingUp(deltaTime);
			break;
		case MOVE_DOWN:
			updateMovingDown(deltaTime);
			break;
		}
	}
	
	private void updateMovingDown(float deltaTime){
		if (position.y<=targetY+1.9f){
			this.velocity.add(accel.x*deltaTime, accel.y*deltaTime);
		}
		this.position.add(velocity.x*deltaTime, velocity.y*deltaTime);
		if (trying){
			if (position.y>=initialY){
				position.y=initialY+0.1f;
				movingState=MOVE_NORMAL;
				state = NORMAL;
				trying=false;
			}
		}else{
			if (position.y<=targetY) {
				position.y=targetY;
				movingState=MOVE_NORMAL;
				state = NORMAL;
			}
		}
		setBounds(position.x, position.y, this.bounds.width, this.bounds.height);
	}
	
	private void updateMovingUp(float deltaTime){
		if (position.y>=targetY-1.9f){
			this.velocity.add(accel.x*deltaTime, accel.y*deltaTime);
		}
		this.position.add(velocity.x*deltaTime, velocity.y*deltaTime);
		if (trying){
			if (position.y<=initialY){
				position.y=initialY-0.1f;
				movingState=MOVE_NORMAL;
				state = NORMAL;
				trying=false;
			}
		}
		if (position.y>=targetY) {
			position.y=targetY;
			movingState=MOVE_NORMAL;
			state = NORMAL;
		}
		setBounds(position.x, position.y, this.bounds.width, this.bounds.height);
	}
	
	private boolean updatePressed(float deltaTime){
		if (pressedState==PRESSED_SHRINK){
			bounds.width-=deltaTime*widthIncRate;
			bounds.height-=deltaTime*heightIncRate;
			if (bounds.width<minWidth) pressedState=PRESSED_GROW;
		}
		else {
			bounds.width+=deltaTime*widthIncRate;
			bounds.height+=deltaTime*heightIncRate;
			if (bounds.width>=maxWidth){
				state=NORMAL;
				bounds.width=maxWidth;
				bounds.height=maxWidth;
				return true;
			}
		}
		return false;
	}
	
	private void updatePop(float deltaTime){
		this.bounds.width+=widthIncRate*deltaTime*2.5f;
		this.bounds.height+=heightIncRate*deltaTime*2.5f;
		if (bounds.width >= maxWidth){
			bounds.width=maxWidth;
			bounds.height=maxWidth;
			state=NORMAL;
		}
	}
	
	//init used in constructors
	/**
	 * calculates minWidth and MaxWidth, heightIncRate and widthIncRate based on given size
	 * sets state to NORMAL
	 */
	private void init(float size){
		minWidth=0.85f*size;
		maxWidth=size;
		
		heightIncRate=size*2;
		widthIncRate=size*2;
	}


}
