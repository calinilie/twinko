package com.calin.tests.objects;

import com.calin.test.levels.StoreDisplayModel;

public class ItemPlayStore extends MenuButton {

	public int id;
	
	public ItemPlayStore(int id){
		super(-100, -100, StoreDisplayModel.objectSize, StoreDisplayModel.objectSize);
		this.id=id;
	}
}
