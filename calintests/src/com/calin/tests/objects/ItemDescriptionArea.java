package com.calin.tests.objects;

import com.calin.tests.framework.math.Vector2;

public class ItemDescriptionArea {
	
	public final Vector2 position;
	final float maxWidth;
	public float width;
	public final float height;
	
	final byte INVISIBLE=1;
	final byte GROW=2;
	final byte NORMAL=3;
	byte state=INVISIBLE;
	final float withIncRate=20;
	
	public ItemDescriptionArea(float x, float y, float width, float height){
		position=new Vector2(x, y);
		this.width=0;
		this.maxWidth=width;
		this.height=height;
	}
	
	public boolean update(float deltaTime){
		switch (state){
		case GROW:
			width+=deltaTime*withIncRate;
			if (width>=maxWidth){
				state=NORMAL;
				width=maxWidth;
				return true;
			}
			break;
		}
		return false;
	}
	
	public void grow(){
		this.width=0;
		this.state=GROW;
	}

}
