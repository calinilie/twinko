package com.calin.tests.objects;

import com.calin.tests.IUsePotionListener;

public abstract class Potion extends Item{
	
	private int quantity;
	private IUsePotionListener[] listeners;
	public NumbersDisplay quantityDisplay;
	
	public Potion(float x, float y, float width, float height, int price, int quantity){
		super(x, y, width, height, price);
		this.quantity = quantity;
		quantityDisplay = new NumbersDisplay(x, y, width/2);
	}
	
	public Potion(int price, int quantity){
		super(price);
		this.quantity=quantity;
	}
	
	@Override
	public boolean update(float deltaTime){
		super.update(deltaTime);
		quantityDisplay.setPosition(this.position.x, this.position.y, this.bounds.width, this.bounds.height);
		quantityDisplay.update(deltaTime, quantity);
		return false;
	}

	public void setPositionSizeAndQuantityInit(float x, float y, float width, float height){
		super.setPositionAndBounds(x, y, width, height);
		quantityDisplay = new NumbersDisplay(x, y, width/4);
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public void increaseQuantity(int value){
		quantity+=value;
	}
	
	public boolean use(){
		if ((quantity-1)>=0){
			quantity--;
			this.notifyListeners();
			return true;
		}
		return false;
	}
	
	public void subscribeListeners(IUsePotionListener ... listener){
		this.listeners=listener;
	}
	
	private void notifyListeners(){
		for (IUsePotionListener l: listeners)
			l.onPotionUsedEvent(this);
	}
}
