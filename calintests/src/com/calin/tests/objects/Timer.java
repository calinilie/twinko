package com.calin.tests.objects;

//import android.util.Log;

public class Timer extends GameMenuItem{
	
	public static final int NORMAL_STATE=1;
	public static final int JUMPING_STATE=2;
	public static final int FALLING_STATE=3;
	
	public int state;
	public Digit[] digits;
	
	float posY;
	
	
	public Timer(float x, float y, float initialX, float initialY){
		super(x, y, initialX, initialY);
		digits=new Digit[4];
		float posx=initialX;
		state=Timer.NORMAL_STATE;
		for (int i=0; i<digits.length; i++){
			digits[i]=new Digit(posx, initialY, 1, 1);
			digits[i].value=0;
			posx+=0.7f;
		}
	}
		
	public void update(int time, float deltaTime){
		super.update(deltaTime);
		digits[0].value=(int)((time/60)/10);
		digits[1].value=(int)((time/60)%10);
		digits[2].value=(int)((time%60)/10);
		digits[3].value=(int)((time%60)%10);
		for (Digit d: digits){
			d.position.y=super.position.y;
		}
//		switch (state){
//			case (Timer.NORMAL_STATE):
//			break;
//			case (Timer.JUMPING_STATE):
//				posY+=0.1f;
//				if (posY>2) state=Timer.FALLING_STATE;
//				break;
//			case(Timer.FALLING_STATE):
//				posY-=0.1f;
//				if (posY<1) state=Timer.JUMPING_STATE;
//				break;
//		}
	}
}
