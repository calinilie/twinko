package com.calin.tests.objects;

import android.util.Log;

public class NumbersDisplay extends MenuButton{
	
	public static final byte ANIMATING = 60;
	public static final byte INACTIVE = 61;
	public boolean dead=true;
	boolean startFade=false;
	
	public Digit[] digits;
	private float targetedNumber;//for some display effect
	private float currentNumber;
	private float incRate;
	
//	private boolean animate = false;
	
	public float alpha=1;
	public float size;
	float incSizerate;
	float xSpacing;
	
	public NumbersDisplay(float x, float y, float size){
		super(x,y,size,size);
		init(size);
	}
	
	public NumbersDisplay(float x, float y, float size, float targetNumber, float incRate){
		super(x, y, size, size);
		init(size);
//		this.animate = true;
		this.targetedNumber = targetNumber;
		this.incRate = incRate;
		state = INACTIVE;
	}
	
	private void init(float size){
		digits=new Digit[10];
		for (int i = 0; i < digits.length; i++) {
			digits[i]=new Digit(this.position.x, this.position.y, size, size);
		}
		this.targetedNumber=0;
		this.size=size;
		this.xSpacing = size / 1.42f;
	}
	
	public void update(float deltaTime, float number){
		prepNumbers(number, xSpacing);
	}
	
	public boolean update(float deltaTime){
		if (state == ANIMATING){
			double increaseRate=targetedNumber/incRate;
			if (currentNumber<=targetedNumber){
				this.prepNumbers(currentNumber, xSpacing);
				currentNumber+=increaseRate;
			}
			else state = NORMAL;
		}
		return false;
	}	
	
	public void updateGrowFade(float deltaTime){
		if (!dead){
			dead = alpha<=0;
			size+=incSizerate;
			if (size>0.5f) startFade=true;
			if (startFade) alpha-=0.05;
		}
	}
	
	public void setPosition(float x, float y, float width, float height){
		for (Digit d : digits) {
			d.position.set(x, y);
		}
		this.position.set(x, y);
	}
	
	public void animate(float x, float y, float number, float incSize, float xSpacing){
		alpha=1;
		size=0.5f;
		this.position.x=x;
		this.position.y=y;
		this.dead=false;
		this.startFade=false;
		incSizerate=incSize;
		this.prepNumbers(number, xSpacing);
	}
	
	public void setTargetedNumber(float targetedNumber){
		this.targetedNumber = targetedNumber;
	}
	
	public void show(){
		if (state == INACTIVE)
			state = ANIMATING;
	}
	
	public void tryShow(boolean show){
		if (show){
			if (state == INACTIVE)
				state = ANIMATING;
		}
	}
	
	public boolean isNormalState(){
		return state == MenuButton.NORMAL;
	}
	
	private void prepNumbers(float number, float x){
		int score=(int)(number);
//		Log.d("complete", "score: "+ score);
		int index=0;
		float xpos=this.position.x;
		if (score==0) {
			digits[index].value=0;
			digits[index].display=true;
			digits[index].position.x=this.position.x;
			index++;
		}
		else{
			while (score!=0){
				int ld=score % 10;
				digits[index].value=ld;
				digits[index].display=true;
				digits[index].position.x=xpos;
				xpos-=x;
				index++;
				score=score/10;
			}
		}
		
		while (index<digits.length){
			digits[index].display=false;
			index++;
		}
		
	}
	
	

}
