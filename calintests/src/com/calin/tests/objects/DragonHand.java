package com.calin.tests.objects;

public class DragonHand {
	public float angle=0;
	final static float angleIncRate=90;
	private final float MAX_ANGLE;
	
	private static final byte MOVE_UP = 1;
	private static final byte MOVE_DOWN = 2; 
	
	private byte state=MOVE_UP;
	
	public DragonHand(byte orientation){
		if (orientation==Dragon.WEST) MAX_ANGLE=-80;
		else MAX_ANGLE = 80;
	}
	
	public void update(float deltaTime){
		switch (state){
		case MOVE_UP:
			angle+=angleIncRate*deltaTime;
			if (angle>=MAX_ANGLE) state = MOVE_DOWN;
			break;
		case MOVE_DOWN:
			angle-=angleIncRate*deltaTime;
			if (angle<50) state = MOVE_UP;
			break;
		}
	}
	
	public void wave(){
		state = MOVE_UP;
		angle = 50;
	}
	
	public void restState(){
		angle=0;
	}

}
