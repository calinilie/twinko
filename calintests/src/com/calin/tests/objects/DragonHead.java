package com.calin.tests.objects;

import com.calin.tests.framework.math.Rectangle;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.particles.Particle;

public class DragonHead{
	
	static final byte NORMAL=1;
	static final byte HIT=2;
	byte state=NORMAL;
	
	final float angleIncRate=Dragon.orientation==Dragon.EAST ? 40 : -40;

	public Vector2 pos;
	public Rectangle bounds;
	public float angle;
	
	Vector2 worker=new Vector2();
	public DragonEyes eyes;
	public DragonMouth mouth;
	
	public DragonHead(Vector2 dragonPos, float width, float height){
		pos=new Vector2();
		pos.set(dragonPos.x-width/38, dragonPos.y+height*0.43f);
		bounds=new Rectangle(pos.x, pos.y, width, height);
		
		eyes=new DragonEyes(height);
		mouth=new DragonMouth(height);
	}
	
	public void update(float deltaTime, Vector2 targetPosition, boolean isTouched, Vector2 dragonPosition, float elapsedTime){
		setPosition(dragonPosition);
		eyes.update(deltaTime, angle, pos);
		mouth.update(deltaTime, angle, pos, isTouched);
		
		switch (state){
		case NORMAL:
			updateNormal(deltaTime, targetPosition, isTouched);
			break;
		case HIT:
			updateHit(deltaTime, elapsedTime);
			break;
		case Dragon.SHY:
			updateScared(deltaTime, elapsedTime);
			break;
			
		}
	}
	
	void updateHit(float deltaTime, float hitPassedTime){
		if (hitPassedTime<Dragon.hitDuration/2){
			angle-=deltaTime*angleIncRate;
		}
		else {
			angle+=deltaTime*angleIncRate;
		}
	}
	
	public void setNormalState(){
		state=NORMAL;
		angle=0;
	}
	
	void updateScared(float deltaTime, float elapsedTime){
		if (elapsedTime<Dragon.scaredDuration/5){
			angle-=deltaTime*(angleIncRate*2);
		}
	}
	
	void updateNormal(float deltaTime, Vector2 targetPosition, boolean isTouched){
		if (isTouched && targetPosition!=null){
			worker.set(targetPosition);
			if (Dragon.orientation==Dragon.EAST) angle=worker.sub(pos).angle()+10;
			else angle=worker.sub(pos).angle()-40;
			switch (Dragon.orientation){
			case Dragon.WEST:
				updateOrientationWest(deltaTime, targetPosition);
				break;
			case Dragon.EAST:
				updateOrientationEast(deltaTime, targetPosition);
				break;
			}
		}
		else {
			angle=0;
		}
	}
	
	void setPosition(Vector2 dragonPosition){
		pos.x=dragonPosition.x-bounds.width/38;		//40
		pos.y=dragonPosition.y+bounds.height*0.43f;	//0.45
	}
	
	void updateOrientationWest(float deltaTime, Vector2 targetPosition){
		float x,y;
		angle += 180;
		x = (float) (this.pos.x + (bounds.width/1.9)*Math.cos((angle+36)*Vector2.TO_RADIANS));
		y = (float) (this.pos.y - (bounds.height/1.9)*Math.sin((angle+36)*Vector2.TO_RADIANS));
		setParticlePos(x, y);
	}
	
	void updateOrientationEast(float deltaTime, Vector2 targetPosition){
		float x, y;
		angle += 25;
		x = (float) (this.pos.x + (bounds.width/1.9)*Math.cos((angle-36)*Vector2.TO_RADIANS));
		y = (float) (this.pos.y + (bounds.height/1.9)*Math.sin((angle-36)*Vector2.TO_RADIANS));
		setParticlePos(x, y);
	}
	
	void setParticlePos(float x, float y){
		Particle.POS_X=x;
		Particle.POS_Y=y;
	}

	public void onDragonHit() {
		state=HIT;
		angle=0;
	}
	
	public void scared(){
		state=Dragon.SHY;
	}
	
}
