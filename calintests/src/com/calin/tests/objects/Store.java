package com.calin.tests.objects;

public class Store {
	
	private static Store instance;
	public StaminaPotion staminaPotion;
	public HealingPotion healingPotion;
	public DamagePotion damagePotion;
	public ItemPlayStore smallGold;
	public ItemPlayStore mediumGold;
	public ItemPlayStore largeGold;
	
	public static Store getInstance(){
		if (instance==null){
			instance=new Store(0, 0, 0);
			return instance;
		}
		return instance;
	}
	
	private Store(int spp, int hpp, int dpp){
		staminaPotion=new StaminaPotion(10, 0);
		healingPotion=new HealingPotion(20, 0);
		damagePotion=new DamagePotion(10, 0);
		smallGold=new ItemPlayStore(-5);
		mediumGold=new ItemPlayStore(-6);
		largeGold=new ItemPlayStore(-7);
	}

}
