package com.calin.tests.objects;

import android.util.Log;

public class DamagePotion extends Potion{

	public static final byte INCREASE_RATE=2;
	
	private DamagePotionOverListener[] listeners;
	private final float MAX_AGE=10;
	private float age;
	private byte state;
	private final byte ACTIVE_STATE=1;
	private final byte INACTIVE_STATE=0;
	
	public DamagePotion(int price, int quantity) {
		super(price, quantity);
		age=0;
		state=INACTIVE_STATE;
	}
	
	public void subscribeDamagePotionOverListeners(DamagePotionOverListener...listeners){
		this.listeners=listeners;
	}
	
	public void updateActive(float deltaTime){
		if (state==ACTIVE_STATE){
			age+=deltaTime;
			if (age>=MAX_AGE){ 
				notifiyListeners();
				age=0;
				state=INACTIVE_STATE;
				Log.d("stats","damage potion effect over");
			}
		}
	}
	
	private void notifiyListeners(){
		for (DamagePotionOverListener l: listeners){
			l.onDamagePotionOver();
		}
	}
	
	@Override
	public boolean use(){
		if (super.use()){
			state=ACTIVE_STATE;
			return true;
		}
		return false;
	}

}
