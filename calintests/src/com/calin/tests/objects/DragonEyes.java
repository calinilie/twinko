package com.calin.tests.objects;

import android.util.Log;

import com.calin.tests.framework.math.Vector2;

public class DragonEyes{
	
	public static final byte NORMAL=1;
	public static final byte FIRE_BREATHING=2;
	public static final byte SAD=3;
	public static final byte CONFUSED=4;
	public static final byte NORMAL_CLOSED=5;
	public static final byte ANGRY=6;
	public static final byte SHY=7;
	public static final byte SURPRISED = 8;
	public byte state;
	
	public Vector2 pos;
	public float width;
	public float height;
	
	public boolean oneEyeOpen=false;
	public byte eye=0;
	final float duration=0.4f;
	float oneEyeOpenedTimePassed=0;
	float timePassed=0;
	
	public DragonEyes(float size){
		pos=new Vector2();
		this.width=(Dragon.orientation==Dragon.WEST) ? -size/2 : size/2;
		this.height=size/2;
	}
	
	public void update(float deltaTime, float headAngle, Vector2 headPos){
		switch (Dragon.orientation){
		case (Dragon.WEST):
			updateOrientationWest(deltaTime, headAngle, headPos);
			break;
		case (Dragon.EAST):
			updateOrientationEast(deltaTime, headAngle, headPos);
			break;
		}
		
		if (state==DragonEyes.SHY){
			updateScared(deltaTime);
		}
	}
	
	void updateScared(float deltaTime){
		timePassed+=deltaTime;
		if (timePassed>0.6f && !oneEyeOpen){
			oneEyeOpen=true;
			Log.d("eyes", "one eye should be opened");
		}
		if (oneEyeOpen){
			oneEyeOpenedTimePassed+=deltaTime;
			if (oneEyeOpenedTimePassed>0.5f) {
				eye++;
				oneEyeOpenedTimePassed=0;
			}
			if (timePassed>1.6f) {
				timePassed=-10;
				oneEyeOpen=false;
			}
		}
	}
	
	void updateOrientationWest(float deltaTime, float headAngle, Vector2 headPos){
		float angle=headAngle+180+17;
		pos.x = (float) (headPos.x - (width/2.875)*Math.cos((angle)*Vector2.TO_RADIANS));
		pos.y = (float) (headPos.y + (height/2.875)*Math.sin((angle)*Vector2.TO_RADIANS));
	}
	
	void updateOrientationEast(float deltaTime, float headAngle, Vector2 headPos){
		pos.x = (float) (headPos.x + (width/2.875)*Math.cos((headAngle-17)*Vector2.TO_RADIANS));
		pos.y = (float) (headPos.y + (height/2.875)*Math.sin((headAngle-17)*Vector2.TO_RADIANS));
	}

	public void onDragonHit() {
		state=SAD;
	}
	
	public void setNormalState(){
		state=NORMAL;
	}
	
	public void confused(){
		state=CONFUSED;
	}
	
	public void angry(){
		state=ANGRY;
	}
	
	public void shy(){
		state=SHY;
		oneEyeOpen=false;
		eye=0;
		timePassed=0;
		oneEyeOpenedTimePassed=0;
	}
	
	public void surprised(){
		state = SURPRISED;
	}
}
