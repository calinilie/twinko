package com.calin.tests.objects;

import com.calin.tests.GameObject;

public class SlideBar extends GameObject{

	public final float minY;
	public final float maxY;
	
	public SlideBar(float x, float y, float width, float height) {
		super(x, y, width, height);
		minY=y-height/2;
		maxY=y+height/2;
		// TODO Auto-generated constructor stub
	}

}
