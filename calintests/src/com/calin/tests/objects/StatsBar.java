package com.calin.tests.objects;

public class StatsBar extends PulseObject{
	public static float NORMAL_WIDTH=0.3f;
	public static float NORMAL_HEIGHT=5;
	public final float x;
	public final float y;
	public float currentHeight=0;
	float temp=0;
	float increaseRate=0;
	private float ratio;
	
	public StatsBar(float x, float y, float maxStat){
		super(NORMAL_WIDTH, 5);
		this.x=x;
		this.y=y;
		ratio=NORMAL_HEIGHT/maxStat;
	}
	
	public void update(float currentStat){
		if (increaseRate>0){
			if(temp<currentStat){
				temp+=increaseRate;
			}
			else{
				increaseRate=0;
				temp=currentStat;
			}
		}
		else temp=currentStat;
		currentHeight = (ratio*temp<=NORMAL_HEIGHT) ? ratio*temp : NORMAL_HEIGHT;
	}
	
	public void incBy(float value){
		increaseRate=value/100;
	}

	@Override
	public void setSizesAndRates() {
		maxHeight=NORMAL_HEIGHT+NORMAL_HEIGHT*0.1f;
		minHeight=NORMAL_HEIGHT;
		barWidthInc=NORMAL_WIDTH/3;
		barHeightInc=NORMAL_HEIGHT/10;
		SPEED=3.5f;
	}

}
