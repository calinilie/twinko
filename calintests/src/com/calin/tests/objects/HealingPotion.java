package com.calin.tests.objects;

public class HealingPotion extends Potion {
	
	public static final int plusHealing=25;
	
	public HealingPotion(int price, int quantity){
		super(price, quantity);
	}
}
