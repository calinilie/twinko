package com.calin.tests;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import android.util.Log;

import com.calin.test.levels.CompleteDialog;
import com.calin.test.levels.PausedDialog;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.ButtonsAsstets;
import com.calin.tests.assets.FoodAssets;
import com.calin.tests.assets.TwinkoAssets;
import com.calin.tests.food.Cherry;
import com.calin.tests.food.Food;
import com.calin.tests.food.Grape;
import com.calin.tests.food.Lemon;
import com.calin.tests.food.Orange;
import com.calin.tests.food.Peach;
import com.calin.tests.food.Plum;
import com.calin.tests.food.Stake;
import com.calin.tests.food.Strawberry;
import com.calin.tests.framework.Game;
import com.calin.tests.framework.Input;
import com.calin.tests.framework.Input.KeyEvent;
import com.calin.tests.framework.Input.TouchEvent;
import com.calin.tests.framework.gl.Camera2D;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.impl.GLGame;
import com.calin.tests.framework.impl.GLGraphics;
import com.calin.tests.framework.impl.GLScreen;
import com.calin.tests.framework.math.Circle;
import com.calin.tests.framework.math.OverlapTester;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.Bee;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.DragonBag;
import com.calin.tests.objects.DragonStats;
import com.calin.tests.objects.Enemy;
import com.calin.tests.objects.MenuButton;
import com.calin.tests.particles.Fire;
import com.calin.tests.renderers.CompleteDialogRenderer;
import com.calin.tests.renderers.PauseDialogRenderer;
import com.calin.tests.renderers.TwinkoRenderer;

public class TestScreen extends GLScreen implements IPauseListener{

	final float FRUSTRUM_WIDTH = 16f;
	final float FRUNSTRUM_HEIGHT=9f;
	
	GLGraphics graphics;
	Input input;
	Camera2D camera;
	SpriteBatcher batcher; 
	Vector2 touch;

	Circle fingerTouch;
	Fire fire;
	DynamicGameObject finger;
	
	MenuButton reset, buy, ok, back;
	
	
//	BatRenderer batRenderer;
	float time=0;
	boolean set=false;
	boolean isTouched=false;
	
	boolean isFireBreathSate = false;
	float elapsedTime = 0;
	public static final float totalFireBreathDuration = 0.7f;
	DynamicGameObject fireBreathTarget;
	
	Dragon twinkoWest;
	TwinkoRenderer twinkoWestRenderer;
	
	CompleteDialog completeDialog;
	PausedDialog pausedDialog;
	
	private float timeTillNextRandomAnimation = 5;
	private float timeSinceLastRandomAnimation = 0;
	
	public TestScreen(Game game) {
		super(game);
		graphics=((GLGame)game).getGLGraphics();
		input=game.getInput();
		camera=new Camera2D(graphics, FRUSTRUM_WIDTH, FRUNSTRUM_HEIGHT);
		batcher=new SpriteBatcher(graphics, 250);
//		fireBatcher=new SpriteBatcher(graphics, 200, true);
		touch=new Vector2(0,0);
		fingerTouch=new Circle(-10, -10, 0.5f);
		finger=new DynamicGameObject(0, 0, 1, 1);
//		batRenderer=new BatRenderer(null);
//		camera.zoom=0.25f;
//		camera.position.set(3.5f, 2.5f);
		
		fire=new Fire();
		fire.setIntensity(3);

		twinkoWest=GameObjectsController.getInstance(game).getDragon();
		twinkoWest.setBoundsSizeOrientationAndPosition(3, 2.5f, 4, 4, Dragon.EAST);
		twinkoWestRenderer=new TwinkoRenderer(twinkoWest, batcher);

		reset=new MenuButton(14, 1.25f, 2, 2);
		ok=new MenuButton(14, 3.5f, 2, 2);
		buy=new MenuButton(14, 5.75f, 2, 2);
		back=new MenuButton(14, 8, 2, 2);
		
		fireBreathTarget = new DynamicGameObject(0,0,1,1);
		
		twinkoWest.head.mouth.registerMouthRendererListeners(twinkoWestRenderer);
		
		completeDialog = new CompleteDialog(8, 4.5f, 7, 7);
		completeDialog.setIncRate(1);
		pausedDialog = new PausedDialog(8, 4.5f, 7, 7, -10);
		pausedDialog.setPausedListener(this);
	}

	@Override
	public void update(float deltaTime) {
		List<TouchEvent> touchEvents=input.getTouchEvents();
		List<KeyEvent> keyEvents = input.getKeyEvents();
			
		int len=keyEvents.size();
		for (int i = 0; i < len; i++) {
			KeyEvent event=keyEvents.get(i);
			if (event.keyCode==android.view.KeyEvent.KEYCODE_BACK && event.type==KeyEvent.KEY_UP){
				((GLGame)game).finish();
			}
		}
		
		for (TouchEvent t:touchEvents){
			touch.set(t.x, t.y);
			camera.touchToWorld(touch);
			fingerTouch.center.set(touch);
			if (t.type==TouchEvent.TOUCH_UP){		
//				if (fingerTouch.center.x>10 && fingerTouch.center.x<13 && fingerTouch.center.y>0 && fingerTouch.center.y<2.25f){
//					if (twinkoWest.setFireBreathState(fingerTouch.center, false));//temp
//						twinkoWest.onFireBreath(fingerTouch.center, false);
//					return;
//				}
			}
			if (t.type==TouchEvent.TOUCH_DOWN){
				ok.onTouchDown(fingerTouch);
				buy.onTouchDown(fingerTouch);
				reset.onTouchDown(fingerTouch);
				back.onTouchDown(fingerTouch);
				this.onTwinkoTouch(fingerTouch);
				pausedDialog.onTouchDown(fingerTouch);
			}
		}
		
		touch.set(input.getTouchX(0), input.getTouchY(0));
		camera.touchToWorld(touch);
		finger.position.set(touch);
		isTouched = finger.position.x<13 && finger.position.x>10 && input.isTouchDown(0);
		
		
		
		if (ok.update(deltaTime)){
//			twinkoWest.setFlexState(false);
			completeDialog.pop();
			completeDialog.setFinalData(true, true, false, true, 100, 100, 50);
		}
		if (buy.update(deltaTime)){
//			twinkoWest.setConfusedState(false);
			pausedDialog.show();
		}
		if (reset.update(deltaTime)){
			twinkoWest.setShyState(false);
		}
		if (back.update(deltaTime)){
			twinkoWest.onDragonHitEvent(null, 0);
			twinkoWestRenderer.onDragonHitEvent(null, 0);
			TwinkoAssets.playSound(TwinkoAssets.ouchSound);
		}
		
		listenForRandomAnimation(deltaTime);
		
		//COPY
		if (isFireBreathSate)
		{
			updateFireBreathState(deltaTime);
			twinkoWest.update(fireBreathTarget, true, 20, deltaTime);
			fire.update(deltaTime, fireBreathTarget, true, true, twinkoWest.head.mouth.fireOK);			
		}
		else{
			twinkoWest.update(finger, isTouched, 20, deltaTime);
			fire.update(deltaTime, finger, isTouched, true, twinkoWest.head.mouth.fireOK);
		}
		time+=deltaTime;
		
		completeDialog.update(deltaTime);
		pausedDialog.update(deltaTime);
	}

	@Override
	public void present(float deltaTime) {
		GL10 gl = graphics.getGL();
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT); 
		camera.setViewportAndMatrices();
		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glDisable(GL10.GL_DEPTH_TEST);
		gl.glEnable(GL10.GL_BLEND);
		
		gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);

		batcher.beginBatch(Assets1.medowBackgroundAtlas);
		batcher.drawSprite(8, 4.5f, 20.48f, 10.24f, Assets1.medowBackground, SpriteBatcher.NORMAL);
		batcher.endBatch();

		twinkoWestRenderer.render(deltaTime, isTouched);
		
		batcher.beginBatch(ButtonsAsstets.atlas);
		batcher.drawSprite(ok.position.x, ok.position.y, ok.bounds.width, ok.bounds.height, ButtonsAsstets.ok, SpriteBatcher.NORMAL);
		batcher.drawSprite(reset.position.x, reset.position.y, reset.bounds.width, reset.bounds.height, ButtonsAsstets.replay, SpriteBatcher.NORMAL);
		batcher.drawSprite(back.position.x, back.position.y, back.bounds.width, back.bounds.height, ButtonsAsstets.back, SpriteBatcher.NORMAL);
		batcher.drawSprite(buy.position.x, buy.position.y, buy.bounds.width, buy.bounds.height, ButtonsAsstets.buy, SpriteBatcher.NORMAL);
		batcher.endBatch();
		
		if (Fire.dead==false){
			gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE);
			batcher.beginBatch(Assets1.particlesAtlas);
			for (int i = 0; i < Fire.particleNumber; i++) {
				batcher.drawSprite(Fire.particles[i].position.x, Fire.particles[i].position.y, Fire.particles[i].textureSize, Fire.particles[i].textureSize, Assets1.particleRegion, SpriteBatcher.NORMAL);
			}
			batcher.endBatch();
			gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
		}	
		
		batcher.beginBatch(Assets1.digitsAtlas);
		CompleteDialogRenderer.renderCompleteDialog(completeDialog, batcher);
		batcher.endBatch();
		
		PauseDialogRenderer.render(pausedDialog, batcher);	
	}
	
	private void updateFireBreathState(float deltaTime){
		elapsedTime+=deltaTime;
		if (elapsedTime>TestScreen.totalFireBreathDuration){
			elapsedTime=0;
			isFireBreathSate = false;
			twinkoWest.resetState();
		}
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {		
	}
	
	
	private void listenForRandomAnimation(float deltaTime){
		timeSinceLastRandomAnimation+=deltaTime;
		if (timeSinceLastRandomAnimation>=timeTillNextRandomAnimation){
			notifyListeners(Dragon.randomAnimation());
			timeSinceLastRandomAnimation = 0;
			timeTillNextRandomAnimation = (float) (3 + Math.random() * 5); 
		}
	}
	
	private void onTwinkoTouch(Circle finger){
		if (twinkoWest.onTouch(finger)){
			notifyListeners(Dragon.randomAnimation());
			timeSinceLastRandomAnimation = 0;
			timeTillNextRandomAnimation = (float) (3 + Math.random() * 5);
		}
	}
	
	private void notifyListeners(byte animationId){
		switch(animationId){
		case Dragon.HAND_WAVE:
			twinkoWest.setHandWaveState((float) (1.5f+Math.random()*2), false);
			break;
		case Dragon.CONFUSED:
			twinkoWest.setConfusedState(false);
			break;
		case Dragon.FLEX:
			twinkoWest.setFlexState(false);
			break;
		case Dragon.SHY:
			twinkoWest.setShyState(false);
			break;
		case Dragon.FIRE_BREATH:
			twinkoWest.setFireBreathState(null, false);
			break;
		}
	}

	@Override
	public void onContinuePressed() {
		Log.d(">>>>>>>>>>>>>>>>>>>>", "gameResumed");
	}

	@Override
	public void onReplayPressed(int levelId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onShopPressed() {
		// TODO Auto-generated method stub
		
	}

}
