package com.calin.tests.renderers;

import com.calin.test.levels.Level;
import com.calin.test.levels.Level05;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.BeeAssets;
import com.calin.tests.assets.FoodAssets;
import com.calin.tests.food.Cherry;
import com.calin.tests.food.Strawberry;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.impl.GLGraphics;
import com.calin.tests.objects.Bee;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.Hint;
import com.calin.tests.objects.Target;

public class Level05Renderer extends LevelRenderer{

	
	BeeRenderer beeRenderer;
	
	public Level05Renderer(Level level, GLGraphics graphics,
			SpriteBatcher batcher, SpriteBatcher fireBatcher) {
		super(level, graphics, batcher, fireBatcher);
		beeRenderer=new BeeRenderer(((Level05)level).bee);
		// TODO Auto-generated constructor stub
	}
	
//	@Override
//	protected void renderDragon(float deltaTime){
//		Dragon dragon = level.dragon;
//		batcher.beginBatch(Assets1.dragonAtlas);
//		float x=dragon.position.x;
//		float y=dragon.position.y;
//		float width=dragon.width;
//		float height=dragon.height;
//		float factor=1.6f;
//		batcher.drawSprite(x-(width*0.08333f), y-(width*0.08333f), -width/factor, height/factor, Assets1.dragonBody, SpriteBatcher.NORMAL);
//		batcher.drawSprite(x, y, -width , height, dragon.angle, Assets1.dragon);
//		batcher.endBatch();
//	}

	@Override
	protected void renderFood() {
		Cherry cherry=((Level05)level).cherry;
		Strawberry strawberry=((Level05)level).strawberry;
		
		batcher.beginBatch(FoodAssets.foodAtlas);
		if (level.currentTarget!=cherry){
			FoodRenderer.renderCherry(cherry, batcher);
		}
		
		if (level.currentTarget!=strawberry){
			FoodRenderer.renderStrawberry(strawberry, batcher);
		}
		batcher.endBatch();
	}

	@Override
	protected void renderEnemies() {
		Bee bee=((Level05)level).bee;
		if (bee!=level.currentTarget && bee.state==Target.ALIVE){
			batcher.beginBatch(BeeAssets.beeAtlas);
			beeRenderer.render(batcher);
			batcher.endBatch();
		}
	}

	@Override
	void renderTutorial() {
		FailNotifierRenderer.render(((Level05)level).failNotifier, batcher);
		if (level.state==Level.TUTORIAL_PAUSED){
			Hint hint=((Level05)level).hint;
			batcher.beginBatch(Assets1.tutorialAtlas);
			batcher.drawSprite(hint.position.x, hint.position.y, hint.size, hint.size, Assets1.noFoodShallPass, SpriteBatcher.NORMAL);
			if (hint.button.enabled)
				batcher.drawSprite(hint.button.position.x, hint.button.position.y, hint.button.bounds.width, hint.button.bounds.height, Assets1.okButton, SpriteBatcher.NORMAL);
			batcher.endBatch();
		}
	}

	@Override
	protected void renderCurrentTarget() {
		if (level.currentTarget!=null){
			if (level.currentTarget.state==Target.ALIVE){
				if (level.currentTarget instanceof Cherry){
					FoodRenderer.renderCherryOwnAtlas((Cherry)level.currentTarget, batcher);
					return;
				}
				
				if (level.currentTarget instanceof Strawberry){
					FoodRenderer.renderStrawberryOwnAtlas((Strawberry)level.currentTarget, batcher);
					return;
				}
				
				if (level.currentTarget instanceof Bee){
					batcher.beginBatch(BeeAssets.beeAtlas);
					beeRenderer.render(batcher);
					batcher.endBatch();
					return;
				}
			}
		}
		
	}

	@Override
	void renderInBackground() {
		// TODO Auto-generated method stub
		
	}

}
