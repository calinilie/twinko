package com.calin.tests.renderers;

import com.calin.test.levels.Level;
import com.calin.test.levels.Level05;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.BeeAssets;
import com.calin.tests.assets.FoodAssets;
import com.calin.test.levels.Level08;
import com.calin.tests.food.Cherry;
import com.calin.tests.food.Mushroom;
import com.calin.tests.food.Plum;
import com.calin.tests.food.Strawberry;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.impl.GLGraphics;
import com.calin.tests.objects.Bee;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.Hint;
import com.calin.tests.objects.Target;

public class Level08Renderer extends LevelRenderer{

	BeeRenderer beeRenderer;
	
	public Level08Renderer(Level level, GLGraphics graphics,
			SpriteBatcher batcher, SpriteBatcher fireBatcher) {
		super(level, graphics, batcher, fireBatcher);
		beeRenderer=new BeeRenderer(((Level08)level).bee);
	}

//	@Override
//	protected void renderDragon(float deltaTime){
//		Dragon dragon = level.dragon;
//		batcher.beginBatch(Assets1.dragonAtlas);
//		float x=dragon.position.x;
//		float y=dragon.position.y;
//		float width=dragon.width;
//		float height=dragon.height;
//		float factor=1.6f;
//		batcher.drawSprite(x-(width*0.08333f), y-(width*0.08333f), -width/factor, height/factor, Assets1.dragonBody, SpriteBatcher.NORMAL);
//		batcher.drawSprite(x, y, -width , height, dragon.angle, Assets1.dragon);
//		batcher.endBatch();
//	}
	
	@Override
	protected void renderFood() {
		Mushroom mushroomFast=((Level08)level).mushroomFast;
		Mushroom mushroomSlow=((Level08)level).mushroomSlow;
		Mushroom mushroomFastLow=((Level08)level).mushroomFastLow;
		Mushroom mushroomSlowLow=((Level08)level).mushroomSlowLow;
		Plum plum=((Level08)level).plum;
		
		batcher.beginBatch(FoodAssets.foodAtlas);
		if (mushroomFastLow!=level.currentTarget){
			FoodRenderer.renderMushRoom(mushroomFastLow, batcher);
		}
		if (mushroomSlowLow!=level.currentTarget){
			FoodRenderer.renderMushRoom(mushroomSlowLow, batcher);
		}
		
		if (mushroomFast!=level.currentTarget && level.targets.contains(mushroomFast)){
			FoodRenderer.renderMushRoom(mushroomFast, batcher);
		}
		if (mushroomSlow!=level.currentTarget && level.targets.contains(mushroomSlow)){
			FoodRenderer.renderMushRoom(mushroomSlow, batcher);
		}
		
		if (plum!=level.currentTarget && level.targets.contains(plum)){
			FoodRenderer.renderPlum(plum, batcher);
		}
		batcher.endBatch();
		
	}

	@Override
	protected void renderEnemies() {
		Bee bee=((Level08)level).bee;
		if (level.currentTarget!=bee && bee.state==Target.ALIVE){
			batcher.beginBatch(BeeAssets.beeAtlas);
			beeRenderer.render(batcher);
			batcher.endBatch();
		}
	}

	@Override
	void renderTutorial() {
		if (level.state==Level.TUTORIAL_PAUSED){
			Hint hint=((Level08)level).hint;
			batcher.beginBatch(Assets1.tutorialAtlas);
			batcher.drawSprite(hint.position.x, hint.position.y, hint.size, hint.size, Assets1.hintLevel8, SpriteBatcher.NORMAL);
			if (hint.button.enabled)
				batcher.drawSprite(hint.button.position.x, hint.button.position.y, hint.button.bounds.width, hint.button.bounds.height, Assets1.okButton, SpriteBatcher.NORMAL);
			batcher.endBatch();
		}
		
	}

	@Override
	protected void renderCurrentTarget() {
		if (level.currentTarget!=null){
			if (level.currentTarget.state==Target.ALIVE){
				if (level.currentTarget instanceof Mushroom){
					FoodRenderer.renderMushRoomOwnAtlas((Mushroom) level.currentTarget, batcher);
					return;
				}
				
				if (level.currentTarget instanceof Plum){
					FoodRenderer.renderPlumOwnAtlas((Plum) level.currentTarget, batcher);
					return;
				}
				
				if (level.currentTarget instanceof Bee){
					batcher.beginBatch(BeeAssets.beeAtlas);
					beeRenderer.render(batcher);
					batcher.endBatch();
					return;
				}
			}
		}
		
	}

	@Override
	void renderInBackground() {
		// TODO Auto-generated method stub
		
	}

}
