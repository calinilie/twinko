package com.calin.tests.renderers;

import android.util.Log;

import com.calin.test.levels.LevelIcon;
import com.calin.test.levels.LevelIconsDisplayModel;
import com.calin.tests.assets.StoreAssets;
import com.calin.tests.framework.gl.SpriteBatcher;

public class LevelsRenderer {
	
	public static void render(LevelIconsDisplayModel levels, SpriteBatcher batcher){
		switch(levels.state){
		case (LevelIconsDisplayModel.MEDOW_LEVELS):
			batcher.beginBatch(StoreAssets.buttonsAtlas);
			for (LevelIcon i : LevelIconsDisplayModel.medowLevels){
				batcher.drawSprite(
						i.position.x, 
						i.position.y, 
						i.bounds.width,
						i.bounds.height, 
						StoreAssets.powerOff,
						SpriteBatcher.NORMAL);
				if (!i.isUnlocked){
					batcher.drawSprite(
							i.position.x+i.bounds.width/2.5f, 
							i.position.y-i.bounds.height/2.5f,
							i.bounds.width/2, 
							i.bounds.height/2,
							StoreAssets.redX, 
							SpriteBatcher.NORMAL);
				}
			}
			batcher.endBatch();
			break;
		case (LevelIconsDisplayModel.JUNGLE_LEVELS):
			batcher.beginBatch(StoreAssets.buttonsAtlas);
			for (LevelIcon i: LevelIconsDisplayModel.jungleLevels){
				batcher.drawSprite(
						i.position.x, 
						i.position.y, 
						i.bounds.width, 
						i.bounds.height, 
						StoreAssets.coinRegion, 
						SpriteBatcher.NORMAL);
				if (!i.isUnlocked){
					batcher.drawSprite(
							i.position.x+i.bounds.width/2.5f, 
							i.position.y-i.bounds.height/2.5f,
							i.bounds.width/2, 
							i.bounds.height/2,
							StoreAssets.redX, 
							SpriteBatcher.NORMAL);
				}
			}
			batcher.endBatch();
			break;
		case (LevelIconsDisplayModel.CAVE_LEVELS):
			batcher.beginBatch(StoreAssets.buttonsAtlas);
			for (LevelIcon i: LevelIconsDisplayModel.caveLevels){
				batcher.drawSprite(
						i.position.x, 
						i.position.y, 
						i.bounds.width, 
						i.bounds.height, 
						StoreAssets.coinRegion, 
						SpriteBatcher.NORMAL);
				if (!i.isUnlocked){
					batcher.drawSprite(
							i.position.x+i.bounds.width/2.5f, 
							i.position.y-i.bounds.height/2.5f,
							i.bounds.width/2, 
							i.bounds.height/2,
							StoreAssets.redX, 
							SpriteBatcher.NORMAL);
				}
			}
			batcher.endBatch();	
			break;
		}
	}
	
	

}
