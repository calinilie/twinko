package com.calin.tests.renderers;

import android.util.Log;

import com.calin.test.levels.Level;
import com.calin.test.levels.LevelArcade;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.BeeAssets;
import com.calin.tests.assets.FoodAssets;
import com.calin.tests.food.Cherry;
import com.calin.tests.food.Mushroom;
import com.calin.tests.food.Orange;
import com.calin.tests.food.Peach;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.impl.GLGraphics;
import com.calin.tests.objects.Bee;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.Target;

public class LevelArcadeRenderer extends LevelRenderer{

	BeeRenderer beeRenderer;
	
	public LevelArcadeRenderer(Level level, GLGraphics graphics,
			SpriteBatcher batcher, SpriteBatcher fireBatcher) {
		super(level, graphics, batcher, fireBatcher);
		beeRenderer=new BeeRenderer(((LevelArcade)level).beeEasy);
	}

	@Override
	protected void renderFood() {
		Mushroom mushroom=((LevelArcade)level).mushroom;
		Orange orange=((LevelArcade)level).orangeEasy;
		Cherry cherry=((LevelArcade)level).cherryEasy;
		
//		batcher.beginBatch(FoodAssets.foodAtlas);
		if (level.targets.contains(mushroom) && level.currentTarget!=mushroom){
			FoodRenderer.renderMushRoomOwnAtlas(mushroom, batcher);
		}
		
		if (level.targets.contains(orange) && level.currentTarget!=orange){
			FoodRenderer.renderOrangeOwnAtlas(orange, batcher);
		}
		
		if (level.targets.contains(cherry) && level.currentTarget!=cherry){
			FoodRenderer.renderCherryOwnAtlas(cherry, batcher);
		}
//		batcher.endBatch();
		
	}
	
	@Override
	protected void renderDragon(float deltaTime){
		Dragon dragon = level.dragon;
		batcher.beginBatch(Assets1.dragonAtlas);
		float x=dragon.position.x;
		float y=dragon.position.y;
		float width=dragon.width;
		float height=dragon.height;
		float factor=1.6f;
		batcher.drawSprite(x-(width*0.08333f), y-(width*0.08333f), -width/factor, height/factor, Assets1.dragonBody, SpriteBatcher.NORMAL);
		batcher.drawSprite(x, y, -width , height, 0, Assets1.dragon);
		batcher.endBatch();
	}
	
	@Override
	protected void renderTimer(){
	}

	@Override
	protected void renderEnemies() {
		if (((LevelArcade)level).beeEasy!=level.currentTarget){
			renderBee();
		}
	}

	@Override
	void renderTutorial() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void renderCurrentTarget() {
		if (level.currentTarget!=null){
			Target currentTarget=level.currentTarget;
			if (currentTarget.state==Target.ALIVE){
				if (currentTarget instanceof Mushroom){
					batcher.beginBatch(FoodAssets.foodAtlas);
					FoodRenderer.renderMushRoom((Mushroom)currentTarget, batcher);
					batcher.endBatch();
					return;
				}
				
				if (currentTarget instanceof Orange){
					batcher.beginBatch(FoodAssets.foodAtlas);
					FoodRenderer.renderOrange((Orange)currentTarget, batcher);
					batcher.endBatch();
					return;
				}
				
				if (currentTarget instanceof Bee){
					renderBee();
					return;
				}
				 if (currentTarget instanceof Cherry){
					 batcher.beginBatch(FoodAssets.foodAtlas);
					 FoodRenderer.renderCherry((Cherry)currentTarget, batcher);
					 batcher.endBatch();
					 return;
				 }
				
			}
		}
	}
	
	private void renderBee(){
		batcher.beginBatch(BeeAssets.beeAtlas);
		beeRenderer.render(batcher);
		batcher.endBatch();
	}

	@Override
	void renderInBackground() {
		// TODO Auto-generated method stub
		
	}
	
	
//	private void renderMushroom(Mushroom mushroom){
//		batcher.beginBatch(Assets1.digitsAtlas);
//		batcher.drawSprite(mushroom.position.x, mushroom.position.y, mushroom.size, mushroom.size, mushroom.angle, Assets1.mushroomRegion);
//		batcher.endBatch();
//	}
//	
//	private void renderPeach(Orange orange){
//		batcher.beginBatch(FoodAssets.foodAtlas);
//		batcher.drawSprite(orange.position.x, orange.position.y, orange.size, orange.size, orange.angle, FoodAssets.orange);
//		batcher.endBatch();
//	}
//	
//	private void renderCherry(Cherry cherry){
//		batcher.beginBatch(FoodAssets.foodAtlas);
//		batcher.drawSprite(cherry.position.x, cherry.position.y, cherry.size, cherry.size, cherry.angle, FoodAssets.cherry);
//		batcher.endBatch();
//	}

}
