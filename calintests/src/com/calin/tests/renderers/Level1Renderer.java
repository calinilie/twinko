package com.calin.tests.renderers;

import com.calin.test.levels.Level;
import com.calin.test.levels.Level1;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.FoodAssets;
import com.calin.tests.food.Broccoli;
import com.calin.tests.food.Mushroom;
import com.calin.tests.food.Stake;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.framework.impl.GLGraphics;
import com.calin.tests.objects.Bat;
import com.calin.tests.objects.Target;

public class Level1Renderer extends LevelRenderer{

	public Level1Renderer(Level level, GLGraphics graphics,
			SpriteBatcher batcher, SpriteBatcher fireBatcher) {
		super(level, graphics, batcher, fireBatcher);
	}

	@Override
	protected void renderFood() {
		Stake s=((Level1)level).mushroom;
		if (s.state==Target.ALIVE && s!=level.currentTarget){
//			batcher.drawSprite(s.position.x, s.position.y, Level1.STAKE_SIZE, Level1.STAKE_SIZE, s.angle, Assets1.mushroomRegion);
			batcher.beginBatch(FoodAssets.foodAtlas);
			FoodRenderer.renderMushRoom((Mushroom)s, batcher);
			batcher.endBatch();
		}
		
		s=((Level1)level).broccoli;
		if (s.state==Target.ALIVE && s!=level.currentTarget)
			batcher.drawSprite(s.position.x, s.position.y, Level1.STAKE_SIZE, Level1.STAKE_SIZE, s.angle, Assets1.broccoliRegion);
		
		s=((Level1)level).stake3;
		if (s.state==Target.ALIVE && s!=level.currentTarget)
			batcher.drawSprite(s.position.x, s.position.y, Level1.STAKE_SIZE, Level1.STAKE_SIZE, s.angle, Assets1.stakeRegion);
	}

	@Override
	protected void renderEnemies() {
//		Bat bat=((Level1)level).bat;
//		if (bat!=level.currentTarget){
//	        EnemyRenderer.renderBat(bat, batcher);
//		}		
	}

	@Override
	protected void renderCurrentTarget() {
		Target target=level.currentTarget;
		if (target.state==Target.ALIVE){
			if (target instanceof Bat){
				batcher.beginBatch(Assets1.digitsAtlas);
//				EnemyRenderer.renderBat((Bat)target, batcher);
				batcher.endBatch();
			}
			else{
				TextureRegion region=null;
				if (target instanceof Stake) region=Assets1.stakeRegion;
				if (target instanceof Mushroom) region=Assets1.stakeRegion;
				if (target instanceof Broccoli) region=Assets1.broccoliRegion;
				batcher.beginBatch(Assets1.digitsAtlas);
				batcher.drawSprite(target.position.x, target.position.y, target.bounds.width, target.bounds.height, ((Stake)target).angle, region);
				batcher.endBatch();
			}
		}		
	}

	@Override
	void renderTutorial() {
		// TODO Auto-generated method stub
		
	}

	@Override
	void renderInBackground() {
		// TODO Auto-generated method stub
		
	}

}
