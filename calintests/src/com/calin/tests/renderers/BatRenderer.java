package com.calin.tests.renderers;

import com.calin.tests.assets.BatAssets;
import com.calin.tests.framework.gl.Animation;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.objects.Bat;

public class BatRenderer {

	Bat bat;
	
	public BatRenderer(Bat bat){
		this.bat=bat;
	}
	
	public void render(SpriteBatcher batcher, float time){
		float size=6;
		float x=8; 
		float y=4.5f;
		
		batcher.beginBatch(BatAssets.batAtlas);
		TextureRegion keyframe=BatAssets.batAnimation.getKeyFrame(time, Animation.ANIMATION_LOOPING);
		batcher.drawSprite(x, y, size, size, keyframe, SpriteBatcher.NORMAL);
		batcher.drawSprite(x-size/15, y-size*0.1475f, size/2, size/2, BatAssets.headAngry, SpriteBatcher.NORMAL);
		batcher.endBatch();
	}
	
}
