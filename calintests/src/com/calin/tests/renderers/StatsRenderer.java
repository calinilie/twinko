package com.calin.tests.renderers;

import com.calin.test.levels.StatsDisplayModel;
import com.calin.tests.GameObject;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.ButtonsAsstets;
import com.calin.tests.framework.gl.SpriteBatcher;

public class StatsRenderer {
	
	public static void render(StatsDisplayModel statsDisplayModel, SpriteBatcher batcher){
		batcher.beginBatch(Assets1.digitsAtlas);
		//stamina
		GameObject gameObject = statsDisplayModel.staminaText;
		batcher.drawSprite(gameObject.position.x, gameObject.position.y, gameObject.bounds.width, gameObject.bounds.height, Assets1.maxStaminaTexture, SpriteBatcher.NORMAL);
		gameObject = statsDisplayModel.incStamina;
		batcher.drawSprite(gameObject.position.x, gameObject.position.y, gameObject.bounds.width, gameObject.bounds.height, Assets1.incTexture, SpriteBatcher.NORMAL);
		RendererHelper.renderNoDisplay(Assets1.digits, batcher, statsDisplayModel.staminaPoints);
		
		//HP
		gameObject = statsDisplayModel.hpText;
		batcher.drawSprite(gameObject.position.x, gameObject.position.y, gameObject.bounds.width, gameObject.bounds.height, Assets1.hpTexture, SpriteBatcher.NORMAL);
		gameObject = statsDisplayModel.incHp;
		batcher.drawSprite(gameObject.position.x, gameObject.position.y, gameObject.bounds.width, gameObject.bounds.height, Assets1.incTexture, SpriteBatcher.NORMAL);
		RendererHelper.renderNoDisplay(Assets1.digits, batcher, statsDisplayModel.hpPoints);
		
		//DMG
		gameObject = statsDisplayModel.damageText;
		batcher.drawSprite(gameObject.position.x, gameObject.position.y, gameObject.bounds.width, gameObject.bounds.height, Assets1.dmgTexture, SpriteBatcher.NORMAL);
		gameObject = statsDisplayModel.incDamage;
		batcher.drawSprite(gameObject.position.x, gameObject.position.y, gameObject.bounds.width, gameObject.bounds.height, Assets1.incTexture, SpriteBatcher.NORMAL);
		RendererHelper.renderNoDisplay(Assets1.digits, batcher, statsDisplayModel.damagePoints);
		
		//skill points
		gameObject = statsDisplayModel.skillPointsText;
		batcher.drawSprite(gameObject.position.x, gameObject.position.y, gameObject.bounds.width, gameObject.bounds.height, Assets1.skillPointsTexture, SpriteBatcher.NORMAL);
		gameObject = statsDisplayModel.moreSkillPoints;
		batcher.drawSprite(gameObject.position.x, gameObject.position.y, gameObject.bounds.width, gameObject.bounds.height, Assets1.incTexture, SpriteBatcher.NORMAL);
		RendererHelper.renderNoDisplay(Assets1.digits, batcher, statsDisplayModel.skillPoints);
		
		
		batcher.endBatch();
		
		//back
		batcher.beginBatch(ButtonsAsstets.atlas);
		batcher.drawSprite(statsDisplayModel.back.position.x, statsDisplayModel.back.position.y, statsDisplayModel.back.bounds.width,  statsDisplayModel.back.bounds.height, ButtonsAsstets.back, SpriteBatcher.NORMAL);
		batcher.endBatch();
		
	}

}
