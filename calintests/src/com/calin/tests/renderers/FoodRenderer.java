package com.calin.tests.renderers;

import com.calin.tests.assets.FoodAssets;
import com.calin.tests.food.*;
import com.calin.tests.framework.gl.SpriteBatcher;

public class FoodRenderer {
	
	public static void renderMushRoomOwnAtlas(Mushroom mushroom, SpriteBatcher batcher){
		batcher.beginBatch(FoodAssets.foodAtlas);
		batcher.drawSprite(mushroom.position.x, mushroom.position.y, mushroom.size, mushroom.size, mushroom.angle, FoodAssets.mushroom);
		batcher.endBatch();
	}
	
	public static void renderMushRoom(Mushroom mushroom, SpriteBatcher batcher){
		batcher.drawSprite(mushroom.position.x, mushroom.position.y, mushroom.size, mushroom.size, mushroom.angle, FoodAssets.mushroom);
	}
	
	public static void renderCherryOwnAtlas(Cherry cherry, SpriteBatcher batcher){
		batcher.beginBatch(FoodAssets.foodAtlas);
		batcher.drawSprite(cherry.position.x, cherry.position.y, cherry.size, cherry.size, cherry.angle, FoodAssets.cherry);
		batcher.endBatch();
	}
	
	public static void renderCherry(Cherry cherry, SpriteBatcher batcher){
		batcher.drawSprite(cherry.position.x, cherry.position.y, cherry.size, cherry.size, cherry.angle, FoodAssets.cherry);
	}
	
	public static void renderOrangeOwnAtlas(Orange orange, SpriteBatcher batcher){
		batcher.beginBatch(FoodAssets.foodAtlas);
		batcher.drawSprite(orange.position.x, orange.position.y, orange.size, orange.size, orange.angle, FoodAssets.orange);
		batcher.endBatch();
	}
	
	public static void renderOrange(Orange orange, SpriteBatcher batcher){
		batcher.drawSprite(orange.position.x, orange.position.y, orange.size, orange.size, orange.angle, FoodAssets.orange);
	}
	
	public static void renderStrawberryOwnAtlas(Strawberry strawberry, SpriteBatcher batcher){
		batcher.beginBatch(FoodAssets.foodAtlas);
		batcher.drawSprite(strawberry.position.x, strawberry.position.y, strawberry.size, strawberry.size, strawberry.angle, FoodAssets.strawberry);
		batcher.endBatch();
	}
	
	public static void renderStrawberry(Strawberry strawberry, SpriteBatcher batcher){
		batcher.drawSprite(strawberry.position.x, strawberry.position.y, strawberry.size, strawberry.size, strawberry.angle, FoodAssets.strawberry);
	}
	
	public static void renderPlumOwnAtlas(Plum plum, SpriteBatcher batcher){
		batcher.beginBatch(FoodAssets.foodAtlas);
		batcher.drawSprite(plum.position.x, plum.position.y, plum.size, plum.size, plum.angle, FoodAssets.plum);
		batcher.endBatch();
	}
	
	public static void renderPlum(Plum plum, SpriteBatcher batcher){
		batcher.drawSprite(plum.position.x, plum.position.y, plum.size, plum.size, plum.angle, FoodAssets.plum);
	}

}
