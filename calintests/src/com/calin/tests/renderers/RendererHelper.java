package com.calin.tests.renderers;

import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.objects.Digit;
import com.calin.tests.objects.NumbersDisplay;

public abstract class RendererHelper {
	
	public static void renderNoDisplay(TextureRegion[] digitsAssets, SpriteBatcher batcher, NumbersDisplay no){
		for (int i = 0; i < no.digits.length; i++) {
			Digit d=no.digits[i];
			if (d.display==true){
				batcher.drawSprite(d.position.x, d.position.y, d.bounds.width, d.bounds.height, digitsAssets[d.value], SpriteBatcher.NORMAL);
			}
		}
	}
}
