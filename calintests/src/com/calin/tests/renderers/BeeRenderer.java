package com.calin.tests.renderers;

import com.calin.tests.assets.BeeAssets;
import com.calin.tests.framework.gl.Animation;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.objects.Bee;

public class BeeRenderer {
	
//	SpriteBatcher batcher;
	Bee bee;
	
	public BeeRenderer(Bee bee, SpriteBatcher batcher){
		this.bee=bee;
//		this.batcher=batcher;
	}
	
	public BeeRenderer(Bee bee){
		this.bee=bee;
	}
	
	public void render(SpriteBatcher batcher){
		float x=bee.position.x;
		float y=bee.position.y;
		float width, height;
		height=bee.size;
		if (bee.orientation==Bee.WEST) width=bee.size;
		else width=-bee.size;
		
		float headX=x-width/4;
		float headY=y-height/3.61f;
		
		float wingX=x+width/20;
		float wingY=y+height/2.4f;
		
		float handsX=x+width/2.3076f;
		float handsY=y-height/5.45454545f;
		
//		batcher.beginBatch(BeeAssets.beeAtlas);
		batcher.drawSprite(x, y, width, height, BeeAssets.body, SpriteBatcher.NORMAL);
		TextureRegion headTexture;
		if (bee.mood==Bee.MOOD_ANGRY){
			headTexture = (bee.eyes==Bee.EYES_OPENED) ? BeeAssets.headAngry : BeeAssets.headAngryEyesClosed;
		}
		else{
			headTexture = (bee.eyes==Bee.EYES_OPENED) ? BeeAssets.headNormal : BeeAssets.headNormalEyesClosed;
		}
		batcher.drawSprite(headX, headY, width, height, headTexture, SpriteBatcher.NORMAL);
			TextureRegion keyFrame=BeeAssets.wings.getKeyFrame(bee.flyingTime, (bee.flyingState==Bee.FLYING) ?  Animation.ANIMATION_LOOPING : Animation.ANIMATION_NONLOOPING);
			batcher.drawSprite(wingX, wingY, width, height, keyFrame ,SpriteBatcher.NORMAL);		
		batcher.drawSprite(handsX, handsY, width, height, (bee.mood==Bee.MOOD_NEUTRAL) ? BeeAssets.handsNormal : BeeAssets.handsAngry, SpriteBatcher.NORMAL);
//		batcher.endBatch();
	}

}
