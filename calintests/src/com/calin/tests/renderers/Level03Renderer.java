package com.calin.tests.renderers;

import com.calin.test.levels.Level;
import com.calin.test.levels.Level03;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.FoodAssets;
import com.calin.tests.food.Mushroom;
import com.calin.tests.food.Orange;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.impl.GLGraphics;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.Hint;
import com.calin.tests.objects.Target;

public class Level03Renderer extends LevelRenderer{

	public Level03Renderer(Level level, GLGraphics graphics,
			SpriteBatcher batcher, SpriteBatcher fireBatcher) {
		super(level, graphics, batcher, fireBatcher);
		// TODO Auto-generated constructor stub
	}
	
//	@Override
//	protected void renderDragon(float deltaTime){
//		Dragon dragon = level.dragon;
//		batcher.beginBatch(Assets1.dragonAtlas);
//		float x=dragon.position.x;
//		float y=dragon.position.y;
//		float width=dragon.width;
//		float height=dragon.height;
//		float factor=1.6f;
//		batcher.drawSprite(x-(width*0.08333f), y-(width*0.08333f), -width/factor, height/factor, Assets1.dragonBody, SpriteBatcher.NORMAL);
//		batcher.drawSprite(x, y, -width , height, dragon.angle, Assets1.dragon);
//		batcher.endBatch();
//	}

	@Override
	protected void renderFood() {
		Mushroom mushroom=((Level03)level).mushroom;
		Orange orange=((Level03)level).orange;
		batcher.beginBatch(FoodAssets.foodAtlas);
		if (level.currentTarget!=mushroom){
			FoodRenderer.renderMushRoom(mushroom, batcher);
		}
		
		if (level.currentTarget!=orange){
			FoodRenderer.renderOrange(orange, batcher);
		}
		batcher.endBatch();
		
	}

	@Override
	protected void renderEnemies() {
		// TODO Auto-generated method stub
		
	}

	@Override
	void renderTutorial() {
		if (level.state==Level.TUTORIAL_PAUSED){
			Hint hint=((Level03)level).hint;
			batcher.beginBatch(Assets1.tutorialAtlas);
			batcher.drawSprite(hint.position.x, hint.position.y, hint.size, hint.size, Assets1.hint8, SpriteBatcher.NORMAL);
			if (hint.button.enabled)
				batcher.drawSprite(hint.button.position.x, hint.button.position.y, hint.button.bounds.width, hint.button.bounds.height, Assets1.okButton, SpriteBatcher.NORMAL);
			batcher.endBatch();
		}
		
	}

	@Override
	protected void renderCurrentTarget() {
		if (level.currentTarget!=null){
			if (level.currentTarget.state==Target.ALIVE){
				batcher.beginBatch(FoodAssets.foodAtlas);
				if (level.currentTarget instanceof Mushroom){
					FoodRenderer.renderMushRoom((Mushroom)level.currentTarget, batcher);
				}
				if (level.currentTarget instanceof Orange){
					FoodRenderer.renderOrange((Orange)level.currentTarget, batcher);
				}
				batcher.endBatch();
			}
		}
	}

	@Override
	void renderInBackground() {
		// TODO Auto-generated method stub
		
	}

}
