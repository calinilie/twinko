package com.calin.tests.renderers;

import com.calin.test.levels.LevelCategoriesDiplayModel;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.StoreAssets;
import com.calin.tests.framework.gl.SpriteBatcher;

public class LevelsCategoriesRenderer {

		public static void render(LevelCategoriesDiplayModel categories, SpriteBatcher batcher){
			batcher.beginBatch(StoreAssets.buttonsAtlas);
			batcher.drawSprite(categories.medowCategory.position.x, 
								categories.medowCategory.position.y, 
								categories.medowCategory.bounds.width, 
								categories.medowCategory.bounds.height, 
								StoreAssets.medow, SpriteBatcher.NORMAL);
			
			batcher.drawSprite(categories.jungleCategory.position.x, 
								categories.jungleCategory.position.y, 
								categories.jungleCategory.bounds.width, 
								categories.jungleCategory.bounds.height, 
								StoreAssets.jungle, 
								SpriteBatcher.NORMAL);
			if (!categories.jungleCategory.isUnlocked){
				batcher.drawSprite(categories.jungleCategory.position.x+LevelCategoriesDiplayModel.width/2.25f, 
									categories.jungleCategory.position.y-LevelCategoriesDiplayModel.height/2, 
									2, 
									2, 
									StoreAssets.redX, 
									SpriteBatcher.NORMAL);
			}
			
			batcher.drawSprite(categories.caveCategory.position.x,
								categories.caveCategory.position.y,
								categories.caveCategory.bounds.width,
								categories.caveCategory.bounds.height, StoreAssets.cave,
								SpriteBatcher.NORMAL);
			if (!categories.caveCategory.isUnlocked){
				batcher.drawSprite(categories.caveCategory.position.x+LevelCategoriesDiplayModel.width/2.25f, 
									categories.caveCategory.position.y-LevelCategoriesDiplayModel.height/2, 
									2, 
									2, 
									StoreAssets.redX, 
									SpriteBatcher.NORMAL);
			}
			batcher.endBatch();
		}
}
