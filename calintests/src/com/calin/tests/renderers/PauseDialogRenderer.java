package com.calin.tests.renderers;

import com.calin.test.levels.PausedDialog;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.ButtonsAsstets;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.objects.MenuButton;

public class PauseDialogRenderer {

	public static void render(PausedDialog dialog, SpriteBatcher batcher){
		if (dialog.state != MenuButton.INVISIBLE){
			
			batcher.beginBatch(Assets1.digitsAtlas);
			batcher.drawSprite(dialog.position.x, dialog.position.y, dialog.bounds.width, dialog.bounds.height, Assets1.dialog, SpriteBatcher.NORMAL);
			batcher.endBatch();
			
			batcher.beginBatch(ButtonsAsstets.atlas);
			batcher.drawSprite(dialog.continueButton.position.x, 
					dialog.continueButton.position.y, 
					dialog.continueButton.bounds.width,  
					dialog.continueButton.bounds.height, 
					ButtonsAsstets.ok, SpriteBatcher.NORMAL);
			
			batcher.drawSprite(dialog.replayButton.position.x, 
					dialog.replayButton.position.y, 
					dialog.replayButton.bounds.width,  
					dialog.replayButton.bounds.height, 
					ButtonsAsstets.replay, SpriteBatcher.NORMAL);
			
			batcher.drawSprite(dialog.shopButton.position.x, 
					dialog.shopButton.position.y, 
					dialog.shopButton.bounds.width,  
					dialog.shopButton.bounds.height, 
					ButtonsAsstets.buy, SpriteBatcher.NORMAL);
			batcher.endBatch();
		}
	}
	
}
