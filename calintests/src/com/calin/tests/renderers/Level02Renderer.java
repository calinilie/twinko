package com.calin.tests.renderers;

import com.calin.test.levels.Level;
import com.calin.test.levels.Level02;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.BeeAssets;
import com.calin.tests.assets.FoodAssets;
import com.calin.tests.food.Mushroom;
import com.calin.tests.food.Stake;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.framework.impl.GLGraphics;
import com.calin.tests.objects.Bee;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.Hint;
import com.calin.tests.objects.Target;

public class Level02Renderer extends LevelRenderer{
		
	BeeRenderer bee;
	public Level02Renderer(Level level, GLGraphics graphics,
			SpriteBatcher batcher, SpriteBatcher fireBatcher) {
		super(level, graphics, batcher, fireBatcher);
		bee=new BeeRenderer(((Level02)level).bee);
	}
	
//	@Override
//	protected void renderDragon(float deltaTime){
//		Dragon dragon = level.dragon;
//		batcher.beginBatch(Assets1.dragonAtlas);
//		float x=dragon.position.x;
//		float y=dragon.position.y;
//		float width=dragon.width;
//		float height=dragon.height;
//		float factor=1.6f;
//		batcher.drawSprite(x+(width*0.08333f), y-(width*0.08333f), width/factor, height/factor, Assets1.dragonBody, SpriteBatcher.NORMAL);
//		batcher.drawSprite(x, y, width , height, dragon.angle, Assets1.dragon);
//		batcher.endBatch();
//	}

	@Override
	protected void renderFood() {
		Mushroom target=((Level02)level).mushroom;
		if (target!=level.currentTarget){
			batcher.beginBatch(FoodAssets.foodAtlas);
			FoodRenderer.renderMushRoom((Mushroom)target, batcher);
			batcher.endBatch();
		}		
	}

	@Override
	protected void renderEnemies() {
		if (((Level02)level).bee!=level.currentTarget){
			batcher.beginBatch(BeeAssets.beeAtlas);
			bee.render(batcher);
			batcher.endBatch();
		}
	}

	@Override
	protected void renderCurrentTarget() {
		if (level.currentTarget!=null){
			Target target=level.currentTarget;
			if (target.state==Target.ALIVE){
				if (target instanceof Mushroom){
					batcher.beginBatch(FoodAssets.foodAtlas);
					FoodRenderer.renderMushRoom((Mushroom)target, batcher);
					batcher.endBatch();
					return;
				}
				if (target instanceof Bee){
					batcher.beginBatch(BeeAssets.beeAtlas);
					bee.render(batcher);
					batcher.endBatch();
					return;
				}
			}	
		}		
	}

	@Override
	void renderTutorial() {
		if (level.state==Level.TUTORIAL_PAUSED){
			Hint hint=((Level02)level).hint;
			TextureRegion region=null;
			switch (hint.id){
			case (5):
				region=Assets1.hint5;
				break;
			case (6):
				region=Assets1.hint6;
				break;
			case (7):
				region=Assets1.hint7;
				break;
			case (8):
				region=Assets1.hint8;
				break;
			}
			batcher.beginBatch(Assets1.tutorialAtlas);
			batcher.drawSprite(hint.position.x, hint.position.y, hint.size, hint.size, region, SpriteBatcher.NORMAL);
			if (hint.button.enabled)
				batcher.drawSprite(hint.button.position.x, hint.button.position.y, hint.button.bounds.width, hint.button.bounds.height, Assets1.okButton, SpriteBatcher.NORMAL);
			batcher.endBatch();
		}
		
	}

	@Override
	void renderInBackground() {
		// TODO Auto-generated method stub
		
	}

	
	
}
