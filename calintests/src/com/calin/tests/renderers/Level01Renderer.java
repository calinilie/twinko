package com.calin.tests.renderers;

import com.calin.test.levels.Level;
import com.calin.test.levels.Level01;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.FoodAssets;
import com.calin.tests.food.Mushroom;
import com.calin.tests.food.Stake;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.framework.impl.GLGraphics;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.Target;
import com.calin.tests.objects.Hint;

public class Level01Renderer extends LevelRenderer{

	public Level01Renderer(Level level, GLGraphics graphics,
			SpriteBatcher batcher, SpriteBatcher fireBatcher) {
		super(level, graphics, batcher, fireBatcher);
		// TODO Auto-generated constructor stub
	}
	
//	@Override
//	protected void renderDragon(float deltaTime){
//		Dragon dragon = level.dragon;
//		batcher.beginBatch(Assets1.dragonAtlas);
//		float x=dragon.position.x;
//		float y=dragon.position.y;
//		float width=dragon.width;
//		float height=dragon.height;
//		float factor=1.6f;
//		batcher.drawSprite(x-(width*0.08333f), y-(width*0.08333f), -width/factor, height/factor, Assets1.dragonBody, SpriteBatcher.NORMAL);
//		batcher.drawSprite(x, y, -width , height, dragon.angle, Assets1.dragon);
//		batcher.endBatch();
//	}

	@Override
	protected void renderFood() {
		Mushroom target=((Level01)level).mushroom;
		if (target!=level.currentTarget){
			batcher.beginBatch(FoodAssets.foodAtlas);
			FoodRenderer.renderMushRoom(target, batcher);
			batcher.endBatch();
		}
	}

	@Override
	protected void renderCurrentTarget() {
		if (level.currentTarget!=null){
			Target target=level.currentTarget;
			if (target.state==Target.ALIVE){
				batcher.beginBatch(FoodAssets.foodAtlas);
				FoodRenderer.renderMushRoom((Mushroom)target, batcher);
				batcher.endBatch();
			}	
		}
	}

	@Override
	protected void renderEnemies() {
		// TODO Auto-generated method stub
		
	}

	@Override
	void renderTutorial() {
		if (level.state==Level.TUTORIAL_PAUSED){
			Hint hint=((Level01)level).hint;
			TextureRegion region=null;
			switch (hint.id){
			case 1:
				region=Assets1.hint1;
				break;
			case 2:
				region=Assets1.hint2;
				break;
			case 3:
				region=Assets1.hint3;
				break;	
			case 4:
				region=Assets1.hint4;
				break;	
			}
			batcher.beginBatch(Assets1.tutorialAtlas);
			batcher.drawSprite(hint.position.x, hint.position.y, hint.size, hint.size, region, SpriteBatcher.NORMAL);
			if (hint.button.enabled)
				batcher.drawSprite(hint.button.position.x, hint.button.position.y, hint.button.bounds.width, hint.button.bounds.height, Assets1.okButton, SpriteBatcher.NORMAL);
			batcher.endBatch();
		}
	}

	@Override
	void renderInBackground() {
		// TODO Auto-generated method stub
		
	}
}
