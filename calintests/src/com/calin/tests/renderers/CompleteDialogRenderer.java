package com.calin.tests.renderers;

import com.calin.test.levels.CompleteDialog;
import com.calin.tests.assets.Assets1;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.objects.Digit;
import com.calin.tests.objects.NumbersDisplay;

public class CompleteDialogRenderer {

	public static void renderCompleteDialog(CompleteDialog cp, SpriteBatcher batcher){
		batcher.drawSprite(cp.position.x, cp.position.y, cp.bounds.width, cp.bounds.height, Assets1.dialog, SpriteBatcher.NORMAL);
		
		if (cp.newHighScoreBadge.visible) {
			batcher.drawSprite(cp.newHighScoreBadge.position.x,
					cp.newHighScoreBadge.position.y,
					cp.newHighScoreBadge.currentWidth,
					cp.newHighScoreBadge.currentHeight, 
					Assets1.highscore,
					SpriteBatcher.NORMAL);
		}

		if (cp.levelUpBadge.visible) {
			batcher.drawSprite(cp.levelUpBadge.position.x,
					cp.levelUpBadge.position.y, 
					cp.levelUpBadge.currentWidth,
					cp.levelUpBadge.currentHeight, 
					Assets1.highscore,
					SpriteBatcher.NORMAL);
		}
		
		if (cp.unlockedNewLevelsBadge.visible){
			batcher.drawSprite(cp.unlockedNewLevelsBadge.position.x,
					cp.unlockedNewLevelsBadge.position.y, 
					cp.unlockedNewLevelsBadge.currentWidth,
					cp.unlockedNewLevelsBadge.currentHeight, 
					Assets1.highscore,
					SpriteBatcher.NORMAL);
		}

		if (cp.currentScoreDisplay.state != NumbersDisplay.INACTIVE) {
			RendererHelper.renderNoDisplay(Assets1.digits, batcher, cp.currentScoreDisplay);
		}

		if (cp.earnedXpDisplay.state != NumbersDisplay.INACTIVE) {
			RendererHelper.renderNoDisplay(Assets1.digits, batcher, cp.earnedXpDisplay);
		}

		if (cp.earnedCoinsDisplay.state != NumbersDisplay.INACTIVE) {
			RendererHelper.renderNoDisplay(Assets1.digits, batcher, cp.earnedCoinsDisplay);
		}
	}

}
