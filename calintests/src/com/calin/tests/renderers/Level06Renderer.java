package com.calin.tests.renderers;

import com.calin.test.levels.Level;
import com.calin.test.levels.Level06;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.BeeAssets;
import com.calin.tests.assets.FoodAssets;
import com.calin.tests.food.Cherry;
import com.calin.tests.food.Strawberry;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.impl.GLGraphics;
import com.calin.tests.objects.Bee;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.Hint;
import com.calin.tests.objects.Target;

public class Level06Renderer extends LevelRenderer{

	BeeRenderer bee;
	public Level06Renderer(Level level, GLGraphics graphics,
			SpriteBatcher batcher, SpriteBatcher fireBatcher) {
		super(level, graphics, batcher, fireBatcher);
		bee=new BeeRenderer(((Level06)level).bee);
	}
	
//	@Override
//	protected void renderDragon(float deltaTime){
//		Dragon dragon = level.dragon;
//		batcher.beginBatch(Assets1.dragonAtlas);
//		float x=dragon.position.x;
//		float y=dragon.position.y;
//		float width=dragon.width;
//		float height=dragon.height;
//		float factor=1.6f;
//		batcher.drawSprite(x+(width*0.08333f), y-(width*0.08333f), width/factor, height/factor, Assets1.dragonBody, SpriteBatcher.NORMAL);
//		batcher.drawSprite(x, y, width , height, dragon.angle, Assets1.dragon);
//		batcher.endBatch();
//	}

	@Override
	protected void renderFood() {
		
		Cherry cherry = ((Level06)level).cherry;
		Strawberry stSlowLow = ((Level06)level).stSlowLow;
		Strawberry stFast = ((Level06)level).stFast;
//		Strawberry stSlow = ((Level06)level).stSlow;
		Strawberry stFastLow = ((Level06)level).stFastLow;
//		Strawberry stNormal= ((Level06)level).stNormal;
		
		batcher.beginBatch(FoodAssets.foodAtlas);
		if (level.currentTarget!=cherry)
			FoodRenderer.renderCherry(cherry, batcher);
		if (level.currentTarget!=stSlowLow)
			FoodRenderer.renderStrawberry(stSlowLow, batcher);
		
		if (level.currentTarget!=stFast && level.targets.contains(stFast))
			FoodRenderer.renderStrawberry(stFast, batcher);
		
//		if (level.currentTarget!=stSlow && level.targets.contains(stSlow))
//			FoodRenderer.renderStrawberry(stSlow, batcher);
		
		if (level.currentTarget!=stFastLow && level.targets.contains(stFastLow))
			FoodRenderer.renderStrawberry(stFastLow, batcher);
		
//		if (level.currentTarget!=stNormal && level.targets.contains(stNormal))
//			FoodRenderer.renderStrawberry(stNormal, batcher);
		
		batcher.endBatch();
	}

	@Override
	protected void renderEnemies() {
		if (((Level06)level).bee!=level.currentTarget){
			batcher.beginBatch(BeeAssets.beeAtlas);
			bee.render(batcher);
			batcher.endBatch();
		}
		
	}

	@Override
	void renderTutorial() {
		if (level.state==Level.TUTORIAL_PAUSED){
			Hint hint=((Level06)level).hint;
			batcher.beginBatch(Assets1.tutorialAtlas);
			batcher.drawSprite(hint.position.x, hint.position.y, hint.size, hint.size, Assets1.hintLevel6, SpriteBatcher.NORMAL);
			if (hint.button.enabled)
				batcher.drawSprite(hint.button.position.x, hint.button.position.y, hint.button.bounds.width, hint.button.bounds.height, Assets1.okButton, SpriteBatcher.NORMAL);
			batcher.endBatch();
		}
		
	}

	@Override
	protected void renderCurrentTarget() {
		if (level.currentTarget!=null){
			if (level.currentTarget.state==Target.ALIVE){
				if (level.currentTarget instanceof Cherry){
					FoodRenderer.renderCherryOwnAtlas((Cherry)level.currentTarget, batcher);
					return;
				}
				
				if (level.currentTarget instanceof Strawberry){
					FoodRenderer.renderStrawberryOwnAtlas((Strawberry)level.currentTarget, batcher);
					return;
				}
				
				if (level.currentTarget instanceof Bee){
					batcher.beginBatch(BeeAssets.beeAtlas);
					bee.render(batcher);
					batcher.endBatch();
					return;
				}
			}
		}
		
	}

	@Override
	void renderInBackground() {
		// TODO Auto-generated method stub
		
	}

}
