package com.calin.tests.renderers;

import android.util.Log;

import com.calin.tests.Drag;
import com.calin.tests.IDragonHitListener;
import com.calin.tests.IDragonRendererListener;
import com.calin.tests.IMouthRendererListener;
import com.calin.tests.assets.TwinkoAssets;
import com.calin.tests.framework.gl.Animation;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.framework.math.Vector2;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.DragonEyes;
import com.calin.tests.objects.DragonHead;
import com.calin.tests.objects.DragonMouth;
import com.calin.tests.objects.Enemy;

public class TwinkoRenderer implements IDragonRendererListener, IMouthRendererListener, IDragonHitListener{
	
	Dragon dragon;
	SpriteBatcher batcher;
	float lfx, lfy, rfx, rfy, width, height;
	//TODO refac to static class
	public TwinkoRenderer(Dragon dragon, SpriteBatcher batcher){
		this.dragon=dragon;
		this.batcher=batcher;
		height=dragon.height;
//		width=(Dragon.orientation==Dragon.WEST) ? -dragon.width : dragon.width;
		width=dragon.width;
		
		lfx=dragon.position.x+width/5.2f;
		lfy=dragon.position.y-height/2.28070175438596f;
		
		rfx=dragon.position.x-width/6.63265306122449f;
		rfy=dragon.position.y-height/2.24137931034483f;
	}
	
	public void render(float deltaTime, boolean isTouched){
		float x=dragon.position.x;
		float y=dragon.position.y;
//		float height=dragon.height;
//		float width=(Dragon.orientation==Dragon.WEST) ? -dragon.width : dragon.width;
		DragonMouth mouth = dragon.head.mouth;
		
		
		batcher.beginBatch(TwinkoAssets.atlas);
		batcher.drawSprite(lfx, lfy, width/2, height/2, TwinkoAssets.leftFoot, SpriteBatcher.NORMAL);//left foot
		
		float tx=x-width*0.28f;
		float ty=y-height*0.33f;
		batcher.drawSprite(tx, ty, width, height, dragon.tailAngle, TwinkoAssets.tail);
		
		float wx=x-width*0.125f;
		float wy=y+height*0.0375f;
		batcher.drawSprite(wx, wy, width, height, dragon.wingAngle, TwinkoAssets.wing);
		
		
		//left arm
		float lax=x+width*0.21f;
		float lay=y-height*0.0575f;	
		TextureRegion leftArmRegion=null;
		if (dragon.state==Dragon.HIT) leftArmRegion=TwinkoAssets.leftArmUp.getKeyFrame(deltaTime);
		if (dragon.state==Dragon.CONFUSED){
			lax=x+width*0.225f;
			lay=y-height*0.095f;
			leftArmRegion=TwinkoAssets.leftArmConfused.getKeyFrame(deltaTime);
		}
		if (dragon.state==Dragon.FLEX){
			lax=x+width*0.25f;//x+width*0.2275f;
			lay=y-height*0.1075f;
			leftArmRegion=TwinkoAssets.leftArmFlex.getKeyFrame(deltaTime);
		}
		if (dragon.state==Dragon.SHY) {
			leftArmRegion=TwinkoAssets.leftArmUpScared.getKeyFrame(deltaTime);
			if (leftArmRegion==null) leftArmRegion=TwinkoAssets.leftArmUp6;
		}
		if (dragon.state==Dragon.HAND_WAVE){
			lax=x+width*0.17f;
		}
		if (leftArmRegion==null) leftArmRegion=TwinkoAssets.leftArmBase;
		batcher.drawSprite(lax, lay, width/2, height/2, dragon.leftHand.angle, leftArmRegion);
		
//		TextureRegion region=TwinkoAssets.handWave.getKeyFrame(time, Animation.ANIMATION_LOOPING);
//		lax=x+width*0.2625f;
//		lay=y-height*0.045f;
//		batcher.drawSprite(lax, lay, width/2, height/2, region, SpriteBatcher.NORMAL);
		
		//body
		batcher.drawSprite(x, y, width, height, TwinkoAssets.body, SpriteBatcher.NORMAL);
		
		//head
		DragonHead head=dragon.head;
		batcher.drawSprite(head.pos.x, head.pos.y, head.bounds.width, head.bounds.height, head.angle, TwinkoAssets.head);
		
		//eyes
		TextureRegion eyeRegion=TwinkoAssets.eyesNormalOpened;
		if (mouth.fireAnimationStarted && mouth.fireOK) eyeRegion=TwinkoAssets.eyesFireBreathing;
		float angle=head.angle;
		DragonEyes eyes=dragon.head.eyes;
		if (eyes.state==DragonEyes.SAD) eyeRegion=TwinkoAssets.eyesSad;
		if (eyes.state==DragonEyes.NORMAL_CLOSED) eyeRegion=TwinkoAssets.eyesNormalClosed;
		if (eyes.state==DragonEyes.CONFUSED) eyeRegion=TwinkoAssets.eyesConfused;
		if (eyes.state==DragonEyes.ANGRY) eyeRegion=TwinkoAssets.eyesAngry;
		if (eyes.state==DragonEyes.SURPRISED) eyeRegion=TwinkoAssets.eyesSad;
		if (eyes.state==DragonEyes.SHY){
			if (eyes.oneEyeOpen){
				if (eyes.eye % 2==0) eyeRegion=TwinkoAssets.eyesRightOpened;
				else eyeRegion=TwinkoAssets.eyesLeftOpened;
			}
			else eyeRegion=TwinkoAssets.eyesClosed;
		}
		batcher.drawSprite(eyes.pos.x, eyes.pos.y, eyes.width, eyes.height, angle, eyeRegion);
		
		//mouth
		TextureRegion mouthRegion=TwinkoAssets.startBreatheFire.getKeyFrame(deltaTime);
		if (dragon.head.mouth.isHit) {
//			mouthRegion=TwinkoAssets.ouch.getKeyFrame(deltaTime);
//			if (mouthRegion==null) 
				mouthRegion=TwinkoAssets.mouthSad;
		}
		if (dragon.head.mouth.isConfused) mouthRegion=TwinkoAssets.mouthConfused;
		if (mouthRegion==null){
			if (mouth.fireAnimationStarted) mouthRegion=TwinkoAssets.mouthFire;
			else mouthRegion=TwinkoAssets.mouthClosed;
		}
		if (dragon.state==Dragon.HAND_WAVE) mouthRegion=TwinkoAssets.mouthNormal2;
		DragonMouth.updatePosition(deltaTime, angle, head.pos);
		batcher.drawSprite(DragonMouth.pos.x, DragonMouth.pos.y, DragonMouth.width, DragonMouth.height, angle, mouthRegion);
		
		
		batcher.drawSprite(rfx, rfy, width/2, height/2, TwinkoAssets.rightFoot, SpriteBatcher.NORMAL);//right foot
		//right arm
		float ray=y-height*0.0575f;
		float rax=x;
		TextureRegion rightArmRegion=null;
		if (dragon.state==Dragon.HIT) rightArmRegion=TwinkoAssets.rightArmUp.getKeyFrame(deltaTime);
		if (dragon.state==Dragon.CONFUSED){
			rax=x-width*0.15f;
			ray=y-height*0.14f;
			rightArmRegion=TwinkoAssets.rightArmConfused.getKeyFrame(deltaTime);
		}
		if (dragon.state==Dragon.FLEX){
			rax=x-width*0.0125f;
			ray=y-height*0.155f;
			rightArmRegion=TwinkoAssets.rightArmFlex.getKeyFrame(deltaTime);
		}
		if (dragon.state==Dragon.SHY) {
			rightArmRegion=TwinkoAssets.rightArmUpScared.getKeyFrame(deltaTime);
			if (rightArmRegion==null) rightArmRegion=TwinkoAssets.rightArmUp6;
		}
		if (rightArmRegion==null) rightArmRegion=TwinkoAssets.rightArmBase;
		batcher.drawSprite(rax, ray, width/2, height/2, rightArmRegion, SpriteBatcher.NORMAL);
		
		batcher.endBatch();
	}

	@Override
	public void startFireBreathingAnimation() {
		TwinkoAssets.startBreatheFire.play();
	}

	@Override
	public void onDragonHitEvent(Enemy source, float damage) {
		TwinkoAssets.rightArmUp.play();
		TwinkoAssets.leftArmUp.play();
		TwinkoAssets.ouch.play();
	}

	@Override
	public boolean onConfusedEvent() {
		TwinkoAssets.rightArmConfused.play();
		TwinkoAssets.leftArmConfused.play();
		return true;
	}

	@Override
	public boolean onFlexStart() {
		TwinkoAssets.rightArmFlex.play();
		TwinkoAssets.leftArmFlex.play();
		return true;
	}

	@Override
	public boolean onShy() {
		TwinkoAssets.rightArmUpScared.play();
		TwinkoAssets.leftArmUpScared.play();
		return true;
	}
}
