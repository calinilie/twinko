package com.calin.tests.renderers;

import javax.microedition.khronos.opengles.GL10;

import android.util.Log;

import com.calin.test.levels.CompleteDialog;
import com.calin.test.levels.Level;
import com.calin.tests.assets.Assets1;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.framework.impl.GLGraphics;
import com.calin.tests.objects.Combo;
import com.calin.tests.objects.CookingStateBar;
import com.calin.tests.objects.Digit;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.PulseObject;
import com.calin.tests.particles.Fire;
import com.calin.tests.particles.Firefly;
import com.calin.tests.particles.FireflyEmitter;

public abstract class LevelRenderer {
	
	SpriteBatcher batcher;
	SpriteBatcher fireBatcher;
	Level level;
	GLGraphics graphics;
	public TwinkoRenderer dragonRenderer;
	
	public LevelRenderer(Level level, GLGraphics graphics, SpriteBatcher batcher, SpriteBatcher fireBatcher){
		this.level=level;
		this.graphics=graphics;
		this.batcher=batcher;
		this.fireBatcher=fireBatcher;
		dragonRenderer=new TwinkoRenderer(level.dragon, this.batcher);
	}
	
	public void render(float deltaTime){
		GL10 gl = graphics.getGL();
		renderBackground();
		renderInBackground();
		
		renderFireflies();
		renderDragon(deltaTime);
		
		
		renderCurrentTarget();
		
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE);
		renderFire();
		gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
		renderFood();
		renderEnemies();
		
		renderCookingBar();
		batcher.beginBatch(Assets1.digitsAtlas);
		renderBars();
		renderTimer();
		renderScore();
		renderCompleted();
		batcher.endBatch();
		renderPaused();
		renderTutorial();
		renderEffects();
	}
	
	protected void renderBackground(){
		batcher.beginBatch(Assets1.medowBackgroundAtlas);
		batcher.drawSprite(8, 4.5f, 20.48f, 10.24f, Assets1.medowBackground, SpriteBatcher.NORMAL);
		batcher.endBatch();
		
		if (level.combo.pulseState==PulseObject.PULSE){
			Combo c= level.combo;
			TextureRegion r=null;
			switch (c.comboState){
			case (Combo.X2):
				r=Assets1.comboX2;
				break;
			case (Combo.X3):
				r=Assets1.comboX3;
				break;
			case (Combo.X4):
				r=Assets1.comboX4;
				break;
			case (Combo.X5):
				r=Assets1.comboX5;
				break;
			}
			if (r!=null){
				batcher.beginBatch(Assets1.digitsAtlas);
				batcher.drawSprite(c.x, c.y, c.pulseWidth, c.pulseHeight, r , SpriteBatcher.NORMAL);
				batcher.endBatch();
			}
		}
	}
	
	
	void renderEffects(){
		if (!level.increaseScoreEffect.dead){
			fireBatcher.beginBatch(Assets1.digitsAtlas);
			for(int i=0; i<level.increaseScoreEffect.digits.length; i++){
				if (level.increaseScoreEffect.digits[i].display)
					fireBatcher.drawSpriteRGBA(level.increaseScoreEffect.digits[i].position.x, level.increaseScoreEffect.position.y, level.increaseScoreEffect.size, level.increaseScoreEffect.size, 0, 0.3921f, 0, level.increaseScoreEffect.alpha , Assets1.digits[level.increaseScoreEffect.digits[i].value], SpriteBatcher.NORMAL);
			}
			fireBatcher.endBatch();
		}
		
		if (!level.dragonHitEffect.dead){
			fireBatcher.beginBatch(Assets1.digitsAtlas);
			for(int i=0; i<level.dragonHitEffect.digits.length; i++){
				if (level.dragonHitEffect.digits[i].display)
					fireBatcher.drawSpriteRGBA(level.dragonHitEffect.digits[i].position.x, level.dragonHitEffect.position.y, level.dragonHitEffect.size, level.dragonHitEffect.size, 1, 0, 0, level.dragonHitEffect.alpha , Assets1.digits[level.dragonHitEffect.digits[i].value], SpriteBatcher.NORMAL);
//				Log.d("numbers", String.format("x pos %1$f y pos %2$f", level.dragonHitEffect.digits[i].position.x, level.dragonHitEffect.y));
			}
			fireBatcher.endBatch();
		}
	}
	
	abstract void renderInBackground();
	
	protected abstract void renderFood();

	protected abstract void renderEnemies();
	
	abstract void renderTutorial();

	protected void renderDragon(float deltaTime){
//		Dragon dragon = level.dragon;
//		batcher.beginBatch(Assets1.dragonAtlas);
//		batcher.drawSprite(dragon.position.x, dragon.position.y, dragon.width , dragon.height, dragon.angle, Assets1.dragon);
//		batcher.endBatch();
		dragonRenderer.render(deltaTime, level.isTouched);
	}
	
	protected void renderFire(){
		if (Fire.dead==false){
			fireBatcher.beginBatch(Assets1.particlesAtlas);
			for (int i = 0; i < Fire.particleNumber; i++) {
				fireBatcher.drawSpriteRGBA(Fire.particles[i].position.x, Fire.particles[i].position.y, Fire.particles[i].textureSize, Fire.particles[i].textureSize, 1, 1, 1, Fire.particles[i].alpha, Assets1.particleRegion, SpriteBatcher.NORMAL);
			}
			fireBatcher.endBatch();
		}
	}
	
	protected void renderFireflies(){
		FireflyEmitter fireflies=level.fireflies;
		if (fireflies.state==FireflyEmitter.ALIVE){
			batcher.beginBatch(Assets1.digitsAtlas);
			for (int i = 0; i < fireflies.fireflies.length; i++) {
				Firefly temp=fireflies.fireflies[i];
//					if (temp.state==Firefly.ALIVE)
					batcher.drawSprite(temp.position.x, temp.position.y, fireflies.size, fireflies.size, Assets1.fireflyRegion, SpriteBatcher.NORMAL);
			}
			batcher.endBatch();
		}
		
	}
	
	protected abstract void renderCurrentTarget();
	
	protected void renderCookingBar(){
		fireBatcher.beginBatch(Assets1.lifeBarAtlas);
		fireBatcher.drawSpriteRGBA(CookingStateBar.posX, CookingStateBar.posY, CookingStateBar.curentWidth, CookingStateBar.HEIGHT, CookingStateBar.red, CookingStateBar.green, CookingStateBar.blue, 1, Assets1.lifeBarTexture, SpriteBatcher.NORMAL);
		fireBatcher.endBatch();
	}
	
	protected void renderBars(){
		batcher.drawSprite(level.staminaBar.x, level.staminaBar.y, level.staminaBar.pulseWidth, level.staminaBar.currentHeight, Assets1.yellowRegion, SpriteBatcher.EXPAND_TOP);
		batcher.drawSprite(level.staminaBar.x, level.staminaBar.y, level.staminaBar.pulseWidth+0.025f, level.staminaBar.pulseHeight, Assets1.barContainer, SpriteBatcher.EXPAND_TOP);
		
		batcher.drawSprite(level.hpBar.x, level.hpBar.y, level.hpBar.pulseWidth, level.hpBar.currentHeight, Assets1.blueRegion, SpriteBatcher.EXPAND_TOP);
		batcher.drawSprite(level.hpBar.x, level.hpBar.y, level.hpBar.pulseWidth+0.025f, level.hpBar.pulseHeight, Assets1.barContainer, SpriteBatcher.EXPAND_TOP);
		
		batcher.drawSprite(level.staminaButton.position.x, level.staminaButton.position.y, level.staminaButton.bounds.width, level.staminaButton.bounds.height, Assets1.yellowDrop, SpriteBatcher.NORMAL);
	}
	
	protected void renderTimer(){
		for(int i=0; i<level.timer.digits.length; i++){
			batcher.drawSprite(level.timer.digits[i].position.x, level.timer.digits[i].position.y, 1, 1, Assets1.digits[level.timer.digits[i].value], SpriteBatcher.NORMAL);
		}
	}
	
	protected void renderScore(){
		for(int i=0; i<level.scoreDisplay.digits.length; i++){
			batcher.drawSprite(level.scoreDisplay.digits[i].position.x, level.scoreDisplay.digits[i].position.y, level.scoreDisplay.digitsize, level.scoreDisplay.digitsize, Assets1.digits[level.scoreDisplay.digits[i].value], SpriteBatcher.NORMAL);
		}
	}
	
	protected void renderCompleted(){
		if (level.state==Level.COMPLETED){
			CompleteDialogRenderer.renderCompleteDialog(level.completeDialog, batcher);
		}
	}
	
	protected void renderPaused(){
		if (level.state == Level.IN_GAME_PAUSED){
			PauseDialogRenderer.render(level.pauseDialog, batcher);
		}
	}

	protected void renderDragon() {
		// TODO Auto-generated method stub
		
	}
}
