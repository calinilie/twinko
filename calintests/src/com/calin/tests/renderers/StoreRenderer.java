package com.calin.tests.renderers;

import com.calin.test.levels.StoreDisplayModel;
import com.calin.tests.DragonGame;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.ButtonsAsstets;
import com.calin.tests.assets.ShopAssets;
import com.calin.tests.assets.StoreAssets;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.gl.TextureRegion;
import com.calin.tests.objects.DamagePotion;
import com.calin.tests.objects.HealingPotion;
import com.calin.tests.objects.Item;
import com.calin.tests.objects.MenuButton;
import com.calin.tests.objects.StaminaPotion;
import com.calin.tests.objects.Store;

public class StoreRenderer {
	
	public static void renderStore(StoreDisplayModel store, SpriteBatcher batcher){
//		batcher.beginBatch(ShopAssets.background);
//		batcher.drawSprite(Assets1.FRUSTRUM_WIDTH/2, Assets1.FRUNSTRUM_HEIGHT/2, 18, 10, ShopAssets.backgroundRegion, SpriteBatcher.NORMAL);
//		batcher.endBatch();
		
		batcher.beginBatch(ShopAssets.itemDescAtlas);
		batcher.drawSprite(store.itemDescriptionArea.position.x, store.itemDescriptionArea.position.y, store.itemDescriptionArea.width,  store.itemDescriptionArea.height, ShopAssets.itemDescriptionBackground, SpriteBatcher.NORMAL);
		batcher.endBatch();
		
		batcher.beginBatch(ShopAssets.atlas);
		batcher.drawSprite(store.potionsCategoryButton.position.x, store.potionsCategoryButton.position.y, store.potionsCategoryButton.bounds.width,  store.potionsCategoryButton.bounds.height, ShopAssets.potionsCategory, SpriteBatcher.NORMAL);
		batcher.drawSprite(store.goldCategoryButton.position.x, store.goldCategoryButton.position.y, store.goldCategoryButton.bounds.width,  store.goldCategoryButton.bounds.height, ShopAssets.goldCategory, SpriteBatcher.NORMAL);
		batcher.drawSprite(store.staminaPotion.position.x, store.staminaPotion.position.y, store.staminaPotion.bounds.width, store.staminaPotion.bounds.height, ShopAssets.staminaPotion, SpriteBatcher.NORMAL);
		batcher.drawSprite(store.healingPotion.position.x, store.healingPotion.position.y, store.healingPotion.bounds.width, store.healingPotion.bounds.height, ShopAssets.healingPotion, SpriteBatcher.NORMAL);
		batcher.drawSprite(store.damagePotion.position.x, store.damagePotion.position.y, store.damagePotion.bounds.width, store.damagePotion.bounds.height, ShopAssets.damagePotion, SpriteBatcher.NORMAL);
		batcher.drawSprite(store.smallGold.position.x, store.smallGold.position.y, store.smallGold.bounds.width, store.smallGold.bounds.height, ShopAssets.smallGold, SpriteBatcher.NORMAL);
		batcher.drawSprite(store.mediumGold.position.x, store.mediumGold.position.y, store.mediumGold.bounds.width, store.mediumGold.bounds.height, ShopAssets.mediumGold, SpriteBatcher.NORMAL);
		batcher.drawSprite(store.largeGold.position.x, store.largeGold.position.y, store.largeGold.bounds.width, store.largeGold.bounds.height, ShopAssets.largeGold, SpriteBatcher.NORMAL);
		batcher.endBatch();

		batcher.beginBatch(Assets1.digitsAtlas);
		RendererHelper.renderNoDisplay(Assets1.digits, batcher, store.goldCoins);
		RendererHelper.renderNoDisplay(Assets1.digits, batcher, store.staminaPotion.quantityDisplay);
		RendererHelper.renderNoDisplay(Assets1.digits, batcher, store.healingPotion.quantityDisplay);
		RendererHelper.renderNoDisplay(Assets1.digits, batcher, store.damagePotion.quantityDisplay);
		batcher.endBatch();
		
		batcher.beginBatch(ButtonsAsstets.atlas);
		batcher.drawSprite(store.backButton.position.x, store.backButton.position.y, store.backButton.bounds.width,  store.backButton.bounds.height, ButtonsAsstets.back, SpriteBatcher.NORMAL);
		batcher.drawSprite(store.buyButton.position.x, store.buyButton.position.y, store.buyButton.bounds.width,  store.buyButton.bounds.height, ButtonsAsstets.buy, SpriteBatcher.NORMAL);
		batcher.endBatch();
	}
}
