package com.calin.tests.renderers;

import com.calin.tests.assets.Assets1;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.objects.FailNotifier;

public class FailNotifierRenderer {

	public static void render(FailNotifier failNotifier, SpriteBatcher batcher){
		if (failNotifier.first.pulseHeight>0){
			batcher.beginBatch(Assets1.digitsAtlas);
			batcher.drawSprite(failNotifier.first.x, failNotifier.first.y, failNotifier.first.pulseWidth, failNotifier.first.pulseHeight, Assets1.redX, SpriteBatcher.NORMAL);
			if (failNotifier.second.pulseHeight>0) batcher.drawSprite(failNotifier.second.x, failNotifier.second.y, failNotifier.second.pulseWidth, failNotifier.second.pulseHeight, Assets1.redX, SpriteBatcher.NORMAL);
			if (failNotifier.last.pulseHeight>0) batcher.drawSprite(failNotifier.last.x, failNotifier.last.y, failNotifier.last.pulseWidth, failNotifier.last.pulseHeight, Assets1.redX, SpriteBatcher.NORMAL);
			batcher.endBatch();
		}
	}
	
}
