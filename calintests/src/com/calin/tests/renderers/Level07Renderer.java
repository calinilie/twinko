package com.calin.tests.renderers;

import com.calin.test.levels.Level;
import com.calin.test.levels.Level06;
import com.calin.test.levels.Level07;
import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.BeeAssets;
import com.calin.tests.framework.gl.SpriteBatcher;
import com.calin.tests.framework.impl.GLGraphics;
import com.calin.tests.objects.Bee;
import com.calin.tests.objects.Dragon;
import com.calin.tests.objects.Hint;
import com.calin.tests.objects.Target;

public class Level07Renderer extends LevelRenderer{

	BeeRenderer beeRenderer, mosquitoRenderer, batRenderer;
	Bee bee, bat, mosquito;
	
	public Level07Renderer(Level level, GLGraphics graphics,
			SpriteBatcher batcher, SpriteBatcher fireBatcher) {
		super(level, graphics, batcher, fireBatcher);
		bee=((Level07)level).bee;
		bat=((Level07)level).bat;
		mosquito=((Level07)level).mosquito;
		beeRenderer=new BeeRenderer(bee);
		mosquitoRenderer=new BeeRenderer(mosquito);
		batRenderer=new BeeRenderer(bat);
	}
	
//	@Override
//	protected void renderDragon(float deltaTime){
//		Dragon dragon = level.dragon;
//		batcher.beginBatch(Assets1.dragonAtlas);
//		float x=dragon.position.x;
//		float y=dragon.position.y;
//		float width=dragon.width;
//		float height=dragon.height;
//		float factor=1.6f;
//		batcher.drawSprite(x-(width*0.08333f), y-(width*0.08333f), -width/factor, height/factor, Assets1.dragonBody, SpriteBatcher.NORMAL);
//		batcher.drawSprite(x, y, -width , height, dragon.angle, Assets1.dragon);
//		batcher.endBatch();
//	}

	@Override
	protected void renderFood() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void renderEnemies() {
		if (bee!=level.currentTarget && bee.sizeState==Bee.NORMAL_SIZE){
			batcher.beginBatch(BeeAssets.beeAtlas);
			beeRenderer.render(batcher);
			batcher.endBatch();
		}
		
		if (bat!=level.currentTarget && bat.sizeState==Bee.NORMAL_SIZE){
			batcher.beginBatch(BeeAssets.beeAtlas);
			batRenderer.render(batcher);
			batcher.endBatch();
		}
		
		if (mosquito!=level.currentTarget && mosquito.sizeState==Bee.NORMAL_SIZE){
			batcher.beginBatch(BeeAssets.beeAtlas);
			mosquitoRenderer.render(batcher);
			batcher.endBatch();
		}
		
	}

	@Override
	void renderTutorial() {
		if (level.state==Level.TUTORIAL_PAUSED){
			Hint hint=((Level07)level).hint;
			batcher.beginBatch(Assets1.tutorialAtlas);
			batcher.drawSprite(hint.position.x, hint.position.y, hint.size, hint.size, Assets1.hintLevel7, SpriteBatcher.NORMAL);
			if (hint.button.enabled)
				batcher.drawSprite(hint.button.position.x, hint.button.position.y, hint.button.bounds.width, hint.button.bounds.height, Assets1.okButton, SpriteBatcher.NORMAL);
			batcher.endBatch();
		}
		
	}

	@Override
	protected void renderCurrentTarget() {
		if (level.currentTarget!=null){
			if (level.currentTarget.state==Target.ALIVE){
				if (bee==level.currentTarget){
					batcher.beginBatch(BeeAssets.beeAtlas);
					beeRenderer.render(batcher);
					batcher.endBatch();
					return;
				}
				
				if (bat==level.currentTarget){
					batcher.beginBatch(BeeAssets.beeAtlas);
					batRenderer.render(batcher);
					batcher.endBatch();
					return;
				}
				
				if (mosquito==level.currentTarget && mosquito.sizeState==Bee.NORMAL_SIZE){
					batcher.beginBatch(BeeAssets.beeAtlas);
					mosquitoRenderer.render(batcher);
					batcher.endBatch();
					return;
				}
			}
		}
	}

	@Override
	void renderInBackground() {
		if (bee.sizeState!=Bee.NORMAL_SIZE){
			batcher.beginBatch(BeeAssets.beeAtlas);
			beeRenderer.render(batcher);
			batcher.endBatch();
		}
		
		if (bat.sizeState!=Bee.NORMAL_SIZE){
			batcher.beginBatch(BeeAssets.beeAtlas);
			batRenderer.render(batcher);
			batcher.endBatch();
		}
		
		if (mosquito.sizeState!=Bee.NORMAL_SIZE){
			batcher.beginBatch(BeeAssets.beeAtlas);
			mosquitoRenderer.render(batcher);
			batcher.endBatch();
		}
	}

}
