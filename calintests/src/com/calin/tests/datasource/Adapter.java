package com.calin.tests.datasource;

import com.calin.test.levels.LevelIconsDisplayModel;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Adapter extends SQLiteOpenHelper{

	private static final String DB_NAME="db38.db";
	private static final int DB_VERSION=1;
	
	public static final String LEVELS_TABLE="levels";
	public static final String LEVELS_ID="id";
	public static final String LEVELS_PASSED="passed";
	public static final String LEVELS_HIGH_SCORE="maxscore";
	
	public static final String CATEGORIES_TABLE = "categories";
	public static final String CATEGORIES_ID = "id";
	public static final String CATEGORIES_IS_UNLOCKED = "isUnlocked";
	
	public static final String DRAGON_STATS_TABLE="dragonstats";
	public static final String DRAGON_STATS_STAMINA="stamina";
	public static final String DRAGON_STATS_MAX_STAMINA="maxstamina";
	public static final String DRAGON_STATS_HP="hp";
	public static final String DRAGON_STATS_MAX_HP="maxhp";
	public static final String DRAGON_STATS_DMG="dmg";
	public static final String DRAGON_STATS_XP="xp";
	public static final String DRAGON_STATS_LVL="level";
	public static final String DRAGON_STATS_SKILLPOINTS="skillpoints";
	public static final String DRAGON_STATS_STAMINA_DROP_RATE="staminadrop";
	public static final String STORE_TABLE="store";
	public static final String STORE_ITEM="item";
	public static final String STORE_PRICE="price";
	public static final String BAG_TABLE="bag";
	public static final String BAG_ITEM="item";
	public static final String BAG_QTY="qty";
	
	public static final String BAG_CONTENT_STAMINA="sp";
	public static final String BAG_CONTENT_HEALING="hp";
	public static final String BAG_CONTENT_DAMAGE="dp";
	public static final String BAG_CONTENT_GOLD="gc";
	public static final String BAG_CONTENT_SILVER="sc";
	
	private final String CREATE_CATEGORIES_TABLE = String.format("CREATE TABLE %1s (%2s integer primary key, %3s integer)", CATEGORIES_TABLE, CATEGORIES_ID, CATEGORIES_IS_UNLOCKED);
	private final String INSERT_CATEGORY_MEDOW = String.format("insert into %1s values(%2d, 1)", CATEGORIES_TABLE, LevelIconsDisplayModel.MEDOW_LEVELS);
	private final String INSERT_CATEGORY_JUNGLE = String.format("insert into %1s values(%2d, 0)", CATEGORIES_TABLE, LevelIconsDisplayModel.JUNGLE_LEVELS);
	private final String INSERT_CATEGORY_CAVE = String.format("insert into %1s values(%2d, 0)", CATEGORIES_TABLE, LevelIconsDisplayModel.CAVE_LEVELS);
	private final String INSERT_CATEGORY_4 = String.format("insert into %1s values(%2d, 0)", CATEGORIES_TABLE, LevelIconsDisplayModel.CATEGORY4);
	
	private final String CREATE_STATS_TABLE="CREATE TABLE "+DRAGON_STATS_TABLE+" ("+DRAGON_STATS_STAMINA+" numeric, "+DRAGON_STATS_MAX_STAMINA+" numeric, "+DRAGON_STATS_HP+" numeric, "+DRAGON_STATS_MAX_HP+" numeric, "+DRAGON_STATS_DMG+" real, "+DRAGON_STATS_XP+" numberic, "+DRAGON_STATS_LVL+" numeric, "+Adapter.DRAGON_STATS_SKILLPOINTS+" numberic, "+DRAGON_STATS_STAMINA_DROP_RATE+" real);";
	private final String INSERT_STATS="INSERT INTO "+DRAGON_STATS_TABLE+" VALUES(150, 150, 30, 30, 0.01, 0, 0, 0, 0.01);";
	
	private final String CREATE_LEVELS_TABLE="CREATE TABLE "+Adapter.LEVELS_TABLE+" ("+Adapter.LEVELS_ID +" integer PRIMARY KEY, "+Adapter.LEVELS_PASSED+" integer, "+Adapter.LEVELS_HIGH_SCORE+" integer);";
	private final String INSERT_LEVEL1="INSERT INTO levels VALUES(1,0,0);";
	private final String INSERT_LEVEL2="INSERT INTO levels VALUES(2,0,0);";
	private final String INSERT_LEVEL3="INSERT INTO levels VALUES(3,0,0);";
	private final String INSERT_LEVEL4="INSERT INTO levels VALUES(4,0,0);";
	private final String INSERT_LEVEL5="INSERT INTO levels VALUES(5,0,0);";
	private final String INSERT_LEVEL6="INSERT INTO levels VALUES(6,0,0);";
	private final String INSERT_LEVEL7="INSERT INTO levels VALUES(7,0,0);";
	private final String INSERT_LEVEL8="INSERT INTO levels VALUES(8,0,0);";
	private final String INSERT_LEVEL9="INSERT INTO levels VALUES(9,0,0);";
	
//	private final String CREATE_STORE="create table "+STORE_TABLE+"("+STORE_ITEM+" text primary key, "+STORE_PRICE+" numeric not null)";
//	private final String POPULATE_STORE1="insert into "+STORE_TABLE+" values('hp', 5)";
//	private final String POPULATE_STORE2="insert into "+STORE_TABLE+" values('sp', 5)";
//	private final String POPULATE_STORE3="insert into "+STORE_TABLE+" values('dp', 5)";
	
	private final String CREATE_BAG="create table "+BAG_TABLE+"("+BAG_ITEM+" text unique, "+BAG_QTY+" numeric not null)";
	private final String POPULATE_BAG1="insert into "+BAG_TABLE+" values('sp', 10)";
	private final String POPULATE_BAG2="insert into "+BAG_TABLE+" values('hp', 2)";
	private final String POPULATE_BAG3="insert into "+BAG_TABLE+" values('dp', 3)";
	private final String POPULATE_BAG4="insert into "+BAG_TABLE+" values('gc', 30)";
	private final String POPULATE_BAG5="insert into "+BAG_TABLE+" values('sc', 50)";
	private final String[] queries = new String[] { 
			CREATE_LEVELS_TABLE, INSERT_LEVEL1, INSERT_LEVEL2, INSERT_LEVEL3, INSERT_LEVEL4, INSERT_LEVEL5, INSERT_LEVEL6, INSERT_LEVEL7, INSERT_LEVEL8, INSERT_LEVEL9,
			CREATE_STATS_TABLE, INSERT_STATS, 
			CREATE_BAG, POPULATE_BAG1, POPULATE_BAG2, POPULATE_BAG3, POPULATE_BAG4, POPULATE_BAG5,
			CREATE_CATEGORIES_TABLE, INSERT_CATEGORY_MEDOW, INSERT_CATEGORY_JUNGLE, INSERT_CATEGORY_CAVE, INSERT_CATEGORY_4};	
	
	public Adapter(Context context){
		super(context, DB_NAME, null, DB_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d("sqlite_db","on create called");
		try{
			for (String s:queries){
				db.execSQL(s);
			}
		}
		catch (Exception e){
			Log.d("sqlite_db", e.getMessage());
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.e("sqlite_db", "on upgrade called in facade");
	}

}
