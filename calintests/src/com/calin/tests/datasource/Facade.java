package com.calin.tests.datasource;

import com.calin.tests.objects.DragonBag;
import com.calin.tests.objects.DragonStats;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Facade {

	private SQLiteDatabase database;
	private Adapter dataAdapter;
	private final String[] level_cols = { Adapter.LEVELS_ID,
			Adapter.LEVELS_PASSED, Adapter.LEVELS_HIGH_SCORE };
	private final String[] stats_cols = { Adapter.DRAGON_STATS_MAX_HP,
			Adapter.DRAGON_STATS_HP, Adapter.DRAGON_STATS_MAX_STAMINA,
			Adapter.DRAGON_STATS_STAMINA, Adapter.DRAGON_STATS_DMG,
			Adapter.DRAGON_STATS_XP, Adapter.DRAGON_STATS_LVL,
			Adapter.DRAGON_STATS_SKILLPOINTS,
			Adapter.DRAGON_STATS_STAMINA_DROP_RATE };
	private final String[] bag_col = { Adapter.BAG_ITEM, Adapter.BAG_QTY };
	private final String[] categories_cols = {Adapter.CATEGORIES_ID, Adapter.CATEGORIES_IS_UNLOCKED};
	private ContentValues values;

	public Facade(Context context) {
		dataAdapter = new Adapter(context);
		values = new ContentValues();
	}

	public void open() {
		database = dataAdapter.getWritableDatabase();
	}

	public void close() {
		dataAdapter.close();
	}

	public void updateMaxScore(int newHighScore, int levelId) {
		values.clear();
		values.put(Adapter.LEVELS_HIGH_SCORE, newHighScore);
		String whereClause = Adapter.LEVELS_HIGH_SCORE + "< ? and "+ Adapter.LEVELS_ID + "= ?";
		String[] whereArgs = new String[] { newHighScore + "", levelId + "" };
		Log.d("stats", "update max score "+database.update(Adapter.LEVELS_TABLE, values, whereClause, whereArgs));
	}

	public int getCurrentHighScore(int levelId) {
		Cursor cursor = database.query(Adapter.LEVELS_TABLE, level_cols, Adapter.LEVELS_ID + " = ?",
				new String[]{levelId+""}, null, null, null);
		int result = 0;
		if (cursor.moveToFirst()) {
			result = cursor.getInt(2);
		}
		cursor.close();
		return result;
	}

	public DragonStats getDragonStats() {
		Cursor cursor = database.query(Adapter.DRAGON_STATS_TABLE, stats_cols,
				null, null, null, null, null);
		DragonStats ds = null;
		if (cursor.moveToFirst()) {
			float MAX_HP = cursor.getFloat(0);
			float hp = cursor.getFloat(1);
//			float hp = MAX_HP;
			float MAX_STAMINA = cursor.getFloat(2);
			 float stamina=cursor.getFloat(3);
//			float stamina = MAX_STAMINA;
			float dmg = cursor.getFloat(4);
			int XP = cursor.getInt(5);
			int lvl = cursor.getInt(6);
			int unusedSkillPoints = cursor.getInt(7);
			float staminDropRate = cursor.getFloat(8);
			// maxhp hp max_stamina stamin_drop| sta dmg XP lvl -
			ds = new DragonStats(MAX_HP, hp, MAX_STAMINA, staminDropRate,
					stamina, dmg, XP, lvl, unusedSkillPoints);
		}
		cursor.close();
		return ds;
	}

	public boolean getLevelPreviouslyPassed(int levelId) {
		String selection = Adapter.LEVELS_ID + " = ?";
		String[] selectionArgs = new String[] { levelId + "" };
		Cursor cursor = database.query(
				Adapter.LEVELS_TABLE, 
				level_cols,
				selection, 
				selectionArgs, 
				null, null, null);
		if (cursor.moveToFirst()) {
			if (cursor.getInt(1) == 1)
				return true;
		}
		return false;
	}

	public DragonBag getBag() {
		Cursor cursor = database.query(Adapter.BAG_TABLE, bag_col, null, null,
				null, null, null);
		cursor.moveToFirst();
		int spq = cursor.getInt(1);
		cursor.moveToNext();
		int hpq = cursor.getInt(1);
		cursor.moveToNext();
		int dpq = cursor.getInt(1);
		cursor.moveToNext();
		int gcq = cursor.getInt(1);
		cursor.moveToNext();
		int scq = cursor.getInt(1);
		DragonBag dragonBag=new DragonBag(spq, hpq, dpq, gcq, scq);
		
		return dragonBag;
	}
	
	public void updateDragonStatsOnLevelComplete(DragonStats stats) {
		values.clear();
		values.put(Adapter.DRAGON_STATS_STAMINA, stats.stamina);
		values.put(Adapter.DRAGON_STATS_HP, stats.hp);
		values.put(Adapter.DRAGON_STATS_XP, stats.XP);
		values.put(Adapter.DRAGON_STATS_LVL, stats.level);
		values.put(Adapter.DRAGON_STATS_SKILLPOINTS, stats.unusedSkillPoints);
		database.update(Adapter.DRAGON_STATS_TABLE, values, null, null);
	}

	public void updateDragonStatsOnSkillInc(DragonStats stats) {
		values.clear();
		values.put(Adapter.DRAGON_STATS_MAX_STAMINA, stats.maxStamina);
		values.put(Adapter.DRAGON_STATS_MAX_HP, stats.maxHP);
		values.put(Adapter.DRAGON_STATS_DMG, stats.damage);
		values.put(Adapter.DRAGON_STATS_STAMINA_DROP_RATE,
				stats.staminaDropRate);
		values.put(Adapter.DRAGON_STATS_SKILLPOINTS, stats.unusedSkillPoints);
		database.update(Adapter.DRAGON_STATS_TABLE, values, null, null);
	}
	
	public void updateDragonBag(DragonBag bag){
		values.clear();
		values.put(Adapter.BAG_QTY, bag.getHealingPotionQ());
		String[] whereArgs = {Adapter.BAG_CONTENT_HEALING}; 
		database.update(Adapter.BAG_TABLE, values, Adapter.BAG_ITEM + "=?", whereArgs);
		
		values.clear();
		values.put(Adapter.BAG_QTY, bag.getDamagePotionQ());
		whereArgs = new String[]{Adapter.BAG_CONTENT_DAMAGE}; 
		database.update(Adapter.BAG_TABLE, values, Adapter.BAG_ITEM + "=?", whereArgs);
		
		values.clear();
		values.put(Adapter.BAG_QTY, bag.getStaminaPotionsQ());
		whereArgs = new String[]{Adapter.BAG_CONTENT_STAMINA}; 
		database.update(Adapter.BAG_TABLE, values, Adapter.BAG_ITEM + "=?", whereArgs);
		
		values.clear();
		values.put(Adapter.BAG_QTY, bag.getGoldCoins());
		whereArgs = new String[]{Adapter.BAG_CONTENT_GOLD};
		database.update(Adapter.BAG_TABLE, values, Adapter.BAG_ITEM + "=?", whereArgs);
		
//		TODO save silver coins
//		values.clear();
//		values.put(Adapter.BAG_QTY, bag.get);
//		whereArgs = new String[]{Adapter.BAG_CONTENT_HEALING}; 
//		Log.d("stats", database.update(Adapter.BAG_TABLE, values, Adapter.BAG_ITEM + "=?", whereArgs) +"");
	}

	public void setLevelPassed(int levelId) {
		values.clear();
		values.put(Adapter.LEVELS_PASSED, 1);
		String[] whereArgs = {levelId+""};
		database.update(Adapter.LEVELS_TABLE, values, Adapter.LEVELS_ID +" = ?", whereArgs);
	}
	
	public void setCategoryUnlocked(int categoryId){
		values.clear();
		values.put(Adapter.CATEGORIES_IS_UNLOCKED, 1);
		String[] whereArgs = {categoryId+""};
		String where = Adapter.CATEGORIES_ID +" = ?";
		database.update(Adapter.CATEGORIES_TABLE, values, where, whereArgs);
		Log.d("levels", String.format("category %d was unlocked", categoryId));
	}
	
	public boolean isCategoryUnlocked(int categoryId){
		String selection = Adapter.CATEGORIES_ID + " = ?";
		String[] selectionArgs = new String[] {categoryId +""};
		Cursor cursor = database.query(
				Adapter.CATEGORIES_TABLE, 
				categories_cols, 
				selection, 
				selectionArgs, 
				null, null, null);
		if (cursor.moveToFirst()){
			if (cursor.getInt(1)==1)
				return true;
		}
		return false;
	}

}
