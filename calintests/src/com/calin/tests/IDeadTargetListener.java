package com.calin.tests;

import com.calin.tests.objects.Target;

public interface IDeadTargetListener {

	public void onDeadTargetEvent(Target source, int score);
	
}
