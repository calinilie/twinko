//package com.calin.tests;
//
//import java.util.List;
//
//import javax.microedition.khronos.opengles.GL10;
//
//import android.util.Log;
//
//import com.calin.tests.datasource.Facade;
//import com.calin.tests.framework.Game;
//import com.calin.tests.framework.Input;
//import com.calin.tests.framework.Input.KeyEvent;
//import com.calin.tests.framework.Input.TouchEvent;
//import com.calin.tests.framework.gl.Camera2D;
//import com.calin.tests.framework.gl.SpriteBatcher;
//import com.calin.tests.framework.gl.Texture;
//import com.calin.tests.framework.gl.TextureRegion;
//import com.calin.tests.framework.impl.GLGame;
//import com.calin.tests.framework.impl.GLGraphics;
//import com.calin.tests.framework.impl.GLScreen;
//import com.calin.tests.framework.math.Circle;
//import com.calin.tests.framework.math.OverlapTester;
//import com.calin.tests.framework.math.Vector2;
//import com.calin.tests.objects.DragonStats;
//import com.calin.tests.objects.NumbersDisplay;
//import com.calin.tests.renderers.RendererHelper;
//
//public class DragonStatsScreen extends GLScreen{
//	
//	final float FRUSTRUM_WIDTH = 10f;
//	final float FRUNSTRUM_HEIGHT=16f;
//
//	Texture digitsAtlas;
//	TextureRegion[] digitsAssets;
//	TextureRegion staminaTexture;
//	TextureRegion hpTexture;
//	TextureRegion dmgTexture;
//	TextureRegion increaseTexture;
//	TextureRegion skillPointsTexture;
//	GameObject stamina, hp, dmg, increaseStamina, increaseHp, increaseDmg, skillPoints;
//	NumbersDisplay staminaNo, hpNo, dmgNo, skillPointsNo;
//	DragonStats dragonStats;
//	GLGraphics graphics;
//	Input input;
//	Camera2D camera;
//	SpriteBatcher batcher;
//	Circle fingerTouch;
//	Vector2 touchPos;
//	
//	public DragonStatsScreen(Game game) {
//		super(game);
//		stamina=new GameObject(2.5f, 15, 5, 5/4f);
//		staminaNo=new NumbersDisplay(5, 13.5f, false, 1);
//		increaseStamina=new GameObject(9, 13.5f, 1, 1);
//		
//		hp=new GameObject(2.5f, 11, 5, 5/4f);
//		hpNo=new NumbersDisplay(5, 9.5f, false, 1);
//		increaseHp=new GameObject(9, 9.5f, 1, 1);
//
//		dmg=new GameObject(2.5f, 7, 5, 5/4f);
//		dmgNo=new NumbersDisplay(5, 5.5f, false, 1);
//		increaseDmg=new GameObject(9, 5.5f, 1, 1);
//		
//		skillPoints=new GameObject(2.5f, 3, 5, 5/4f);
//		skillPointsNo=new NumbersDisplay(7.5f, 3, false, 1);
//		
//		graphics=((GLGame)game).getGLGraphics();
//		input=game.getInput();
//		camera=new Camera2D(graphics, FRUSTRUM_WIDTH, FRUNSTRUM_HEIGHT);
//		batcher=new SpriteBatcher(graphics, 50);
//		
//		dragonStats=GameObjectsController.getInstance(game).dragon.dragonStats;
//		fingerTouch=new Circle(-1, -1, 1);
//		touchPos=new Vector2();
//	}
//	
//	@Override
//	public void update(float deltaTime) {
//		List<TouchEvent> touchEvents=input.getTouchEvents();
//		List<KeyEvent> keyEvents = input.getKeyEvents();
//		
//		staminaNo.update(deltaTime, dragonStats.maxStamina);
//		hpNo.update(deltaTime, dragonStats.maxHP);
//		dmgNo.update(deltaTime, dragonStats.damage*1000);
//		skillPointsNo.update(deltaTime, dragonStats.unusedSkillPoints);
//		
//		for (TouchEvent event : touchEvents){
//			touchPos.set(event.x, event.y);
//			camera.touchToWorld(touchPos);
//			
//			fingerTouch.center.set(touchPos);
//			if (OverlapTester.overlapCircleRectangle(fingerTouch, increaseStamina.bounds) && event.type==TouchEvent.TOUCH_UP){
//				Log.d("stats", "increase stamina");
//				dragonStats.increaseMaxStamina();
//			}
//			
//			if (OverlapTester.overlapCircleRectangle(fingerTouch, increaseHp.bounds) && event.type==TouchEvent.TOUCH_UP){
//				Log.d("stats", "increase HP");
//				dragonStats.increaseMaxHp();
//			}
//			
//			if (OverlapTester.overlapCircleRectangle(fingerTouch, increaseDmg.bounds) && event.type==TouchEvent.TOUCH_UP){
//				Log.d("stats", "increase dmg");
//				dragonStats.increaseDmg();
//			}
//		}
//		
//		for (KeyEvent event : keyEvents){
//			if (event.keyCode==android.view.KeyEvent.KEYCODE_BACK && event.type==KeyEvent.KEY_UP){
//				game.setScreen(new MainScreen(game));
//			}
//		}
//		
//	}
//
//	@Override
//	public void present(float deltaTime) {
//		GL10 gl = graphics.getGL();
//		gl.glClear(GL10.GL_COLOR_BUFFER_BIT); 
//		camera.setViewportAndMatrices();
//		
//		gl.glEnable(GL10.GL_TEXTURE_2D);
//		gl.glEnable(GL10.GL_BLEND);
//		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
//		
//		GameObject g;
//		batcher.beginBatch(digitsAtlas);
//		g=stamina;
//		batcher.drawSprite(g.position.x, g.position.y, g.bounds.width, g.bounds.height, staminaTexture, SpriteBatcher.NORMAL);
//		g=hp;
//		batcher.drawSprite(g.position.x, g.position.y, g.bounds.width, g.bounds.height, hpTexture, SpriteBatcher.NORMAL);
//		g=dmg;
//		batcher.drawSprite(g.position.x, g.position.y, g.bounds.width, g.bounds.height, dmgTexture, SpriteBatcher.NORMAL);
//		g=increaseStamina;
//		batcher.drawSprite(g.position.x, g.position.y, g.bounds.width, g.bounds.height, increaseTexture, SpriteBatcher.NORMAL);
//		g=increaseHp;
//		batcher.drawSprite(g.position.x, g.position.y, g.bounds.width, g.bounds.height, increaseTexture, SpriteBatcher.NORMAL);
//		g=increaseDmg;
//		batcher.drawSprite(g.position.x, g.position.y, g.bounds.width, g.bounds.height, increaseTexture, SpriteBatcher.NORMAL);
//		g=skillPoints;
//		batcher.drawSprite(g.position.x, g.position.y, g.bounds.width, g.bounds.height, skillPointsTexture, SpriteBatcher.NORMAL);
//		
//		RendererHelper.renderNoDisplay(digitsAssets, batcher, staminaNo);
//		RendererHelper.renderNoDisplay(digitsAssets, batcher, hpNo);
//		RendererHelper.renderNoDisplay(digitsAssets, batcher, dmgNo);
//		RendererHelper.renderNoDisplay(digitsAssets, batcher, skillPointsNo);
//		
//		batcher.endBatch();
//		
//		gl.glDisable(GL10.GL_BLEND);
//	}
//	
//	@Override
//	public void pause() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void resume() {
//		digitsAtlas = new Texture((GLGame) game, "digits.png");
//		digitsAssets = new TextureRegion[11];
//		int x=0;
//		int y=0;
//		for (int i = 0; i < digitsAssets.length; i++) {
//			digitsAssets[i]=new TextureRegion(digitsAtlas, x, y, 32, 32);
//			x+=32;
//			if (i==7) y+=32;
//		}
//		staminaTexture=new TextureRegion(digitsAtlas, 0, 256, 128, 32);
//		dmgTexture=new TextureRegion(digitsAtlas, 128, 256, 128, 32);
//		hpTexture=new TextureRegion(digitsAtlas, 0, 288, 128, 32);
//		increaseTexture=new TextureRegion(digitsAtlas, 128, 288, 32, 32);
//		skillPointsTexture=new TextureRegion(digitsAtlas, 0, 320, 128, 32);
//	}
//
//	@Override
//	public void dispose() {
//		digitsAtlas.dispose();
//		Facade facade=((GLGame)game).getDataSourceFacade();
//		facade.open();
//		facade.updateDragonStatsOnSkillInc(dragonStats);
//		facade.close();
//		Log.d("stats", dragonStats.toString());
//	}
//}
