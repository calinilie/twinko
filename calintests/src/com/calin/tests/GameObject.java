package com.calin.tests;

import com.calin.tests.framework.math.Rectangle;
import com.calin.tests.framework.math.Vector2;

public class GameObject {
    public final Vector2 position;
    public final Rectangle bounds;
    
    /**
     @param x x-axis coordinate for vector position
     @param y y-axis coordinate for vector position
     */
    public GameObject(float x, float y, float width, float height) {
        this.position = new Vector2(x,y);
        this.bounds = new Rectangle(x-width/2, y-height/2, width, height);
    }
    
    public void setBounds(float x, float y, float width, float height){
    	this.bounds.lowerLeft.set(x-width/2, y-height/2);
    	this.bounds.height=height;
    	this.bounds.width=width;
    }
}
