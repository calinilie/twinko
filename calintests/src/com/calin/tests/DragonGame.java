package com.calin.tests;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.calin.tests.assets.Assets1;
import com.calin.tests.assets.BeeAssets;
import com.calin.tests.assets.ButtonsAsstets;
import com.calin.tests.assets.FoodAssets;
import com.calin.tests.assets.ShopAssets;
import com.calin.tests.assets.StoreAssets;
import com.calin.tests.assets.TwinkoAssets;
import com.calin.tests.framework.Screen;
import com.calin.tests.framework.impl.GLGame;

public class DragonGame extends GLGame{	
	
	 boolean firstTimeCreate = true;
	 public static final boolean tutorial=false;
	
	@Override
	public Screen getStartScreen() {
		return new MainScreen(this);
	}
	
	 public void onSurfaceCreated(GL10 gl, EGLConfig config) {         
	        super.onSurfaceCreated(gl, config);
	        if(firstTimeCreate) {
	        	TwinkoAssets.load(this);
	            Assets1.load(this);
	            StoreAssets.load(this);
	            BeeAssets.load(this);
				FoodAssets.load(this);
				ButtonsAsstets.load(this);
				ShopAssets.load(this);
	            firstTimeCreate = false;            
	        } else {
	        	TwinkoAssets.reload();
	            Assets1.reload();
	            StoreAssets.reload();
	            BeeAssets.reload();
	            FoodAssets.reload();
	            ButtonsAsstets.reload();
	            ShopAssets.reload();
	        }
	    }

	@Override
	public void onBackPressed(){
		return;
	}
} 
